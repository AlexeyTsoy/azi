﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectingManagerScript : MonoBehaviour
{
    public UILabel notification;

	void Start ()
    {
		
	}
	
	void Update ()
    {
        if (ConnectorScript.errorMsg != null)
        {
            notification.text = ConnectorScript.errorMsg;
        }
	}

    void OnDisable()
    {
        ConnectorScript.errorMsg = null;
    }
}
