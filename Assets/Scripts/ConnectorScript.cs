﻿using UnityEngine;
using BestHTTP.WebSocket;
using System;
using UnityEngine.SceneManagement;

public class ConnectorScript : MonoBehaviour
{
    public static WebSocket webSocket;

    public static string errorMsg;
    public static string notificationMsg;
    public static string ipAdress;

    bool isAlreadyOpen;
    bool isAlreadyOn;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    void OnDestroy()
    {
        if (webSocket != null)
        {
            webSocket.Close();
            Debug.Log("Websocket closed");
        }
    }

    void Start ()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            webSocket.Close();
        }

        if (webSocket == null && !isAlreadyOn)
        {
            isAlreadyOn = true;
            // Create the WebSocket instance
            webSocket = new WebSocket(new Uri("ws://" + ipAdress +":80"));

            // Subscribe to the WS events
            webSocket.OnOpen += OnOpen;
            webSocket.OnMessage += OnMessageReceived;
            webSocket.OnClosed += OnClosed;
            webSocket.OnError += OnError;

            isAlreadyOpen = false;

            // Start connecting to the server
            webSocket.Open();
        }

        if (webSocket != null)
        {
            if (webSocket.IsOpen && !isAlreadyOpen)
            {
                isAlreadyOpen = true;
                SceneManager.LoadScene("Main");
            }
        }
    }

    #region WebSocket Event Handlers

    /// <summary>
    /// Called when the web socket is open, and we are ready to send and receive data
    /// </summary>
    void OnOpen(WebSocket ws)
    {
        print(string.Format("-WebSocket Open!\n"));
    }

    /// <summary>
    /// Called when we received a text message from the server
    /// </summary>
    void OnMessageReceived(WebSocket ws, string message)
    {
        //print(string.Format("-Message received: {0}\n", message));
        print(string.Format("-Message received: {0}\n", System.Text.RegularExpressions.Regex.Unescape(message)));

        JsonUtility.FromJsonOverwrite(message.Trim(), Table.instance);
        JsonUtility.FromJsonOverwrite(message.Trim(), Profile.instance);
        JsonUtility.FromJsonOverwrite(message.Trim(), Players.instance);

        Notification.instance = JsonUtility.FromJson<Notification>(message.Trim());
        if (Notification.instance.error != null)
        {
            errorMsg = Notification.instance.error;
        }
        else
        {
            notificationMsg = Notification.instance.notification;
        }
    }

    /// <summary>
    /// Called when the web socket closed
    /// </summary>
    void OnClosed(WebSocket ws, UInt16 code, string message)
    {
        print(string.Format("-WebSocket closed! Code: {0} Message: {1}\n", code, message));
        webSocket = null;
    }

    /// <summary>
    /// Called when an error occured on client side
    /// </summary>
    void OnError(WebSocket ws, Exception ex)
    {
        string errorMsg = string.Empty;
#if !UNITY_WEBGL || UNITY_EDITOR
        if (ws.InternalRequest.Response != null)
            errorMsg = string.Format("Status Code from Server: {0} and Message: {1}", ws.InternalRequest.Response.StatusCode, ws.InternalRequest.Response.Message);
#endif

        print(string.Format("-An error occured: {0}\n", (ex != null ? ex.Message : "Unknown Error " + errorMsg)));
        ConnectorScript.errorMsg = string.Format("-An error occured: {0}\n", (ex != null ? ex.Message : "Unknown Error " + errorMsg));
        webSocket = null;
    }

    #endregion
}
