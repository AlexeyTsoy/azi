﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    public static GameManagerScript instance;

    public List<UISprite> cards = new List<UISprite>();

    public UILabel betLabel; // ставка

    public Table table;
    public Profile profile;
    public Players p;

    public bool isMakedBet; // удалить

    bool isAlreadyStarted;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep; // что бы экран не засыпал
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        GetGameUsers();
    }
    void makeDefBet()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // сброс карты
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "makeDefBet";
            msgToSend.place_id = Table.instance.place_id;
            msgToSend.game_id = Table.instance.game_id;
            msgToSend.table_id = Table.instance.table_id;
            msgToSend.sid = Profile.instance.sid;
            msgToSend.cash = Table.instance.cash;

            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n" + json);

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }
    void Update()
    {
        table = Table.instance;
        profile = Profile.instance;
        p = Players.instance;

        if (Players.instance.start_game == 1 && !isAlreadyStarted)
        {
            GetCards();
            isAlreadyStarted = true;
        }
        if (Players.instance.start_game == 0)
        {
            isAlreadyStarted = false;
        }
        if (!string.IsNullOrEmpty(cards[1].spriteName) && cards[1].spriteName != "0" && !isMakedBet
            && Players.instance.next_def_bet_user == Table.instance.place_id)
        {
            isMakedBet = true;
            makeDefBet();
            print("makeDefBet");
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            GetGameUsers();
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // поиск игры
            {
                // Create json to server
                SendMsg msgToSend = new SendMsg();

                msgToSend.action = "place";
                msgToSend.sid = Profile.instance.sid;
                msgToSend.cash = 5;

                string json = JsonUtility.ToJson(msgToSend);

                print("Sending message...\n");

                // Send message to the server
                ConnectorScript.webSocket.Send(json);
            }
        }

    }

    void GetCards()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // получение карт от сервера
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "cards";
            msgToSend.table_id = Table.instance.table_id;
            msgToSend.game_id = Table.instance.game_id;
            msgToSend.sid = Profile.instance.sid;

            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    void GetGameUsers()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // получения массива всех игроков
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "getGameUsers";
            msgToSend.table_id = Table.instance.table_id;
            msgToSend.game_id = Table.instance.game_id;
            msgToSend.players_limit = Table.instance.test;
            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    void MakeBet(int bet)
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // сброс карты
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "makeBet";
            msgToSend.place_id = Table.instance.place_id;
            msgToSend.game_id = Table.instance.game_id;
            msgToSend.table_id = Table.instance.table_id;
            msgToSend.sid = Profile.instance.sid;
            msgToSend.bet = bet;

            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n" + json);

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void Call()
    {
        int call = Players.instance.currentBet;
        MakeBet(call);
    }

    public void Raise()
    {
        int raise = int.Parse(betLabel.text);
        print(raise);
        MakeBet(raise);
    }

    public void Check()
    {
        MakeBet(0);
    }

    public void GetPossibleCards()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // получения возможных карт для хода
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "variantCard";
            msgToSend.user = Table.instance.place_id;
            msgToSend.game_id = Table.instance.game_id;

            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void Card1()
    {
        if (Players.instance.card1Possible == cards[1].spriteName || Players.instance.card2Possible == cards[1].spriteName
            || Players.instance.card3Possible == cards[1].spriteName)
        {
            if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) //сброс карты
            {
                // Create json to server
                SendMsg msgToSend = new SendMsg();

                msgToSend.action = "sendCard";
                msgToSend.game_id = Table.instance.game_id;
                msgToSend.table_id = Table.instance.table_id;
                msgToSend.card_name = cards[1].spriteName;
                msgToSend.sid = Profile.instance.sid;

                string json = JsonUtility.ToJson(msgToSend);

                print("Sending message...\n" + json);

                Players.instance.next_user = 0;
                Players.instance.card1Possible = null;
                Players.instance.card2Possible = null;
                Players.instance.card3Possible = null;

                // Send message to the server
                ConnectorScript.webSocket.Send(json);
            }
        }
    }

    public void Card2()
    {
        if (Players.instance.card1Possible == cards[2].spriteName || Players.instance.card2Possible == cards[2].spriteName
            || Players.instance.card3Possible == cards[2].spriteName)
        {
            if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) //сброс карты
            {
                // Create json to server
                SendMsg msgToSend = new SendMsg();

                msgToSend.action = "sendCard";
                msgToSend.game_id = Table.instance.game_id;
                msgToSend.table_id = Table.instance.table_id;
                msgToSend.card_name = cards[2].spriteName;
                msgToSend.sid = Profile.instance.sid;

                string json = JsonUtility.ToJson(msgToSend);

                print("Sending message...\n" + json);

                Players.instance.next_user = 0;
                Players.instance.card1Possible = null;
                Players.instance.card2Possible = null;
                Players.instance.card3Possible = null;

                // Send message to the server
                ConnectorScript.webSocket.Send(json);
            }
        }
    }

    public void Card3()
    {
        if (Players.instance.card1Possible == cards[3].spriteName || Players.instance.card2Possible == cards[3].spriteName
            || Players.instance.card3Possible == cards[3].spriteName)
        {
            if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) //сброс карты
            {
                // Create json to server
                SendMsg msgToSend = new SendMsg();

                msgToSend.action = "sendCard";
                msgToSend.game_id = Table.instance.game_id;
                msgToSend.table_id = Table.instance.table_id;
                msgToSend.card_name = cards[3].spriteName;
                msgToSend.sid = Profile.instance.sid;

                string json = JsonUtility.ToJson(msgToSend);

                print("Sending message...\n" + json);

                Players.instance.next_user = 0;
                Players.instance.card1Possible = null;
                Players.instance.card2Possible = null;
                Players.instance.card3Possible = null;

                // Send message to the server
                ConnectorScript.webSocket.Send(json);
            }
        }
    }

    public void knopkaB()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // поиск игры
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "place";
            msgToSend.sid = Profile.instance.sid;
            msgToSend.cash = 5;

            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void knopkaC()
    {
        GetGameUsers();
    }
}
