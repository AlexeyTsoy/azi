﻿public class SendMsg
{
    public string action;
    public string email;
    public string password;
    public string userName;
    public string sid;
    public string card_name;

    public int cash;
    public int table_id;
    public int game_id;
    public int user;
    public int place_id;
    public int bet;
    public int players_limit;
}

[System.Serializable]
public class Profile
{
    public static Profile instance = new Profile();

    public string email;
    public string password;
    public string sid;
}

public class Notification
{
    public static Notification instance = new Notification();

    public string error;
    public string notification;
}

[System.Serializable]
public class Table
{
    public static Table instance = new Table();

    public int table_id;
    public int players_count;
    public int place_row_id;
    public int place_id;
    public int game_id;
    public int turn;
    public int cash;
    public int bet_state;
    public int test;
}

[System.Serializable]
public class Players
{
    public static Players instance = new Players();

    public string trump;
    public string card_winner;
    public int round_winner;
    public int rWinnerCopy;
    public int start_game;
    public int next_user;
    public int next_bet_user;
    public int next_def_bet_user;
    public int bet;
    public int currentBet;
    public int winner;
    public int pot;
    public int bank;

    public string card1Possible;
    public string card2Possible;
    public string card3Possible;

    public int p1IsPlay;
    public int p1Balance;
    public string p1Name;
    public string p1Avatar;
    public int p1Place;
    public int p1Bet;
    public string p1Card1;
    public string p1Card2;
    public string p1Card3;

    public int p2IsPlay;
    public int p2Balance;
    public string p2Name;
    public string p2Avatar;
    public int p2Place;
    public int p2Bet;
    public string p2Card1;
    public string p2Card2;
    public string p2Card3;

    public int p3IsPlay;
    public int p3Balance;
    public string p3Name;
    public string p3Avatar;
    public int p3Place;
    public int p3Bet;
    public string p3Card1;
    public string p3Card2;
    public string p3Card3;

    public int p4IsPlay;
    public int p4Balance;
    public string p4Name;
    public string p4Avatar;
    public int p4Place;
    public int p4Bet;
    public string p4Card1;
    public string p4Card2;
    public string p4Card3;

    public int p5IsPlay;
    public int p5Balance;
    public string p5Name;
    public string p5Avatar;
    public int p5Place;
    public int p5Bet;
    public string p5Card1;
    public string p5Card2;
    public string p5Card3;

    public int p6IsPlay;
    public int p6Balance;
    public string p6Name;
    public string p6Avatar;
    public int p6Place;
    public int p6Bet;
    public string p6Card1;
    public string p6Card2;
    public string p6Card3;
}





