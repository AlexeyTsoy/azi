﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LobbyManagerScript : MonoBehaviour
{

    void Start ()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep; // что бы экран не засыпал
        Screen.orientation = ScreenOrientation.Portrait;
        Table.instance.table_id = 0;
        Table.instance.cash = 5;
    }
	
	void Update ()
    {
        if (Table.instance.table_id != 0)
        {
            SceneManager.LoadScene("Table");
        }
	}

    public void GameSearch()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // поиск игры
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "place";
            msgToSend.sid = Profile.instance.sid;
            msgToSend.cash = Table.instance.cash;
            Table.instance.test = 1;
            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void GameSearch2()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // поиск игры
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "place";
            msgToSend.sid = Profile.instance.sid;
            msgToSend.cash = Table.instance.cash;
            Table.instance.test = 2;
            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void GameSearch3()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // поиск игры
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "place";
            msgToSend.sid = Profile.instance.sid;
            msgToSend.cash = Table.instance.cash;
            Table.instance.test = 3;
            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void GameSearch4()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // поиск игры
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "place";
            msgToSend.sid = Profile.instance.sid;
            msgToSend.cash = Table.instance.cash;
            Table.instance.test = 4;
            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void GameSearch5()
    {
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen) // поиск игры
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "place";
            msgToSend.sid = Profile.instance.sid;
            msgToSend.cash = Table.instance.cash;
            Table.instance.test = 5;
            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }
}
