﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System;

public class MainScript : MonoBehaviour
{
    public UIInput emailField;
    public UIInput passField;

    public UIInput regEmailField;
    public UIInput regUserNameField;
    public UIInput regPassField;
    public UIInput regRePassField;

    public UIInput forgotPassField;

    public UILabel notification;
    public TweenPosition notificationTP;

    public GameObject rootButtons;
    public GameObject regPanel;
    public GameObject forgotPassPanel, loginPanel;


    UIButton[] allButtons;

    public Profile profile;

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep; // что бы экран не засыпал
        Screen.orientation = ScreenOrientation.Portrait;
        allButtons = rootButtons.GetComponentsInChildren<UIButton>();
        emailField.value = PlayerPrefs.GetString("email");
        //passField.value = PlayerPrefs.GetString("pass");
    }

    IEnumerator Fall()
    {
        yield return new WaitForSeconds(1f);
        for (byte i = 255; i > 10; i -= 10)
        {
            if (i < 10f)
            {
                i = 0;
            }
            //notification.color = new Color32(255, 255, 255, i);
            yield return null;
        }
        notification.text = null;
        //notification.color = new Color32(255, 255, 255, 255);
    }


    public void OnRegister()
    {
        Buttons();
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen)
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "reg";
            msgToSend.email = regEmailField.value;
            msgToSend.password = regPassField.value;
            msgToSend.userName = regUserNameField.value;

            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void OnLogin()
    {
        Buttons();
        if (ConnectorScript.webSocket != null && ConnectorScript.webSocket.IsOpen)
        {
            // Create json to server
            SendMsg msgToSend = new SendMsg();

            msgToSend.action = "auth";
            msgToSend.email = emailField.value;
            msgToSend.password = passField.value;

            string json = JsonUtility.ToJson(msgToSend);

            print("Sending message...\n");
      
            PlayerPrefs.SetString("email", emailField.value);
            //PlayerPrefs.SetString("pass", passField.value);

            // Send message to the server
            ConnectorScript.webSocket.Send(json);
        }
    }

    public void OnForgotPassword()
    {

    }

    public void OnResend()
    {
       
    }

    public void RegPanelStep1()
    {
        notificationTP.SetStartToCurrentValue();
        notificationTP.to = new Vector3(0f, 208f, 0f);
        notificationTP.PlayForward();
    }
    public void RegPanelStep2()
    {
        notificationTP.SetStartToCurrentValue();
        notificationTP.to = new Vector3(0f, 108f, 0f);
        notificationTP.PlayForward();
    }
    public void RegPanelStep3()
    {
        notificationTP.SetStartToCurrentValue();
        notificationTP.to = new Vector3(0f, -267f, 0f);
        notificationTP.PlayForward();
    }
    public void LoginPanelBack()
    {
        notificationTP.SetStartToCurrentValue();
        notificationTP.to = new Vector3(0f, 237f, 0f);
        notificationTP.PlayForward();
    }


    void Update()
    {
        if (ConnectorScript.errorMsg != null)
        {
            notification.text = ConnectorScript.errorMsg;
            ConnectorScript.errorMsg = null;
            Buttons();
            StartCoroutine(Fall());
        }
        else if (ConnectorScript.notificationMsg != null)
        {
            notification.text = ConnectorScript.notificationMsg;
            ConnectorScript.notificationMsg = null;
            StartCoroutine(Fall());
        }
        else if (Profile.instance.sid != null)
        {
            SceneManager.LoadScene("Lobby");
        }
    }

    void Buttons()
    {
        foreach (var button in allButtons)
        {
            if (!button.enabled)
            {
                button.enabled = true;
                button.ResetDefaultColor();
                print("Buttons_Enabled");
            }
            else
            {
                button.enabled = false;
                button.defaultColor = button.disabledColor;
                print("Buttons_Disabled");
            }
        }
    }
}