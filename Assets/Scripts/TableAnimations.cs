﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableAnimations : MonoBehaviour
{

    public GameObject chipP1GO, chipP2GO, chipP3GO, chipP4GO, chipP5GO, chipP6GO;
    public GameObject chipP1LBLGO, chipP2LBLGO, chipP3LBLGO, chipP4LBLGO, chipP5LBLGO, chipP6LBLGO;
    public GameObject seat1, seat2, seat3, seat4, seat5, seat6;
    public GameObject p1GO, p2GO, p3GO, p4GO, p5GO, p6GO;
    public GameObject betChangerGO, foldBTN, checkBTN, callBTN, raiseBTN;
    public UILabel chipP1LBL, chipP2LBL, chipP3LBL, chipP4LBL, chipP5LBL, chipP6LBL, betLBL, betBTNLBL;
    public UILabel p1Name, p2Name, p3Name, p4Name, p5Name, p6Name;
    public UILabel p1Bank, p2Bank, p3Bank, p4Bank, p5Bank, p6Bank, Pot;
    public TweenPosition p1c1, p1c2, p1c3, p2c1, p2c2, p2c3, p3c1, p3c2, p3c3, p4c1, p4c2, p4c3, p5c1, p5c2, p5c3, p6c1, p6c2, p6c3;
    public TweenPosition chipP1, chipP2, chipP3, chipP4, chipP5, chipP6;
    public TweenScale p1c1s, p1c2s, p1c3s;
    public UIButton p1c1BTN, p1c2BTN, p1c3BTN;
    public BoxCollider p1c1CLDR, p1c2CLDR, p1c3CLDR;
    public BoxCollider betChanger, betMainSprite;
    public UIGrid gridP1, gridP2, gridP3, gridP4, gridP5, gridP6;
    public UISprite p1c1Podsvetka, p1c2Podsvetka, p1c3Podsvetka;
    public UISprite p1Podsvetka, p2Podsvetka, p3Podsvetka, p4Podsvetka, p5Podsvetka, p6Podsvetka;
    public UIScrollBar betScrollBar;    
    public bool p1c1IsGiven, p1c2IsGiven, p1c3IsGiven, p2c1IsGiven, p2c2IsGiven, p2c3IsGiven, p3c1IsGiven, p3c2IsGiven, p3c3IsGiven, p4c1IsGiven, p4c2IsGiven, p4c3IsGiven, p5c1IsGiven, p5c2IsGiven, p5c3IsGiven, p6c1IsGiven, p6c2IsGiven, p6c3IsGiven;
    public bool p1c1Fixed, p1c2Fixed, p1c3Fixed, p2c1Fixed, p2c2Fixed, p2c3Fixed, p3c1Fixed, p3c2Fixed, p3c3Fixed, p4c1Fixed, p4c2Fixed, p4c3Fixed, p5c1Fixed, p5c2Fixed, p5c3Fixed, p6c1Fixed, p6c2Fixed, p6c3Fixed, utka;
    public bool nelzyaRazdatKarti, mojnoStavit, mojnoCheckBTNT, sobratFishki, vizvatMetodGGU;
    public bool p1c1Slot1, p1c2Slot1, p1c3Slot1, p2c1Slot1, p2c2Slot1, p2c3Slot1, p3c1Slot1, p3c2Slot1, p3c3Slot1, p4c1Slot1, p4c2Slot1, p4c3Slot1, p5c1Slot1, p5c2Slot1, p5c3Slot1, p6c1Slot1, p6c2Slot1, p6c3Slot1;
    public bool p1c1VoVzyatke, p1c2VoVzyatke, p1c3VoVzyatke, p2c1VoVzyatke, p2c2VoVzyatke, p2c3VoVzyatke, p3c1VoVzyatke, p3c2VoVzyatke, p3c3VoVzyatke, p4c1VoVzyatke, p4c2VoVzyatke, p4c3VoVzyatke, p5c1VoVzyatke, p5c2VoVzyatke, p5c3VoVzyatke, p6c1VoVzyatke, p6c2VoVzyatke, p6c3VoVzyatke, utka2;
    public bool vkluchitTimer, mojnoHodit, mojnoRazdatFishki, pervayaRazdachaFishek, mojnoObnulit, tiIgraew;
    public float timerPeredVzyatkoi, timerPeredObnuleniem, timerPosleObnuleniya, testt;
    //public bool p1c1Slot1, p1c2Slot1, p1c3Slot1, p2c1Slot1, p2c2Slot1,, p2c3Slot1, p3c1Slot1,, p3c2Slot1, p3c3Slot1, p4c1Slot1, p4c2Slot1, p4c3Slot1, p5c1Slot1, p5c2Slot2, p5c3Slot2, p6c1Slot2, p6c2Slot2, p6c3Slot2;
    //public bool p1c1Slot1, p1c2Slot1, p1c3Slot1, p2c1Slot1, p2c2Slot1,, p2c3Slot1, p3c1Slot1,, p3c2Slot1, p3c3Slot1, p4c1Slot1, p4c2Slot1, p4c3Slot1, p5c1Slot1, p5c2Slot3, p5c3Slot3, p6c1Slot3, p6c2Slot3, p6c3Slot3;
    //public bool p1c1Slot1, p1c2Slot1, p1c3Slot1, p2c1Slot1, p2c2Slot1,, p2c3Slot1, p3c1Slot1,, p3c2Slot1, p3c3Slot1, p4c1Slot1, p4c2Slot1, p4c3Slot1, p5c1Slot1, p5c2Slot4, p5c3Slot4, p6c1Slot4, p6c2Slot4, p6c3Slot4;
    //public bool p1c1Slot1, p1c2Slot1, p1c3Slot1, p2c1Slot1, p2c2Slot1,, p2c3Slot1, p3c1Slot1,, p3c2Slot1, p3c3Slot1, p4c1Slot1, p4c2Slot1, p4c3Slot1, p5c1Slot1, p5c2Slot5, p5c3Slot5, p6c1Slot5, p6c2Slot5, p6c3Slot5;
    //public bool p1c1Slot1, p1c2Slot1, p1c3Slot1, p2c1Slot1, p2c2Slot1,, p2c3Slot1, p3c1Slot1,, p3c2Slot1, p3c3Slot1, p4c1Slot1, p4c2Slot1, p4c3Slot1, p5c1Slot1, p5c2Slot6, p5c3Slot6, p6c1Slot6, p6c2Slot6, p6c3Slot6;
    public int ktoHodit;
    public Vector3 Slot1P1, Slot2P1, Slot3P1, Slot4P1, Slot5P1, Slot6P1;
    public Vector3 Slot1P2, Slot2P2, Slot3P2, Slot4P2, Slot5P2, Slot6P2;
    public Vector3 Slot1P3, Slot2P3, Slot3P3, Slot4P3, Slot5P3, Slot6P3;
    public Vector3 Slot1P4, Slot2P4, Slot3P4, Slot4P4, Slot5P4, Slot6P4;
    public Vector3 Slot1P5, Slot2P5, Slot3P5, Slot4P5, Slot5P5, Slot6P5;
    public Vector3 Slot1P6, Slot2P6, Slot3P6, Slot4P6, Slot5P6, Slot6P6;
    public List<TweenPosition> cardTweenPositions = new List<TweenPosition>();
    public List<bool> cardsAreFixedList = new List<bool>();
    public List<bool> cardsVoVzyatke = new List<bool>();
    public List<TweenScale> cardsTweenScales = new List<TweenScale>();

    void Awake()
    {
        if (Table.instance.place_id == 1 && Players.instance.p1IsPlay == 1)
            tiIgraew = true;
        if (Table.instance.place_id == 2 && Players.instance.p1IsPlay == 2)
            tiIgraew = true;
        if (Table.instance.place_id == 3 && Players.instance.p1IsPlay == 3)
            tiIgraew = true;
        if (Table.instance.place_id == 4 && Players.instance.p1IsPlay == 4)
            tiIgraew = true;
        if (Table.instance.place_id == 5 && Players.instance.p1IsPlay == 5)
            tiIgraew = true;
        if (Table.instance.place_id == 6 && Players.instance.p1IsPlay == 6)
            tiIgraew = true;
        if (!tiIgraew)
            Players.instance.start_game = 0;
        Table.instance.bet_state = 1;
    }  

        // Use this for initialization
    void Start()
    {

        sobratFishki = true;
        vizvatMetodGGU = false;
        //Изменить
        mojnoStavit = true;

        betScrollBar.value = 0;
        

        ktoHodit = 1;

        vkluchitTimer = true;

        pervayaRazdachaFishek = true;

        Slot1P1 = new Vector3(-147.6f, 126.3f, 0f);
        Slot2P1 = new Vector3(-82f, 126.3f, 0f);
        Slot3P1 = new Vector3(-17.6f, 126.3f, 0f);
        Slot4P1 = new Vector3(47.4f, 126.3f, 0f);
        Slot5P1 = new Vector3(113f, 126.3f, 0f);
        Slot6P1 = new Vector3(178f, 126.3f, 0f);

        Slot1P2 = new Vector3(298f, 48.8f, 0f);
        Slot2P2 = new Vector3(362.4f, 48.8f, 0f);
        Slot3P2 = new Vector3(428f, 48.8f, 0f);
        Slot4P2 = new Vector3(491.7f, 48.8f, 0f);
        Slot5P2 = new Vector3(557.3f, 48.8f, 0f);
        Slot6P2 = new Vector3(621.7f, 48.8f, 0f);

        Slot1P3 = new Vector3(298f, -141.3f, 0f);
        Slot2P3 = new Vector3(362.4f, -141.3f, 0f);
        Slot3P3 = new Vector3(428f, -141.3f, 0f);
        Slot4P3 = new Vector3(491.7f, -141.3f, 0f);
        Slot5P3 = new Vector3(557.3f, -141.3f, 0f);
        Slot6P3 = new Vector3(621.7f, -141.3f, 0f);

        Slot1P4 = new Vector3(-147.6f, -243.8f, 0f);
        Slot2P4 = new Vector3(-82f, -243.8f, 0f);
        Slot3P4 = new Vector3(-17.6f, -243.8f, 0f);
        Slot4P4 = new Vector3(47.4f, -243.8f, 0f);
        Slot5P4 = new Vector3(113f, -243.8f, 0f);
        Slot6P4 = new Vector3(178f, -243.8f, 0f);

        Slot1P5 = new Vector3(-621.2f, -140.7f, 0f);
        Slot2P5 = new Vector3(-556f, -140.7f, 0f);
        Slot3P5 = new Vector3(-490.4f, -140.7f, 0f);
        Slot4P5 = new Vector3(-425.9f, -140.7f, 0f);
        Slot5P5 = new Vector3(-361.1f, -140.7f, 0f);
        Slot6P5 = new Vector3(-295.9f, -140.7f, 0f);

        Slot1P6 = new Vector3(-621.2f, 49.7f, 0f);
        Slot2P6 = new Vector3(-556f, 49.7f, 0f);
        Slot3P6 = new Vector3(-490.4f, 49.7f, 0f);
        Slot4P6 = new Vector3(-425.9f, 49.7f, 0f);
        Slot5P6 = new Vector3(-361.1f, 49.7f, 0f);
        Slot6P6 = new Vector3(-295.9f, 49.7f, 0f);

        cardsAreFixedList.Add(utka);
        cardsAreFixedList.Add(p1c1Fixed);
        cardsAreFixedList.Add(p1c2Fixed);
        cardsAreFixedList.Add(p1c3Fixed);
        cardsAreFixedList.Add(p2c1Fixed);
        cardsAreFixedList.Add(p2c2Fixed);
        cardsAreFixedList.Add(p2c3Fixed);
        cardsAreFixedList.Add(p3c1Fixed);
        cardsAreFixedList.Add(p3c2Fixed);
        cardsAreFixedList.Add(p3c3Fixed);
        cardsAreFixedList.Add(p4c1Fixed);
        cardsAreFixedList.Add(p4c2Fixed);
        cardsAreFixedList.Add(p4c3Fixed);
        cardsAreFixedList.Add(p5c1Fixed);
        cardsAreFixedList.Add(p5c2Fixed);
        cardsAreFixedList.Add(p5c3Fixed);
        cardsAreFixedList.Add(p6c1Fixed);
        cardsAreFixedList.Add(p6c2Fixed);
        cardsAreFixedList.Add(p6c3Fixed);

        cardsVoVzyatke.Add(utka2);
        cardsVoVzyatke.Add(p1c1VoVzyatke);
        cardsVoVzyatke.Add(p1c2VoVzyatke);
        cardsVoVzyatke.Add(p1c3VoVzyatke);
        cardsVoVzyatke.Add(p2c1VoVzyatke);
        cardsVoVzyatke.Add(p2c2VoVzyatke);
        cardsVoVzyatke.Add(p2c3VoVzyatke);
        cardsVoVzyatke.Add(p3c1VoVzyatke);
        cardsVoVzyatke.Add(p3c2VoVzyatke);
        cardsVoVzyatke.Add(p3c3VoVzyatke);
        cardsVoVzyatke.Add(p4c1VoVzyatke);
        cardsVoVzyatke.Add(p4c2VoVzyatke);
        cardsVoVzyatke.Add(p4c3VoVzyatke);
        cardsVoVzyatke.Add(p5c1VoVzyatke);
        cardsVoVzyatke.Add(p5c2VoVzyatke);
        cardsVoVzyatke.Add(p5c3VoVzyatke);
        cardsVoVzyatke.Add(p6c1VoVzyatke);
        cardsVoVzyatke.Add(p6c2VoVzyatke);
        cardsVoVzyatke.Add(p6c3VoVzyatke);


    }

    // Update is called once per frame
    void Update()
    {
        if(Table.instance.bet_state == 0)
        {
            Players.instance.p1Bet = 0;
            Players.instance.p2Bet = 0;
            Players.instance.p3Bet = 0;
            Players.instance.p4Bet = 0;
            Players.instance.p5Bet = 0;
            Players.instance.p6Bet = 0;
        }

        testt = (float)(((float)Players.instance.currentBet * 2) / (float)Players.instance.p2Balance);

        timerPosleObnuleniya -= Time.deltaTime;
        timerPeredObnuleniem -= Time.deltaTime;
        timerPeredVzyatkoi -= Time.deltaTime;
        if (Players.instance.round_winner > 0 && vkluchitTimer)
        {
            timerPeredVzyatkoi = 1f;
            vkluchitTimer = false;
        }

        if (Players.instance.round_winner > 0 && timerPeredVzyatkoi <= 0)
        {
            for (int i = 1; i < cardsTweenScales.Count; i++)
            {
                cardsTweenScales[i].SetStartToCurrentValue();
                cardsTweenScales[i].to = new Vector3(0.25f, 0.25f, 0.25f);
            }
            VzyatkaAnimation();
            for (int i = 1; i < cardsTweenScales.Count; i++)
            {
                if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                {
                    cardsTweenScales[i].PlayForward();
                    cardsVoVzyatke[i] = true;
                }
            }

        }


        if (Players.instance.start_game == 0 && Players.instance.winner > 0)
        {
            timerPeredObnuleniem = 3f;
            mojnoObnulit = true;
            Players.instance.winner = 0;
        }

        if (mojnoObnulit && timerPeredObnuleniem<0)
        {
            Obnulit();
            
            mojnoObnulit = false;
        }
        if (timerPosleObnuleniya <= 0 && vizvatMetodGGU && Table.instance.place_id == 1)
        {
            GameManagerScript.instance.knopkaC();
            vizvatMetodGGU = false;
        }
        CheckChips();

        if (Table.instance.place_id == 1)
        {
            HodAnimationPid1();
        }
        if (Table.instance.place_id == 2)
        {
            HodAnimationPid2();
        }
        if (Table.instance.place_id == 3)
        {
            HodAnimationPid3();
        }
        if (Table.instance.place_id == 4)
        {
            HodAnimationPid4();
        }
        if (Table.instance.place_id == 5)
        {
            HodAnimationPid5();
        }
        if (Table.instance.place_id == 6)
        {
            HodAnimationPid6();
        }

        if (Players.instance.start_game == 1)
        {
            if (Players.instance.round_winner > 0)
                Players.instance.rWinnerCopy = Players.instance.round_winner;

            PodsvetkaIgroka();

            SetChipsTPDurationOnReverse();
            CheckBets();

            checkBetButtons();

            Pot.text = (Players.instance.pot + Players.instance.bank).ToString();

            if (mojnoStavit)
            {
                betChanger.enabled = true;
                betMainSprite.enabled = true;
                BetLabelsValue();
            }
            else
            {
                betChanger.enabled = false;
                betMainSprite.enabled = false;
            }

           
            GameManagerScript.instance.cards[0].spriteName = Players.instance.trump;



            

            if (Players.instance.card1Possible == GameManagerScript.instance.cards[1].spriteName || Players.instance.card2Possible == GameManagerScript.instance.cards[1].spriteName || Players.instance.card3Possible == GameManagerScript.instance.cards[1].spriteName)
            {
                if (mojnoHodit && Table.instance.place_id == Players.instance.next_user && Table.instance.bet_state == 0)
                {
                    p1c1Podsvetka.enabled = true;
                    p1c1CLDR.enabled = true;
                }
            }
            else
            {
                p1c1Podsvetka.enabled = false;
                p1c1CLDR.enabled = false;
            }
            if (Players.instance.card1Possible == GameManagerScript.instance.cards[2].spriteName || Players.instance.card2Possible == GameManagerScript.instance.cards[2].spriteName || Players.instance.card3Possible == GameManagerScript.instance.cards[2].spriteName)
            {
                if (mojnoHodit && Table.instance.place_id == Players.instance.next_user && Table.instance.bet_state == 0)
                {
                    p1c2Podsvetka.enabled = true;
                    p1c2CLDR.enabled = true;
                }
            }
            else
            {
                p1c2Podsvetka.enabled = false;
                p1c2CLDR.enabled = false;
            }
            if (Players.instance.card1Possible == GameManagerScript.instance.cards[3].spriteName || Players.instance.card2Possible == GameManagerScript.instance.cards[3].spriteName || Players.instance.card3Possible == GameManagerScript.instance.cards[3].spriteName)
            {
                if (mojnoHodit && Table.instance.place_id == Players.instance.next_user && Table.instance.bet_state == 0)
                {
                    p1c3Podsvetka.enabled = true;
                    p1c3CLDR.enabled = true;
                }
            }
            else
            {
                p1c3Podsvetka.enabled = false;
                p1c3CLDR.enabled = false;
            }
            RazdachaCart();


            
            if (Players.instance.round_winner > 0)
            {
                mojnoHodit = false;
            }
            else
            {
                mojnoHodit = true;
            }

            
            
            if (pervayaRazdachaFishek)
            {
                chipP1LBL.text = Table.instance.cash.ToString();
                chipP2LBL.text = Table.instance.cash.ToString();
                chipP3LBL.text = Table.instance.cash.ToString();
                chipP4LBL.text = Table.instance.cash.ToString();
                chipP5LBL.text = Table.instance.cash.ToString();
                chipP6LBL.text = Table.instance.cash.ToString();
            }
            if (Players.instance.start_game == 1 && pervayaRazdachaFishek)
            {
                mojnoRazdatFishki = true;
                pervayaRazdachaFishek = false;
            }
            if (mojnoRazdatFishki)
            {
                RazdatFishki();
                mojnoRazdatFishki = false;
            }
            if(Table.instance.bet_state == 0 && sobratFishki == true)
            {
                RazdatFishki();
                if (Players.instance.next_user == Table.instance.place_id)
                    GameManagerScript.instance.GetPossibleCards();
                sobratFishki = false;
                print("vizvano iz update");
            }
            
        }  
        
    }
    public void HodP1C1()
    {
        cardsTweenScales[1].from = new Vector3(0.5f, 0.5f, 0.5f);
        cardsTweenScales[1].SetEndToCurrentValue();
        if (!cardsAreFixedList[1])
        {
            if (Table.instance.turn == 1)
            {
                p1c1.from = new Vector3(-121.9f, -130.3f, 0f);
                p1c1.to = Slot1P1;
                p1c1.ResetToBeginning();p1c1.PlayForward();
                p1c1s.PlayReverse();
            }
            if (Table.instance.turn == 2)
            {
                p1c1.from = new Vector3(-121.9f, -130.3f, 0f);
                p1c1.to = Slot2P1;
                p1c1.ResetToBeginning();p1c1.PlayForward();
                p1c1s.PlayReverse();
            }
            if (Table.instance.turn == 3)
            {
                p1c1.from = new Vector3(-121.9f, -130.3f, 0f);
                p1c1.to = Slot3P1;
                p1c1.ResetToBeginning();p1c1.PlayForward();
                p1c1s.PlayReverse();
            }
            if (Table.instance.turn == 4)
            {
                p1c1.from = new Vector3(-121.9f, -130.3f, 0f);
                p1c1.to = Slot4P1;
                p1c1.ResetToBeginning();p1c1.PlayForward();
                p1c1s.PlayReverse();
            }
            if (Table.instance.turn == 5)
            {
                p1c1.from = new Vector3(-121.9f, -130.3f, 0f);
                p1c1.to = Slot5P1;
                p1c1.ResetToBeginning();p1c1.PlayForward();
                p1c1s.PlayReverse();
            }
            if (Table.instance.turn == 6)
            {
                p1c1.from = new Vector3(-121.9f, -130.3f, 0f);
                p1c1.to = Slot6P1;
                p1c1.ResetToBeginning();p1c1.PlayForward();
                p1c1s.PlayReverse();
            }
            cardsAreFixedList[1] = true;
        }
        
        p1c1CLDR.enabled = false;
        p1c2CLDR.enabled = false;
        p1c3CLDR.enabled = false;
        p1c1Podsvetka.enabled = false;
        p1c2Podsvetka.enabled = false;
        p1c3Podsvetka.enabled = false;
    }
    public void HodP1C2()
    {
        cardsTweenScales[2].from = new Vector3(0.5f, 0.5f, 0.5f);
        cardsTweenScales[2].SetEndToCurrentValue();
        if (!cardsAreFixedList[2])
        {
            if (Table.instance.turn == 1)
            {
                p1c2.from = new Vector3(15.4f, -130.3f, 0f);
                p1c2.to = Slot1P1;
                p1c2.ResetToBeginning();p1c2.PlayForward();
                p1c2s.PlayReverse();
            }
            if (Table.instance.turn == 2)
            {
                p1c2.from = new Vector3(15.4f, -130.3f, 0f);
                p1c2.to = Slot2P1;
                p1c2.ResetToBeginning();p1c2.PlayForward();
                p1c2s.PlayReverse();
            }
            if (Table.instance.turn == 3)
            {
                p1c2.from = new Vector3(15.4f, -130.3f, 0f);
                p1c2.to = Slot3P1;
                p1c2.ResetToBeginning();p1c2.PlayForward();
                p1c2s.PlayReverse();
            }
            if (Table.instance.turn == 4)
            {
                p1c2.from = new Vector3(15.4f, -130.3f, 0f);
                p1c2.to = Slot4P1;
                p1c2.ResetToBeginning();p1c2.PlayForward();
                p1c2s.PlayReverse();
            }
            if (Table.instance.turn == 5)
            {
                p1c2.from = new Vector3(15.4f, -130.3f, 0f);
                p1c2.to = Slot5P1;
                p1c2.ResetToBeginning();p1c2.PlayForward();
                p1c2s.PlayReverse();
            }
            if (Table.instance.turn == 6)
            {
                p1c2.from = new Vector3(15.4f, -130.3f, 0f);
                p1c2.to = Slot6P1;
                p1c2.ResetToBeginning();p1c2.PlayForward();
                p1c2s.PlayReverse();
            }
            cardsAreFixedList[2] = true;
        }
        p1c1CLDR.enabled = false;
        p1c2CLDR.enabled = false;
        p1c3CLDR.enabled = false;
        p1c1Podsvetka.enabled = false;
        p1c2Podsvetka.enabled = false;
        p1c3Podsvetka.enabled = false;
    }
    public void HodP1C3()
    {
        cardsTweenScales[3].from = new Vector3(0.5f, 0.5f, 0.5f);
        cardsTweenScales[3].SetEndToCurrentValue();
        if (!cardsAreFixedList[3])
        {
            if (Table.instance.turn == 1)
            {
                p1c3.from = new Vector3(151.1f, -130.3f, 0f);
                p1c3.to = Slot1P1;
                p1c3.ResetToBeginning();p1c3.PlayForward();
                p1c3s.PlayReverse();
            }
            if (Table.instance.turn == 2)
            {
                p1c3.from = new Vector3(151.1f, -130.3f, 0f);
                p1c3.to = Slot2P1;
                p1c3.ResetToBeginning();p1c3.PlayForward();
                p1c3s.PlayReverse();
            }
            if (Table.instance.turn == 3)
            {
                p1c3.from = new Vector3(151.1f, -130.3f, 0f);
                p1c3.to = Slot3P1;
                p1c3.ResetToBeginning();p1c3.PlayForward();
                p1c3s.PlayReverse();
            }
            if (Table.instance.turn == 4)
            {
                p1c3.from = new Vector3(151.1f, -130.3f, 0f);
                p1c3.to = Slot4P1;
                p1c3.ResetToBeginning();p1c3.PlayForward();
                p1c3s.PlayReverse();
            }
            if (Table.instance.turn == 5)
            {
                p1c3.from = new Vector3(151.1f, -130.3f, 0f);
                p1c3.to = Slot5P1;
                p1c3.ResetToBeginning();p1c3.PlayForward();
                p1c3s.PlayReverse();
            }
            if (Table.instance.turn == 6)
            {
                p1c3.from = new Vector3(151.1f, -130.3f, 0f);
                p1c3.to = Slot6P1;
                p1c3.ResetToBeginning();p1c3.PlayForward();
                p1c3s.PlayReverse();
            }
            cardsAreFixedList[3] = true;
        }
        p1c1CLDR.enabled = false;
        p1c2CLDR.enabled = false;
        p1c3CLDR.enabled = false;
        p1c1Podsvetka.enabled = false;
        p1c2Podsvetka.enabled = false;
        p1c3Podsvetka.enabled = false;

    }
    public void HodP2C1()
    {
        if (!cardsAreFixedList[4])
        {
            if (Table.instance.turn == 1)
            {
                p2c1.SetStartToCurrentValue();
                p2c1.to = Slot1P2;
                p2c1.ResetToBeginning();p2c1.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p2c1.SetStartToCurrentValue();
                p2c1.to = Slot2P2;
                p2c1.ResetToBeginning();p2c1.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p2c1.SetStartToCurrentValue();
                p2c1.to = Slot3P2;
                p2c1.ResetToBeginning();p2c1.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p2c1.SetStartToCurrentValue();
                p2c1.to = Slot4P2;
                p2c1.ResetToBeginning();p2c1.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p2c1.SetStartToCurrentValue();
                p2c1.to = Slot5P2;
                p2c1.ResetToBeginning();p2c1.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p2c1.SetStartToCurrentValue();
                p2c1.to = Slot6P2;
                p2c1.ResetToBeginning();p2c1.PlayForward();
            }
            if(Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[4] = true;
        }
    }
    public void HodP2C2()
    {
        if (!cardsAreFixedList[5])
        {
            if (Table.instance.turn == 1)
            {
                p2c2.SetStartToCurrentValue();
                p2c2.to = Slot1P2;
                p2c2.ResetToBeginning();p2c2.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p2c2.SetStartToCurrentValue();
                p2c2.to = Slot2P2;
                p2c2.ResetToBeginning();p2c2.PlayForward();
                
            }
            if (Table.instance.turn == 3)
            {
                p2c2.SetStartToCurrentValue();
                p2c2.to = Slot3P2;
                p2c2.ResetToBeginning();p2c2.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p2c2.SetStartToCurrentValue();
                p2c2.to = Slot4P2;
                p2c2.ResetToBeginning();p2c2.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p2c2.SetStartToCurrentValue();
                p2c2.to = Slot5P2;
                p2c2.ResetToBeginning();p2c2.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p2c2.SetStartToCurrentValue();
                p2c2.to = Slot6P2;
                p2c2.ResetToBeginning();p2c2.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[5] = true;
        }
    }
    public void HodP2C3()
    {
        if (!cardsAreFixedList[6])
        {
            if (Table.instance.turn == 1)
            {
                p2c3.SetStartToCurrentValue();
                p2c3.to = Slot1P2;
                p2c3.ResetToBeginning();p2c3.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p2c3.SetStartToCurrentValue();
                p2c3.to = Slot2P2;
                p2c3.ResetToBeginning();p2c3.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p2c3.SetStartToCurrentValue();
                p2c3.to = Slot3P2;
                p2c3.ResetToBeginning();p2c3.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p2c3.SetStartToCurrentValue();
                p2c3.to = Slot4P2;
                p2c3.ResetToBeginning();p2c3.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p2c3.SetStartToCurrentValue();
                p2c3.to = Slot5P2;
                p2c3.ResetToBeginning();p2c3.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p2c3.SetStartToCurrentValue();
                p2c3.to = Slot6P2;
                p2c3.ResetToBeginning();p2c3.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[6] = true;
        }
    }
    public void HodP3C1()
    {
        if (!cardsAreFixedList[7])
        {
            if (Table.instance.turn == 1)
            {
                p3c1.SetStartToCurrentValue();
                p3c1.to = Slot1P3;
                p3c1.ResetToBeginning();p3c1.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p3c1.SetStartToCurrentValue();
                p3c1.to = Slot2P3;
                p3c1.ResetToBeginning();p3c1.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p3c1.SetStartToCurrentValue();
                p3c1.to = Slot3P3;
                p3c1.ResetToBeginning();p3c1.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p3c1.SetStartToCurrentValue();
                p3c1.to = Slot4P3;
                p3c1.ResetToBeginning();p3c1.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p3c1.SetStartToCurrentValue();
                p3c1.to = Slot5P3;
                p3c1.ResetToBeginning();p3c1.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p3c1.SetStartToCurrentValue();
                p3c1.to = Slot6P3;
                p3c1.ResetToBeginning();p3c1.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[7] = true;
        }
    }
    public void HodP3C2()
    {
        if (!cardsAreFixedList[8])
        {
            if (Table.instance.turn == 1)
            {
                p3c2.SetStartToCurrentValue();
                p3c2.to = Slot1P3;
                p3c2.ResetToBeginning();p3c2.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p3c2.SetStartToCurrentValue();
                p3c2.to = Slot2P3;
                p3c2.ResetToBeginning();p3c2.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p3c2.SetStartToCurrentValue();
                p3c2.to = Slot3P3;
                p3c2.ResetToBeginning();p3c2.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p3c2.SetStartToCurrentValue();
                p3c2.to = Slot4P3;
                p3c2.ResetToBeginning();p3c2.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p3c2.SetStartToCurrentValue();
                p3c2.to = Slot5P3;
                p3c2.ResetToBeginning();p3c2.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p3c2.SetStartToCurrentValue();
                p3c2.to = Slot6P3;
                p3c2.ResetToBeginning();p3c2.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[8] = true;
        }
    }
    public void HodP3C3()
    {
        if (!cardsAreFixedList[9])
        {
            if (Table.instance.turn == 1)
            {
                p3c3.SetStartToCurrentValue();
                p3c3.to = Slot1P3;
                p3c3.ResetToBeginning();p3c3.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p3c3.SetStartToCurrentValue();
                p3c3.to = Slot2P3;
                p3c3.ResetToBeginning();p3c3.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p3c3.SetStartToCurrentValue();
                p3c3.to = Slot3P3;
                p3c3.ResetToBeginning();p3c3.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p3c3.SetStartToCurrentValue();
                p3c3.to = Slot4P3;
                p3c3.ResetToBeginning();p3c3.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p3c3.SetStartToCurrentValue();
                p3c3.to = Slot5P3;
                p3c3.ResetToBeginning();p3c3.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p3c3.SetStartToCurrentValue();
                p3c3.to = Slot6P3;
                p3c3.ResetToBeginning();p3c3.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[9] = true;
        }
    }
    public void HodP4C1()
    {
        if (!cardsAreFixedList[10])
        {
            if (Table.instance.turn == 1)
            {
                p4c1.SetStartToCurrentValue();
                p4c1.to = Slot1P4;
                p4c1.ResetToBeginning();p4c1.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p4c1.SetStartToCurrentValue();
                p4c1.to = Slot2P4;
                p4c1.ResetToBeginning();p4c1.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p4c1.SetStartToCurrentValue();
                p4c1.to = Slot3P4;
                p4c1.ResetToBeginning();p4c1.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p4c1.SetStartToCurrentValue();
                p4c1.to = Slot4P4;
                p4c1.ResetToBeginning();p4c1.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p4c1.SetStartToCurrentValue();
                p4c1.to = Slot5P4;
                p4c1.ResetToBeginning();p4c1.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p4c1.SetStartToCurrentValue();
                p4c1.to = Slot6P4;
                p4c1.ResetToBeginning();p4c1.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[10] = true;
        }
    }
    public void HodP4C2()
    {
        if (!cardsAreFixedList[11])
        {
            if (Table.instance.turn == 1)
            {
                p4c2.SetStartToCurrentValue();
                p4c2.to = Slot1P4;
                p4c2.ResetToBeginning();p4c2.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p4c2.SetStartToCurrentValue();
                p4c2.to = Slot2P4;
                p4c2.ResetToBeginning();p4c2.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p4c2.SetStartToCurrentValue();
                p4c2.to = Slot3P4;
                p4c2.ResetToBeginning();p4c2.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p4c2.SetStartToCurrentValue();
                p4c2.to = Slot4P4;
                p4c2.ResetToBeginning();p4c2.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p4c2.SetStartToCurrentValue();
                p4c2.to = Slot5P4;
                p4c2.ResetToBeginning();p4c2.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p4c2.SetStartToCurrentValue();
                p4c2.to = Slot6P4;
                p4c2.ResetToBeginning();p4c2.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[11] = true;
        }
    }
    public void HodP4C3()
    {
        if (!cardsAreFixedList[12])
        {
            if (Table.instance.turn == 1)
            {
                p4c3.SetStartToCurrentValue();
                p4c3.to = Slot1P4;
                p4c3.ResetToBeginning();p4c3.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p4c3.SetStartToCurrentValue();
                p4c3.to = Slot2P4;
                p4c3.ResetToBeginning();p4c3.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p4c3.SetStartToCurrentValue();
                p4c3.to = Slot3P4;
                p4c3.ResetToBeginning();p4c3.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p4c3.SetStartToCurrentValue();
                p4c3.to = Slot4P4;
                p4c3.ResetToBeginning();p4c3.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p4c3.SetStartToCurrentValue();
                p4c3.to = Slot5P4;
                p4c3.ResetToBeginning();p4c3.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p4c3.SetStartToCurrentValue();
                p4c3.to = Slot6P4;
                p4c3.ResetToBeginning();p4c3.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[12] = true;
        }
    }
    public void HodP5C1()
    {
        if (!cardsAreFixedList[13])
        {
            if (Table.instance.turn == 1)
            {
                p5c1.SetStartToCurrentValue();
                p5c1.to = Slot1P5;
                p5c1.ResetToBeginning();p5c1.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p5c1.SetStartToCurrentValue();
                p5c1.to = Slot2P5;
                p5c1.ResetToBeginning();p5c1.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p5c1.SetStartToCurrentValue();
                p5c1.to = Slot3P5;
                p5c1.ResetToBeginning();p5c1.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p5c1.SetStartToCurrentValue();
                p5c1.to = Slot4P5;
                p5c1.ResetToBeginning();p5c1.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p5c1.SetStartToCurrentValue();
                p5c1.to = Slot5P5;
                p5c1.ResetToBeginning();p5c1.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p5c1.SetStartToCurrentValue();
                p5c1.to = Slot6P5;
                p5c1.ResetToBeginning();p5c1.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[13] = true;
        }
    }
    public void HodP5C2()
    {
        if (!cardsAreFixedList[14])
        {
            if (Table.instance.turn == 1)
            {
                p5c2.SetStartToCurrentValue();
                p5c2.to = Slot1P5;
                p5c2.ResetToBeginning();p5c2.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p5c2.SetStartToCurrentValue();
                p5c2.to = Slot2P5;
                p5c2.ResetToBeginning();p5c2.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p5c2.SetStartToCurrentValue();
                p5c2.to = Slot3P5;
                p5c2.ResetToBeginning();p5c2.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p5c2.SetStartToCurrentValue();
                p5c2.to = Slot4P5;
                p5c2.ResetToBeginning();p5c2.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p5c2.SetStartToCurrentValue();
                p5c2.to = Slot5P5;
                p5c2.ResetToBeginning();p5c2.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p5c2.SetStartToCurrentValue();
                p5c2.to = Slot6P5;
                p5c2.ResetToBeginning();p5c2.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[14] = true;
        }
    }
    public void HodP5C3()
    {
        if (!cardsAreFixedList[15])
        {
            if (Table.instance.turn == 1)
            {
                p5c3.SetStartToCurrentValue();
                p5c3.to = Slot1P5;
                p5c3.ResetToBeginning();p5c3.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p5c3.SetStartToCurrentValue();
                p5c3.to = Slot2P5;
                p5c3.ResetToBeginning();p5c3.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p5c3.SetStartToCurrentValue();
                p5c3.to = Slot3P5;
                p5c3.ResetToBeginning();p5c3.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p5c3.SetStartToCurrentValue();
                p5c3.to = Slot4P5;
                p5c3.ResetToBeginning();p5c3.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p5c3.SetStartToCurrentValue();
                p5c3.to = Slot5P5;
                p5c3.ResetToBeginning();p5c3.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p5c3.SetStartToCurrentValue();
                p5c3.to = Slot6P5;
                p5c3.ResetToBeginning();p5c3.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[15] = true;
        }
    }
    public void HodP6C1()
    {
        if (!cardsAreFixedList[16])
        {
            if (Table.instance.turn == 1)
            {
                p6c1.SetStartToCurrentValue();
                p6c1.to = Slot1P6;
                p6c1.ResetToBeginning();p6c1.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p6c1.SetStartToCurrentValue();
                p6c1.to = Slot2P6;
                p6c1.ResetToBeginning();p6c1.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p6c1.SetStartToCurrentValue();
                p6c1.to = Slot3P6;
                p6c1.ResetToBeginning();p6c1.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p6c1.SetStartToCurrentValue();
                p6c1.to = Slot4P6;
                p6c1.ResetToBeginning();p6c1.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p6c1.SetStartToCurrentValue();
                p6c1.to = Slot5P6;
                p6c1.ResetToBeginning();p6c1.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p6c1.SetStartToCurrentValue();
                p6c1.to = Slot6P6;
                p6c1.ResetToBeginning();p6c1.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[16] = true;
        }
    }
    public void HodP6C2()
    {
        if (!cardsAreFixedList[17])
        {
            if (Table.instance.turn == 1)
            {
                p6c2.SetStartToCurrentValue();
                p6c2.to = Slot1P6;
                p6c2.ResetToBeginning();p6c2.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p6c2.SetStartToCurrentValue();
                p6c2.to = Slot2P6;
                p6c2.ResetToBeginning();p6c2.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p6c2.SetStartToCurrentValue();
                p6c2.to = Slot3P6;
                p6c2.ResetToBeginning();p6c2.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p6c2.SetStartToCurrentValue();
                p6c2.to = Slot4P6;
                p6c2.ResetToBeginning();p6c2.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p6c2.SetStartToCurrentValue();
                p6c2.to = Slot5P6;
                p6c2.ResetToBeginning();p6c2.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p6c2.SetStartToCurrentValue();
                p6c2.to = Slot6P6;
                p6c2.ResetToBeginning();p6c2.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[17] = true;
        }
    }
    public void HodP6C3()
    {
        if (!cardsAreFixedList[18])
        {
            if (Table.instance.turn == 1)
            {
                p6c3.SetStartToCurrentValue();
                p6c3.to = Slot1P6;
                p6c3.ResetToBeginning();p6c3.PlayForward();
            }
            if (Table.instance.turn == 2)
            {
                p6c3.SetStartToCurrentValue();
                p6c3.to = Slot2P6;
                p6c3.ResetToBeginning();p6c3.PlayForward();
            }
            if (Table.instance.turn == 3)
            {
                p6c3.SetStartToCurrentValue();
                p6c3.to = Slot3P6;
                p6c3.ResetToBeginning();p6c3.PlayForward();
            }
            if (Table.instance.turn == 4)
            {
                p6c3.SetStartToCurrentValue();
                p6c3.to = Slot4P6;
                p6c3.ResetToBeginning();p6c3.PlayForward();
            }
            if (Table.instance.turn == 5)
            {
                p6c3.SetStartToCurrentValue();
                p6c3.to = Slot5P6;
                p6c3.ResetToBeginning();p6c3.PlayForward();
            }
            if (Table.instance.turn == 6)
            {
                p6c3.SetStartToCurrentValue();
                p6c3.to = Slot6P6;
                p6c3.ResetToBeginning();p6c3.PlayForward();
            }
            if (Table.instance.place_id == Players.instance.next_user)
                GameManagerScript.instance.GetPossibleCards();
            cardsAreFixedList[18] = true;
        }
    }
    public void RazdachaCart()
    {
        if (nelzyaRazdatKarti == false && Table.instance.place_id == 1 && Players.instance.p1Card1 != "0" && !string.IsNullOrEmpty(Players.instance.p1Card1) && Players.instance.p1Card2 != "0" && !string.IsNullOrEmpty(Players.instance.p1Card2) && Players.instance.p1Card3 != "0" && !string.IsNullOrEmpty(Players.instance.p1Card3))
        {
            for (int i = 0; i < cardTweenPositions.Count; i++)
            {
                cardTweenPositions[i].duration = 0.1f;
            }
            for (int i = 1; i < 4; i++)
            {
                cardsTweenScales[i].SetStartToCurrentValue();
                cardsTweenScales[i].to = new Vector3(1f, 1f, 1f);
            }
            
            if (ktoHodit == 1)
            {
                p1c1TweenPos();
                
            }
            if (ktoHodit == 2)
            {
                p2c1TweenPos();
            }
            if (ktoHodit == 3)
            {
                p3c1TweenPos();
            }
            if (ktoHodit == 4)
            {
                p4c1TweenPos();
            }
            if (ktoHodit == 5)
            {
                p5c1TweenPos();
            }
            if (ktoHodit == 6)
            {
                p6c1TweenPos();
            }

            p1Name.text = Players.instance.p1Name;
            p1Bank.text = Players.instance.p1Balance.ToString();
            p2Name.text = Players.instance.p2Name;
            p2Bank.text = Players.instance.p2Balance.ToString();
            p3Name.text = Players.instance.p3Name;
            p3Bank.text = Players.instance.p3Balance.ToString();
            p4Name.text = Players.instance.p4Name;
            p4Bank.text = Players.instance.p4Balance.ToString();
            p5Name.text = Players.instance.p5Name;
            p5Bank.text = Players.instance.p5Balance.ToString();
            p6Name.text = Players.instance.p6Name;
            p6Bank.text = Players.instance.p6Balance.ToString();

            nelzyaRazdatKarti = true;
        }

        if (nelzyaRazdatKarti == false && Table.instance.place_id == 2 && Players.instance.p2Card1 != "0" && !string.IsNullOrEmpty(Players.instance.p2Card1) && Players.instance.p2Card2 != "0" && !string.IsNullOrEmpty(Players.instance.p2Card2) && Players.instance.p2Card3 != "0" && !string.IsNullOrEmpty(Players.instance.p2Card3))
        {
            for (int i = 0; i < cardTweenPositions.Count; i++)
            {
                cardTweenPositions[i].duration = 0.1f;
            }
            for (int i = 1; i < 4; i++)
            {
                cardsTweenScales[i].SetStartToCurrentValue();
                cardsTweenScales[i].to = new Vector3(1f, 1f, 1f);
            }
            if (ktoHodit == 1)
            {
                p1c1TweenPos();
            }
            if (ktoHodit == 2)
            {
                p2c1TweenPos();
            }
            if (ktoHodit == 3)
            {
                p3c1TweenPos();
            }
            if (ktoHodit == 4)
            {
                p4c1TweenPos();
            }
            if (ktoHodit == 5)
            {
                p5c1TweenPos();
            }
            if (ktoHodit == 6)
            {
                p6c1TweenPos();
            }

            p6Name.text = Players.instance.p1Name;
            p6Bank.text = Players.instance.p1Balance.ToString();
            p1Name.text = Players.instance.p2Name;
            p1Bank.text = Players.instance.p2Balance.ToString();
            p2Name.text = Players.instance.p3Name;
            p2Bank.text = Players.instance.p3Balance.ToString();
            p3Name.text = Players.instance.p4Name;
            p3Bank.text = Players.instance.p4Balance.ToString();
            p4Name.text = Players.instance.p5Name;
            p4Bank.text = Players.instance.p5Balance.ToString();
            p5Name.text = Players.instance.p6Name;
            p5Bank.text = Players.instance.p6Balance.ToString();

            nelzyaRazdatKarti = true;
        }
        if (nelzyaRazdatKarti == false && Table.instance.place_id == 3 && Players.instance.p3Card1 != "0" && !string.IsNullOrEmpty(Players.instance.p3Card1) && Players.instance.p3Card2 != "0" && !string.IsNullOrEmpty(Players.instance.p3Card2) && Players.instance.p3Card3 != "0" && !string.IsNullOrEmpty(Players.instance.p3Card3))
        {
            for (int i = 0; i < cardTweenPositions.Count; i++)
            {
                cardTweenPositions[i].duration = 0.1f;
            }
            for (int i = 1; i < 4; i++)
            {
                cardsTweenScales[i].SetStartToCurrentValue();
                cardsTweenScales[i].to = new Vector3(1f, 1f, 1f);
            }
            if (ktoHodit == 1)
            {
                p1c1TweenPos();
            }
            if (ktoHodit == 2)
            {
                p2c1TweenPos();
            }
            if (ktoHodit == 3)
            {
                p3c1TweenPos();
            }
            if (ktoHodit == 4)
            {
                p4c1TweenPos();
            }
            if (ktoHodit == 5)
            {
                p5c1TweenPos();
            }
            if (ktoHodit == 6)
            {
                p6c1TweenPos();
            }

            p5Name.text = Players.instance.p1Name;
            p5Bank.text = Players.instance.p1Balance.ToString();
            p6Name.text = Players.instance.p2Name;
            p6Bank.text = Players.instance.p2Balance.ToString();
            p1Name.text = Players.instance.p3Name;
            p1Bank.text = Players.instance.p3Balance.ToString();
            p2Name.text = Players.instance.p4Name;
            p2Bank.text = Players.instance.p4Balance.ToString();
            p3Name.text = Players.instance.p5Name;
            p3Bank.text = Players.instance.p5Balance.ToString();
            p4Name.text = Players.instance.p6Name;
            p4Bank.text = Players.instance.p6Balance.ToString();

            nelzyaRazdatKarti = true;
        }
        if (nelzyaRazdatKarti == false && Table.instance.place_id == 4 && Players.instance.p4Card1 != "0" && !string.IsNullOrEmpty(Players.instance.p4Card1) && Players.instance.p4Card2 != "0" && !string.IsNullOrEmpty(Players.instance.p4Card2) && Players.instance.p4Card3 != "0" && !string.IsNullOrEmpty(Players.instance.p4Card3))
        {
            for (int i = 0; i < cardTweenPositions.Count; i++)
            {
                cardTweenPositions[i].duration = 0.1f;
            }
            for (int i = 1; i < 4; i++)
            {
                cardsTweenScales[i].SetStartToCurrentValue();
                cardsTweenScales[i].to = new Vector3(1f, 1f, 1f);
            }
            if (ktoHodit == 1)
            {
                p1c1TweenPos();
            }
            if (ktoHodit == 2)
            {
                p2c1TweenPos();
            }
            if (ktoHodit == 3)
            {
                p3c1TweenPos();
            }
            if (ktoHodit == 4)
            {
                p4c1TweenPos();
            }
            if (ktoHodit == 5)
            {
                p5c1TweenPos();
            }
            if (ktoHodit == 6)
            {
                p6c1TweenPos();
            }

            p4Name.text = Players.instance.p1Name;
            p4Bank.text = Players.instance.p1Balance.ToString();
            p5Name.text = Players.instance.p2Name;
            p5Bank.text = Players.instance.p2Balance.ToString();
            p6Name.text = Players.instance.p3Name;
            p6Bank.text = Players.instance.p3Balance.ToString();
            p1Name.text = Players.instance.p4Name;
            p1Bank.text = Players.instance.p4Balance.ToString();
            p2Name.text = Players.instance.p5Name;
            p2Bank.text = Players.instance.p5Balance.ToString();
            p3Name.text = Players.instance.p6Name;
            p3Bank.text = Players.instance.p6Balance.ToString();

            nelzyaRazdatKarti = true;
        }
        if (nelzyaRazdatKarti == false && Table.instance.place_id == 5 && Players.instance.p5Card1 != "0" && !string.IsNullOrEmpty(Players.instance.p5Card1) && Players.instance.p5Card2 != "0" && !string.IsNullOrEmpty(Players.instance.p5Card2) && Players.instance.p5Card3 != "0" && !string.IsNullOrEmpty(Players.instance.p5Card3))
        {
            for (int i = 0; i < cardTweenPositions.Count; i++)
            {
                cardTweenPositions[i].duration = 0.1f;
            }
            for (int i = 1; i < 4; i++)
            {
                cardsTweenScales[i].SetStartToCurrentValue();
                cardsTweenScales[i].to = new Vector3(1f, 1f, 1f);
            }
            if (ktoHodit == 1)
            {
                p1c1TweenPos();
            }
            if (ktoHodit == 2)
            {
                p2c1TweenPos();
            }
            if (ktoHodit == 3)
            {
                p3c1TweenPos();
            }
            if (ktoHodit == 4)
            {
                p4c1TweenPos();
            }
            if (ktoHodit == 5)
            {
                p5c1TweenPos();
            }
            if (ktoHodit == 6)
            {
                p6c1TweenPos();
            }

            p3Name.text = Players.instance.p1Name;
            p3Bank.text = Players.instance.p1Balance.ToString();
            p4Name.text = Players.instance.p2Name;
            p4Bank.text = Players.instance.p2Balance.ToString();
            p5Name.text = Players.instance.p3Name;
            p5Bank.text = Players.instance.p3Balance.ToString();
            p6Name.text = Players.instance.p4Name;
            p6Bank.text = Players.instance.p4Balance.ToString();
            p1Name.text = Players.instance.p5Name;
            p1Bank.text = Players.instance.p5Balance.ToString();
            p2Name.text = Players.instance.p6Name;
            p2Bank.text = Players.instance.p6Balance.ToString();

            nelzyaRazdatKarti = true;
        }
        if (nelzyaRazdatKarti == false && Table.instance.place_id == 6 && Players.instance.p6Card1 != "0" && !string.IsNullOrEmpty(Players.instance.p6Card1) && Players.instance.p6Card2 != "0" && !string.IsNullOrEmpty(Players.instance.p6Card2) && Players.instance.p6Card3 != "0" && !string.IsNullOrEmpty(Players.instance.p6Card3))
        {
            
            for (int i = 0; i < cardTweenPositions.Count; i++)
            {
                cardTweenPositions[i].duration = 0.1f;
            }
            for (int i = 1; i < 4; i++)
            {
                cardsTweenScales[i].SetStartToCurrentValue();
                cardsTweenScales[i].to = new Vector3(1f, 1f, 1f);
            }
            if (ktoHodit == 1)
            {
                p1c1TweenPos();
            }
            if (ktoHodit == 2)
            {
                p2c1TweenPos();
            }
            if (ktoHodit == 3)
            {
                p3c1TweenPos();
            }
            if (ktoHodit == 4)
            {
                p4c1TweenPos();
            }
            if (ktoHodit == 5)
            {
                p5c1TweenPos();
            }
            if (ktoHodit == 6)
            {
                p6c1TweenPos();
            }

            p2Name.text = Players.instance.p1Name;
            p2Bank.text = Players.instance.p1Balance.ToString();
            p3Name.text = Players.instance.p2Name;
            p3Bank.text = Players.instance.p2Balance.ToString();
            p4Name.text = Players.instance.p3Name;
            p4Bank.text = Players.instance.p3Balance.ToString();
            p5Name.text = Players.instance.p4Name;
            p5Bank.text = Players.instance.p4Balance.ToString();
            p6Name.text = Players.instance.p5Name;
            p6Bank.text = Players.instance.p5Balance.ToString();
            p1Name.text = Players.instance.p6Name;
            p1Bank.text = Players.instance.p6Balance.ToString();

            nelzyaRazdatKarti = true;
        }


    }
    public void p1c1TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {
                if (Players.instance.p1IsPlay == 1 && !p1c1IsGiven)
                {
                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                    p1c1.ResetToBeginning();p1c1.PlayForward();
                    p1c1s.PlayForward();
                    p1c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p2c1IsGiven)
                    {
                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c1.to = new Vector3(-10f, 78f, 0f);
                        p2c1.ResetToBeginning();p2c1.PlayForward();
                        p2c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p3c1IsGiven)
                        {
                            p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c1.to = new Vector3(-10f, 78f, 0f);
                            p3c1.ResetToBeginning();p3c1.PlayForward();
                            p3c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p4c1IsGiven)
                            {
                                p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                p4c1.to = new Vector3(-10f, 78f, 0f);
                                p4c1.ResetToBeginning();p4c1.PlayForward();
                                p4c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p5c1IsGiven)
                                {
                                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c1.to = new Vector3(-10f, 78f, 0f);
                                    p5c1.ResetToBeginning();p5c1.PlayForward();
                                    p5c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p6c1IsGiven)
                                    {
                                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c1.to = new Vector3(-10f, 78f, 0f);
                                        p6c1.ResetToBeginning();p6c1.PlayForward();
                                        p6c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p1c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {
                if (Players.instance.p2IsPlay == 1 && !p1c1IsGiven)
                {
                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                    p1c1.ResetToBeginning();p1c1.PlayForward();
                    p1c1s.PlayForward();
                    p1c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p2c1IsGiven)
                    {
                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c1.to = new Vector3(-10f, 78f, 0f);
                        p2c1.ResetToBeginning();p2c1.PlayForward();
                        p2c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p3c1IsGiven)
                        {
                            p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c1.to = new Vector3(-10f, 78f, 0f);
                            p3c1.ResetToBeginning();p3c1.PlayForward();
                            p3c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p4c1IsGiven)
                            {
                                p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                p4c1.to = new Vector3(-10f, 78f, 0f);
                                p4c1.ResetToBeginning();p4c1.PlayForward();
                                p4c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p5c1IsGiven)
                                {
                                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c1.to = new Vector3(-10f, 78f, 0f);
                                    p5c1.ResetToBeginning();p5c1.PlayForward();
                                    p5c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p6c1IsGiven)
                                    {
                                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c1.to = new Vector3(-10f, 78f, 0f);
                                        p6c1.ResetToBeginning();p6c1.PlayForward();
                                        p6c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p1c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {
                if (Players.instance.p3IsPlay == 1 && !p1c1IsGiven)
                {
                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                    p1c1.ResetToBeginning();p1c1.PlayForward();
                    p1c1s.PlayForward();
                    p1c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p2c1IsGiven)
                    {
                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c1.to = new Vector3(-10f, 78f, 0f);
                        p2c1.ResetToBeginning();p2c1.PlayForward();
                        p2c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p3c1IsGiven)
                        {
                            p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c1.to = new Vector3(-10f, 78f, 0f);
                            p3c1.ResetToBeginning();p3c1.PlayForward();
                            p3c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p4c1IsGiven)
                            {
                                p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                p4c1.to = new Vector3(-10f, 78f, 0f);
                                p4c1.ResetToBeginning();p4c1.PlayForward();
                                p4c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p5c1IsGiven)
                                {
                                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c1.to = new Vector3(-10f, 78f, 0f);
                                    p5c1.ResetToBeginning();p5c1.PlayForward();
                                    p5c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p6c1IsGiven)
                                    {
                                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c1.to = new Vector3(-10f, 78f, 0f);
                                        p6c1.ResetToBeginning();p6c1.PlayForward();
                                        p6c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p1c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Table.instance.place_id == 4)
            {
                if (Players.instance.p4IsPlay == 1 && !p1c1IsGiven)
                {
                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                    p1c1.ResetToBeginning();p1c1.PlayForward();
                    p1c1s.PlayForward();
                    p1c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p2c1IsGiven)
                    {
                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c1.to = new Vector3(-10f, 78f, 0f);
                        p2c1.ResetToBeginning();p2c1.PlayForward();
                        p2c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p3c1IsGiven)
                        {
                            p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c1.to = new Vector3(-10f, 78f, 0f);
                            p3c1.ResetToBeginning();p3c1.PlayForward();
                            p3c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p4c1IsGiven)
                            {
                                p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                p4c1.to = new Vector3(-10f, 78f, 0f);
                                p4c1.ResetToBeginning();p4c1.PlayForward();
                                p4c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p5c1IsGiven)
                                {
                                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c1.to = new Vector3(-10f, 78f, 0f);
                                    p5c1.ResetToBeginning();p5c1.PlayForward();
                                    p5c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p6c1IsGiven)
                                    {
                                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c1.to = new Vector3(-10f, 78f, 0f);
                                        p6c1.ResetToBeginning();p6c1.PlayForward();
                                        p6c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p1c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {
                if (Players.instance.p5IsPlay == 1 && !p1c1IsGiven)
                {
                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                    p1c1.ResetToBeginning();p1c1.PlayForward();
                    p1c1s.PlayForward();
                    p1c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p2c1IsGiven)
                    {
                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c1.to = new Vector3(-10f, 78f, 0f);
                        p2c1.ResetToBeginning();p2c1.PlayForward();
                        p2c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p3c1IsGiven)
                        {
                            p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c1.to = new Vector3(-10f, 78f, 0f);
                            p3c1.ResetToBeginning();p3c1.PlayForward();
                            p3c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p4c1IsGiven)
                            {
                                p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                p4c1.to = new Vector3(-10f, 78f, 0f);
                                p4c1.ResetToBeginning();p4c1.PlayForward();
                                p4c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p5c1IsGiven)
                                {
                                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c1.to = new Vector3(-10f, 78f, 0f);
                                    p5c1.ResetToBeginning();p5c1.PlayForward();
                                    p5c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p6c1IsGiven)
                                    {
                                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c1.to = new Vector3(-10f, 78f, 0f);
                                        p6c1.ResetToBeginning();p6c1.PlayForward();
                                        p6c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p1c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {
                if (Players.instance.p6IsPlay == 1 && !p1c1IsGiven)
                {
                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                    p1c1.ResetToBeginning();p1c1.PlayForward();
                    p1c1s.PlayForward();
                    p1c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p2c1IsGiven)
                    {
                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c1.to = new Vector3(-10f, 78f, 0f);
                        p2c1.ResetToBeginning();p2c1.PlayForward();
                        p2c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p3c1IsGiven)
                        {
                            p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c1.to = new Vector3(-10f, 78f, 0f);
                            p3c1.ResetToBeginning();p3c1.PlayForward();
                            p3c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p4c1IsGiven)
                            {
                                p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                p4c1.to = new Vector3(-10f, 78f, 0f);
                                p4c1.ResetToBeginning();p4c1.PlayForward();
                                p4c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p5c1IsGiven)
                                {
                                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c1.to = new Vector3(-10f, 78f, 0f);
                                    p5c1.ResetToBeginning();p5c1.PlayForward();
                                    p5c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p6c1IsGiven)
                                    {
                                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c1.to = new Vector3(-10f, 78f, 0f);
                                        p6c1.ResetToBeginning();p6c1.PlayForward();
                                        p6c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p1c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p2c1TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p2IsPlay == 1 && !p2c1IsGiven)
                {
                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c1.to = new Vector3(-10f, 78f, 0f);
                    p2c1.ResetToBeginning();p2c1.PlayForward();
                    p2c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p3c1IsGiven)
                    {
                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c1.to = new Vector3(-10f, 78f, 0f);
                        p3c1.ResetToBeginning();p3c1.PlayForward();
                        p3c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p4c1IsGiven)
                        {
                            p4c1.from = new Vector3(-27.1f, -112f, 0f);
                            p4c1.to = new Vector3(-10f, 78f, 0f);
                            p4c1.ResetToBeginning();p4c1.PlayForward();
                            p4c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p5c1IsGiven)
                            {
                                p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c1.to = new Vector3(-10f, 78f, 0f);
                                p5c1.ResetToBeginning();p5c1.PlayForward();
                                p5c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p6c1IsGiven)
                                {
                                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c1.to = new Vector3(-10f, 78f, 0f);
                                    p6c1.ResetToBeginning();p6c1.PlayForward();
                                    p6c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p1c1IsGiven)
                                    {
                                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                        p1c1.ResetToBeginning();p1c1.PlayForward();
                                        p1c1s.PlayForward();
                                        p1c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p3IsPlay == 1 && !p2c1IsGiven)
                {
                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c1.to = new Vector3(-10f, 78f, 0f);
                    p2c1.ResetToBeginning();p2c1.PlayForward();
                    p2c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p3c1IsGiven)
                    {
                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c1.to = new Vector3(-10f, 78f, 0f);
                        p3c1.ResetToBeginning();p3c1.PlayForward();
                        p3c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p4c1IsGiven)
                        {
                            p4c1.from = new Vector3(-27.1f, -112f, 0f);
                            p4c1.to = new Vector3(-10f, 78f, 0f);
                            p4c1.ResetToBeginning();p4c1.PlayForward();
                            p4c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p5c1IsGiven)
                            {
                                p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c1.to = new Vector3(-10f, 78f, 0f);
                                p5c1.ResetToBeginning();p5c1.PlayForward();
                                p5c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p6c1IsGiven)
                                {
                                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c1.to = new Vector3(-10f, 78f, 0f);
                                    p6c1.ResetToBeginning();p6c1.PlayForward();
                                    p6c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p1c1IsGiven)
                                    {
                                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                        p1c1.ResetToBeginning();p1c1.PlayForward();
                                        p1c1s.PlayForward();
                                        p1c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p4IsPlay == 1 && !p2c1IsGiven)
                {
                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c1.to = new Vector3(-10f, 78f, 0f);
                    p2c1.ResetToBeginning();p2c1.PlayForward();
                    p2c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p3c1IsGiven)
                    {
                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c1.to = new Vector3(-10f, 78f, 0f);
                        p3c1.ResetToBeginning();p3c1.PlayForward();
                        p3c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p4c1IsGiven)
                        {
                            p4c1.from = new Vector3(-27.1f, -112f, 0f);
                            p4c1.to = new Vector3(-10f, 78f, 0f);
                            p4c1.ResetToBeginning();p4c1.PlayForward();
                            p4c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p5c1IsGiven)
                            {
                                p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c1.to = new Vector3(-10f, 78f, 0f);
                                p5c1.ResetToBeginning();p5c1.PlayForward();
                                p5c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p6c1IsGiven)
                                {
                                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c1.to = new Vector3(-10f, 78f, 0f);
                                    p6c1.ResetToBeginning();p6c1.PlayForward();
                                    p6c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p1c1IsGiven)
                                    {
                                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                        p1c1.ResetToBeginning();p1c1.PlayForward();
                                        p1c1s.PlayForward();
                                        p1c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p5IsPlay == 1 && !p2c1IsGiven)
                {
                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c1.to = new Vector3(-10f, 78f, 0f);
                    p2c1.ResetToBeginning();p2c1.PlayForward();
                    p2c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p3c1IsGiven)
                    {
                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c1.to = new Vector3(-10f, 78f, 0f);
                        p3c1.ResetToBeginning();p3c1.PlayForward();
                        p3c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p4c1IsGiven)
                        {
                            p4c1.from = new Vector3(-27.1f, -112f, 0f);
                            p4c1.to = new Vector3(-10f, 78f, 0f);
                            p4c1.ResetToBeginning();p4c1.PlayForward();
                            p4c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p5c1IsGiven)
                            {
                                p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c1.to = new Vector3(-10f, 78f, 0f);
                                p5c1.ResetToBeginning();p5c1.PlayForward();
                                p5c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p6c1IsGiven)
                                {
                                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c1.to = new Vector3(-10f, 78f, 0f);
                                    p6c1.ResetToBeginning();p6c1.PlayForward();
                                    p6c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p1c1IsGiven)
                                    {
                                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                        p1c1.ResetToBeginning();p1c1.PlayForward();
                                        p1c1s.PlayForward();
                                        p1c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p6IsPlay == 1 && !p2c1IsGiven)
                {
                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c1.to = new Vector3(-10f, 78f, 0f);
                    p2c1.ResetToBeginning();p2c1.PlayForward();
                    p2c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p3c1IsGiven)
                    {
                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c1.to = new Vector3(-10f, 78f, 0f);
                        p3c1.ResetToBeginning();p3c1.PlayForward();
                        p3c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p4c1IsGiven)
                        {
                            p4c1.from = new Vector3(-27.1f, -112f, 0f);
                            p4c1.to = new Vector3(-10f, 78f, 0f);
                            p4c1.ResetToBeginning();p4c1.PlayForward();
                            p4c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p5c1IsGiven)
                            {
                                p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c1.to = new Vector3(-10f, 78f, 0f);
                                p5c1.ResetToBeginning();p5c1.PlayForward();
                                p5c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p6c1IsGiven)
                                {
                                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c1.to = new Vector3(-10f, 78f, 0f);
                                    p6c1.ResetToBeginning();p6c1.PlayForward();
                                    p6c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p1c1IsGiven)
                                    {
                                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                        p1c1.ResetToBeginning();p1c1.PlayForward();
                                        p1c1s.PlayForward();
                                        p1c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p1IsPlay == 1 && !p2c1IsGiven)
                {
                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c1.to = new Vector3(-10f, 78f, 0f);
                    p2c1.ResetToBeginning();p2c1.PlayForward();
                    p2c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p3c1IsGiven)
                    {
                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c1.to = new Vector3(-10f, 78f, 0f);
                        p3c1.ResetToBeginning();p3c1.PlayForward();
                        p3c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p4c1IsGiven)
                        {
                            p4c1.from = new Vector3(-27.1f, -112f, 0f);
                            p4c1.to = new Vector3(-10f, 78f, 0f);
                            p4c1.ResetToBeginning();p4c1.PlayForward();
                            p4c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p5c1IsGiven)
                            {
                                p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c1.to = new Vector3(-10f, 78f, 0f);
                                p5c1.ResetToBeginning();p5c1.PlayForward();
                                p5c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p6c1IsGiven)
                                {
                                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c1.to = new Vector3(-10f, 78f, 0f);
                                    p6c1.ResetToBeginning();p6c1.PlayForward();
                                    p6c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p1c1IsGiven)
                                    {
                                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                        p1c1.ResetToBeginning();p1c1.PlayForward();
                                        p1c1s.PlayForward();
                                        p1c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }
    public void p3c1TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p3IsPlay == 1 && !p3c1IsGiven)
                {
                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c1.to = new Vector3(-10f, 78f, 0f);
                    p3c1.ResetToBeginning();p3c1.PlayForward();
                    p3c1IsGiven = true;
                    print("p3c1 srabotalo");
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p4c1IsGiven)
                    {
                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                        p4c1.to = new Vector3(-10f, 78f, 0f);
                        p4c1.ResetToBeginning();p4c1.PlayForward();
                        p4c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p5c1IsGiven)
                        {
                            p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c1.to = new Vector3(-10f, 78f, 0f);
                            p5c1.ResetToBeginning();p5c1.PlayForward();
                            p5c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p6c1IsGiven)
                            {
                                p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c1.to = new Vector3(-10f, 78f, 0f);
                                p6c1.ResetToBeginning();p6c1.PlayForward();
                                p6c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p1c1IsGiven)
                                {
                                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                    p1c1.ResetToBeginning();p1c1.PlayForward();
                                    p1c1s.PlayForward();
                                    p1c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p2c1IsGiven)
                                    {
                                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c1.to = new Vector3(-10f, 78f, 0f);
                                        p2c1.ResetToBeginning();p2c1.PlayForward();
                                        p2c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p4IsPlay == 1 && !p3c1IsGiven)
                {
                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c1.to = new Vector3(-10f, 78f, 0f);
                    p3c1.ResetToBeginning();p3c1.PlayForward();
                    p3c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p4c1IsGiven)
                    {
                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                        p4c1.to = new Vector3(-10f, 78f, 0f);
                        p4c1.ResetToBeginning();p4c1.PlayForward();
                        p4c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p5c1IsGiven)
                        {
                            p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c1.to = new Vector3(-10f, 78f, 0f);
                            p5c1.ResetToBeginning();p5c1.PlayForward();
                            p5c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p6c1IsGiven)
                            {
                                p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c1.to = new Vector3(-10f, 78f, 0f);
                                p6c1.ResetToBeginning();p6c1.PlayForward();
                                p6c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p1c1IsGiven)
                                {
                                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                    p1c1.ResetToBeginning();p1c1.PlayForward();
                                    p1c1s.PlayForward();
                                    p1c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p2c1IsGiven)
                                    {
                                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c1.to = new Vector3(-10f, 78f, 0f);
                                        p2c1.ResetToBeginning();p2c1.PlayForward();
                                        p2c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p5IsPlay == 1 && !p3c1IsGiven)
                {
                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c1.to = new Vector3(-10f, 78f, 0f);
                    p3c1.ResetToBeginning();p3c1.PlayForward();
                    p3c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p4c1IsGiven)
                    {
                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                        p4c1.to = new Vector3(-10f, 78f, 0f);
                        p4c1.ResetToBeginning();p4c1.PlayForward();
                        p4c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p5c1IsGiven)
                        {
                            p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c1.to = new Vector3(-10f, 78f, 0f);
                            p5c1.ResetToBeginning();p5c1.PlayForward();
                            p5c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p6c1IsGiven)
                            {
                                p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c1.to = new Vector3(-10f, 78f, 0f);
                                p6c1.ResetToBeginning();p6c1.PlayForward();
                                p6c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p1c1IsGiven)
                                {
                                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                    p1c1.ResetToBeginning();p1c1.PlayForward();
                                    p1c1s.PlayForward();
                                    p1c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p2c1IsGiven)
                                    {
                                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c1.to = new Vector3(-10f, 78f, 0f);
                                        p2c1.ResetToBeginning();p2c1.PlayForward();
                                        p2c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p6IsPlay == 1 && !p3c1IsGiven)
                {
                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c1.to = new Vector3(-10f, 78f, 0f);
                    p3c1.ResetToBeginning();p3c1.PlayForward();
                    p3c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p4c1IsGiven)
                    {
                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                        p4c1.to = new Vector3(-10f, 78f, 0f);
                        p4c1.ResetToBeginning();p4c1.PlayForward();
                        p4c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p5c1IsGiven)
                        {
                            p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c1.to = new Vector3(-10f, 78f, 0f);
                            p5c1.ResetToBeginning();p5c1.PlayForward();
                            p5c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p6c1IsGiven)
                            {
                                p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c1.to = new Vector3(-10f, 78f, 0f);
                                p6c1.ResetToBeginning();p6c1.PlayForward();
                                p6c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p1c1IsGiven)
                                {
                                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                    p1c1.ResetToBeginning();p1c1.PlayForward();
                                    p1c1s.PlayForward();
                                    p1c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p2c1IsGiven)
                                    {
                                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c1.to = new Vector3(-10f, 78f, 0f);
                                        p2c1.ResetToBeginning();p2c1.PlayForward();
                                        p2c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p1IsPlay == 1 && !p3c1IsGiven)
                {
                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c1.to = new Vector3(-10f, 78f, 0f);
                    p3c1.ResetToBeginning();p3c1.PlayForward();
                    p3c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p4c1IsGiven)
                    {
                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                        p4c1.to = new Vector3(-10f, 78f, 0f);
                        p4c1.ResetToBeginning();p4c1.PlayForward();
                        p4c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p5c1IsGiven)
                        {
                            p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c1.to = new Vector3(-10f, 78f, 0f);
                            p5c1.ResetToBeginning();p5c1.PlayForward();
                            p5c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p6c1IsGiven)
                            {
                                p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c1.to = new Vector3(-10f, 78f, 0f);
                                p6c1.ResetToBeginning();p6c1.PlayForward();
                                p6c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p1c1IsGiven)
                                {
                                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                    p1c1.ResetToBeginning();p1c1.PlayForward();
                                    p1c1s.PlayForward();
                                    p1c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p2c1IsGiven)
                                    {
                                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c1.to = new Vector3(-10f, 78f, 0f);
                                        p2c1.ResetToBeginning();p2c1.PlayForward();
                                        p2c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p2IsPlay == 1 && !p3c1IsGiven)
                {
                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c1.to = new Vector3(-10f, 78f, 0f);
                    p3c1.ResetToBeginning();p3c1.PlayForward();
                    p3c1IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p4c1IsGiven)
                    {
                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                        p4c1.to = new Vector3(-10f, 78f, 0f);
                        p4c1.ResetToBeginning();p4c1.PlayForward();
                        p4c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p5c1IsGiven)
                        {
                            p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c1.to = new Vector3(-10f, 78f, 0f);
                            p5c1.ResetToBeginning();p5c1.PlayForward();
                            p5c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p6c1IsGiven)
                            {
                                p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c1.to = new Vector3(-10f, 78f, 0f);
                                p6c1.ResetToBeginning();p6c1.PlayForward();
                                p6c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p1c1IsGiven)
                                {
                                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                    p1c1.ResetToBeginning();p1c1.PlayForward();
                                    p1c1s.PlayForward();
                                    p1c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p2c1IsGiven)
                                    {
                                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c1.to = new Vector3(-10f, 78f, 0f);
                                        p2c1.ResetToBeginning();p2c1.PlayForward();
                                        p2c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }    
    public void p4c1TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p4IsPlay == 1 && !p4c1IsGiven)
                {
                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                    p4c1.to = new Vector3(-10f, 78f, 0f);
                    p4c1.ResetToBeginning();p4c1.PlayForward();
                    p4c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p5c1IsGiven)
                    {
                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c1.to = new Vector3(-10f, 78f, 0f);
                        p5c1.ResetToBeginning();p5c1.PlayForward();
                        p5c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p6c1IsGiven)
                        {
                            p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c1.to = new Vector3(-10f, 78f, 0f);
                            p6c1.ResetToBeginning();p6c1.PlayForward();
                            p6c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p1c1IsGiven)
                            {
                                p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                p1c1.ResetToBeginning();p1c1.PlayForward();
                                p1c1s.PlayForward();
                                p1c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p2c1IsGiven)
                                {
                                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c1.to = new Vector3(-10f, 78f, 0f);
                                    p2c1.ResetToBeginning();p2c1.PlayForward();
                                    p2c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p3c1IsGiven)
                                    {
                                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c1.to = new Vector3(-10f, 78f, 0f);
                                        p3c1.ResetToBeginning();p3c1.PlayForward();
                                        p3c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p5IsPlay == 1 && !p4c1IsGiven)
                {
                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                    p4c1.to = new Vector3(-10f, 78f, 0f);
                    p4c1.ResetToBeginning();p4c1.PlayForward();
                    p4c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p5c1IsGiven)
                    {
                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c1.to = new Vector3(-10f, 78f, 0f);
                        p5c1.ResetToBeginning();p5c1.PlayForward();
                        p5c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p6c1IsGiven)
                        {
                            p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c1.to = new Vector3(-10f, 78f, 0f);
                            p6c1.ResetToBeginning();p6c1.PlayForward();
                            p6c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p1c1IsGiven)
                            {
                                p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                p1c1.ResetToBeginning();p1c1.PlayForward();
                                p1c1s.PlayForward();
                                p1c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p2c1IsGiven)
                                {
                                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c1.to = new Vector3(-10f, 78f, 0f);
                                    p2c1.ResetToBeginning();p2c1.PlayForward();
                                    p2c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p3c1IsGiven)
                                    {
                                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c1.to = new Vector3(-10f, 78f, 0f);
                                        p3c1.ResetToBeginning();p3c1.PlayForward();
                                        p3c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p6IsPlay == 1 && !p4c1IsGiven)
                {
                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                    p4c1.to = new Vector3(-10f, 78f, 0f);
                    p4c1.ResetToBeginning();p4c1.PlayForward();
                    p4c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p5c1IsGiven)
                    {
                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c1.to = new Vector3(-10f, 78f, 0f);
                        p5c1.ResetToBeginning();p5c1.PlayForward();
                        p5c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p6c1IsGiven)
                        {
                            p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c1.to = new Vector3(-10f, 78f, 0f);
                            p6c1.ResetToBeginning();p6c1.PlayForward();
                            p6c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p1c1IsGiven)
                            {
                                p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                p1c1.ResetToBeginning();p1c1.PlayForward();
                                p1c1s.PlayForward();
                                p1c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p2c1IsGiven)
                                {
                                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c1.to = new Vector3(-10f, 78f, 0f);
                                    p2c1.ResetToBeginning();p2c1.PlayForward();
                                    p2c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p3c1IsGiven)
                                    {
                                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c1.to = new Vector3(-10f, 78f, 0f);
                                        p3c1.ResetToBeginning();p3c1.PlayForward();
                                        p3c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p1IsPlay == 1 && !p4c1IsGiven)
                {
                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                    p4c1.to = new Vector3(-10f, 78f, 0f);
                    p4c1.ResetToBeginning();p4c1.PlayForward();
                    p4c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p5c1IsGiven)
                    {
                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c1.to = new Vector3(-10f, 78f, 0f);
                        p5c1.ResetToBeginning();p5c1.PlayForward();
                        p5c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p6c1IsGiven)
                        {
                            p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c1.to = new Vector3(-10f, 78f, 0f);
                            p6c1.ResetToBeginning();p6c1.PlayForward();
                            p6c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p1c1IsGiven)
                            {
                                p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                p1c1.ResetToBeginning();p1c1.PlayForward();
                                p1c1s.PlayForward();
                                p1c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p2c1IsGiven)
                                {
                                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c1.to = new Vector3(-10f, 78f, 0f);
                                    p2c1.ResetToBeginning();p2c1.PlayForward();
                                    p2c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p3c1IsGiven)
                                    {
                                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c1.to = new Vector3(-10f, 78f, 0f);
                                        p3c1.ResetToBeginning();p3c1.PlayForward();
                                        p3c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p2IsPlay == 1 && !p4c1IsGiven)
                {
                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                    p4c1.to = new Vector3(-10f, 78f, 0f);
                    p4c1.ResetToBeginning();p4c1.PlayForward();
                    p4c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p5c1IsGiven)
                    {
                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c1.to = new Vector3(-10f, 78f, 0f);
                        p5c1.ResetToBeginning();p5c1.PlayForward();
                        p5c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p6c1IsGiven)
                        {
                            p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c1.to = new Vector3(-10f, 78f, 0f);
                            p6c1.ResetToBeginning();p6c1.PlayForward();
                            p6c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p1c1IsGiven)
                            {
                                p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                p1c1.ResetToBeginning();p1c1.PlayForward();
                                p1c1s.PlayForward();
                                p1c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p2c1IsGiven)
                                {
                                    p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c1.to = new Vector3(-10f, 78f, 0f);
                                    p2c1.ResetToBeginning();p2c1.PlayForward();
                                    p2c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p3c1IsGiven)
                                    {
                                        p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c1.to = new Vector3(-10f, 78f, 0f);
                                        p3c1.ResetToBeginning();p3c1.PlayForward();
                                        p3c1IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {
                {
                    if (Players.instance.p3IsPlay == 1 && !p4c1IsGiven)
                    {
                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                        p4c1.to = new Vector3(-10f, 78f, 0f);
                        p4c1.ResetToBeginning();p4c1.PlayForward();
                        p4c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p5c1IsGiven)
                        {
                            p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c1.to = new Vector3(-10f, 78f, 0f);
                            p5c1.ResetToBeginning();p5c1.PlayForward();
                            p5c1IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p6c1IsGiven)
                            {
                                p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c1.to = new Vector3(-10f, 78f, 0f);
                                p6c1.ResetToBeginning();p6c1.PlayForward();
                                p6c1IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p1c1IsGiven)
                                {
                                    p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                                    p1c1.ResetToBeginning();p1c1.PlayForward();
                                    p1c1s.PlayForward();
                                    p1c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p2c1IsGiven)
                                    {
                                        p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c1.to = new Vector3(-10f, 78f, 0f);
                                        p2c1.ResetToBeginning();p2c1.PlayForward();
                                        p2c1IsGiven = true;
                                    }
                                    else
                                    {
                                        if (Players.instance.p2IsPlay == 1 && !p3c1IsGiven)
                                        {
                                            p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                            p3c1.to = new Vector3(-10f, 78f, 0f);
                                            p3c1.ResetToBeginning();p3c1.PlayForward();
                                            p3c1IsGiven = true;
                                        }
                                        else

                                            p4c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p5c1TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p5IsPlay == 1 && !p5c1IsGiven)
                {
                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c1.to = new Vector3(-10f, 78f, 0f);
                    p5c1.ResetToBeginning();p5c1.PlayForward();
                    p5c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p6c1IsGiven)
                    {
                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c1.to = new Vector3(-10f, 78f, 0f);
                        p6c1.ResetToBeginning();p6c1.PlayForward();
                        p6c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p1c1IsGiven)
                        {
                            p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                            p1c1.ResetToBeginning();p1c1.PlayForward();
                            p1c1s.PlayForward();
                            p1c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p2c1IsGiven)
                            {
                                p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c1.to = new Vector3(-10f, 78f, 0f);
                                p2c1.ResetToBeginning();p2c1.PlayForward();
                                p2c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p3c1IsGiven)
                                {
                                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c1.to = new Vector3(-10f, 78f, 0f);
                                    p3c1.ResetToBeginning();p3c1.PlayForward();
                                    p3c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p4c1IsGiven)
                                    {
                                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c1.to = new Vector3(-10f, 78f, 0f);
                                        p4c1.ResetToBeginning();p4c1.PlayForward();
                                        p4c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p5c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p6IsPlay == 1 && !p5c1IsGiven)
                {
                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c1.to = new Vector3(-10f, 78f, 0f);
                    p5c1.ResetToBeginning();p5c1.PlayForward();
                    p5c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p6c1IsGiven)
                    {
                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c1.to = new Vector3(-10f, 78f, 0f);
                        p6c1.ResetToBeginning();p6c1.PlayForward();
                        p6c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p1c1IsGiven)
                        {
                            p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                            p1c1.ResetToBeginning();p1c1.PlayForward();
                            p1c1s.PlayForward();
                            p1c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p2c1IsGiven)
                            {
                                p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c1.to = new Vector3(-10f, 78f, 0f);
                                p2c1.ResetToBeginning();p2c1.PlayForward();
                                p2c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p3c1IsGiven)
                                {
                                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c1.to = new Vector3(-10f, 78f, 0f);
                                    p3c1.ResetToBeginning();p3c1.PlayForward();
                                    p3c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p4c1IsGiven)
                                    {
                                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c1.to = new Vector3(-10f, 78f, 0f);
                                        p4c1.ResetToBeginning();p4c1.PlayForward();
                                        p4c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p5c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p1IsPlay == 1 && !p5c1IsGiven)
                {
                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c1.to = new Vector3(-10f, 78f, 0f);
                    p5c1.ResetToBeginning();p5c1.PlayForward();
                    p5c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p6c1IsGiven)
                    {
                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c1.to = new Vector3(-10f, 78f, 0f);
                        p6c1.ResetToBeginning();p6c1.PlayForward();
                        p6c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p1c1IsGiven)
                        {
                            p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                            p1c1.ResetToBeginning();p1c1.PlayForward();
                            p1c1s.PlayForward();
                            p1c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p2c1IsGiven)
                            {
                                p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c1.to = new Vector3(-10f, 78f, 0f);
                                p2c1.ResetToBeginning();p2c1.PlayForward();
                                p2c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p3c1IsGiven)
                                {
                                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c1.to = new Vector3(-10f, 78f, 0f);
                                    p3c1.ResetToBeginning();p3c1.PlayForward();
                                    p3c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p4c1IsGiven)
                                    {
                                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c1.to = new Vector3(-10f, 78f, 0f);
                                        p4c1.ResetToBeginning();p4c1.PlayForward();
                                        p4c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p5c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p2IsPlay == 1 && !p5c1IsGiven)
                {
                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c1.to = new Vector3(-10f, 78f, 0f);
                    p5c1.ResetToBeginning();p5c1.PlayForward();
                    p5c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p6c1IsGiven)
                    {
                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c1.to = new Vector3(-10f, 78f, 0f);
                        p6c1.ResetToBeginning();p6c1.PlayForward();
                        p6c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p1c1IsGiven)
                        {
                            p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                            p1c1.ResetToBeginning();p1c1.PlayForward();
                            p1c1s.PlayForward();
                            p1c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p2c1IsGiven)
                            {
                                p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c1.to = new Vector3(-10f, 78f, 0f);
                                p2c1.ResetToBeginning();p2c1.PlayForward();
                                p2c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p3c1IsGiven)
                                {
                                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c1.to = new Vector3(-10f, 78f, 0f);
                                    p3c1.ResetToBeginning();p3c1.PlayForward();
                                    p3c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p4c1IsGiven)
                                    {
                                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c1.to = new Vector3(-10f, 78f, 0f);
                                        p4c1.ResetToBeginning();p4c1.PlayForward();
                                        p4c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p5c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p3IsPlay == 1 && !p5c1IsGiven)
                {
                    p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c1.to = new Vector3(-10f, 78f, 0f);
                    p5c1.ResetToBeginning();p5c1.PlayForward();
                    p5c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p6c1IsGiven)
                    {
                        p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c1.to = new Vector3(-10f, 78f, 0f);
                        p6c1.ResetToBeginning();p6c1.PlayForward();
                        p6c1IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p1c1IsGiven)
                        {
                            p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                            p1c1.ResetToBeginning();p1c1.PlayForward();
                            p1c1s.PlayForward();
                            p1c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p2c1IsGiven)
                            {
                                p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c1.to = new Vector3(-10f, 78f, 0f);
                                p2c1.ResetToBeginning();p2c1.PlayForward();
                                p2c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p3c1IsGiven)
                                {
                                    p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c1.to = new Vector3(-10f, 78f, 0f);
                                    p3c1.ResetToBeginning();p3c1.PlayForward();
                                    p3c1IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p4c1IsGiven)
                                    {
                                        p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c1.to = new Vector3(-10f, 78f, 0f);
                                        p4c1.ResetToBeginning();p4c1.PlayForward();
                                        p4c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p5c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                p5c1.to = new Vector3(-10f, 78f, 0f);
                p5c1.ResetToBeginning();p5c1.PlayForward();
                p5c1IsGiven = true;

            }
            else
            {
                if (Players.instance.p5IsPlay == 1 && !p6c1IsGiven)
                {
                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c1.to = new Vector3(-10f, 78f, 0f);
                    p6c1.ResetToBeginning();p6c1.PlayForward();
                    p6c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p1c1IsGiven)
                    {
                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                        p1c1.ResetToBeginning();p1c1.PlayForward();
                        p1c1s.PlayForward();
                        p1c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p2c1IsGiven)
                        {
                            p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c1.to = new Vector3(-10f, 78f, 0f);
                            p2c1.ResetToBeginning();p2c1.PlayForward();
                            p2c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p3c1IsGiven)
                            {
                                p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c1.to = new Vector3(-10f, 78f, 0f);
                                p3c1.ResetToBeginning();p3c1.PlayForward();
                                p3c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p4c1IsGiven)
                                {
                                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c1.to = new Vector3(-10f, 78f, 0f);
                                    p4c1.ResetToBeginning();p4c1.PlayForward();
                                    p4c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p5c1IsGiven)
                                    {
                                        p5c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p6c1TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p6IsPlay == 1 && !p6c1IsGiven)
                {
                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c1.to = new Vector3(-10f, 78f, 0f);
                    p6c1.ResetToBeginning();p6c1.PlayForward();
                    p6c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p1c1IsGiven)
                    {
                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                        p1c1.ResetToBeginning();p1c1.PlayForward();
                        p1c1s.PlayForward();
                        p1c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p2c1IsGiven)
                        {
                            p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c1.to = new Vector3(-10f, 78f, 0f);
                            p2c1.ResetToBeginning();p2c1.PlayForward();
                            p2c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p3c1IsGiven)
                            {
                                p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c1.to = new Vector3(-10f, 78f, 0f);
                                p3c1.ResetToBeginning();p3c1.PlayForward();
                                p3c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p4c1IsGiven)
                                {
                                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c1.to = new Vector3(-10f, 78f, 0f);
                                    p4c1.ResetToBeginning();p4c1.PlayForward();
                                    p4c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p5c1IsGiven)
                                    {
                                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c1.to = new Vector3(-10f, 78f, 0f);
                                        p5c1.ResetToBeginning();p5c1.PlayForward();
                                        p5c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p6c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p1IsPlay == 1 && !p6c1IsGiven)
                {
                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c1.to = new Vector3(-10f, 78f, 0f);
                    p6c1.ResetToBeginning();p6c1.PlayForward();
                    p6c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p1c1IsGiven)
                    {
                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                        p1c1.ResetToBeginning();p1c1.PlayForward();
                        p1c1s.PlayForward();
                        p1c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p2c1IsGiven)
                        {
                            p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c1.to = new Vector3(-10f, 78f, 0f);
                            p2c1.ResetToBeginning();p2c1.PlayForward();
                            p2c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p3c1IsGiven)
                            {
                                p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c1.to = new Vector3(-10f, 78f, 0f);
                                p3c1.ResetToBeginning();p3c1.PlayForward();
                                p3c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p4c1IsGiven)
                                {
                                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c1.to = new Vector3(-10f, 78f, 0f);
                                    p4c1.ResetToBeginning();p4c1.PlayForward();
                                    p4c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p5c1IsGiven)
                                    {
                                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c1.to = new Vector3(-10f, 78f, 0f);
                                        p5c1.ResetToBeginning();p5c1.PlayForward();
                                        p5c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p6c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p2IsPlay == 1 && !p6c1IsGiven)
                {
                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c1.to = new Vector3(-10f, 78f, 0f);
                    p6c1.ResetToBeginning();p6c1.PlayForward();
                    p6c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p1c1IsGiven)
                    {
                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                        p1c1.ResetToBeginning();p1c1.PlayForward();
                        p1c1s.PlayForward();
                        p1c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p2c1IsGiven)
                        {
                            p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c1.to = new Vector3(-10f, 78f, 0f);
                            p2c1.ResetToBeginning();p2c1.PlayForward();
                            p2c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p3c1IsGiven)
                            {
                                p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c1.to = new Vector3(-10f, 78f, 0f);
                                p3c1.ResetToBeginning();p3c1.PlayForward();
                                p3c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p4c1IsGiven)
                                {
                                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c1.to = new Vector3(-10f, 78f, 0f);
                                    p4c1.ResetToBeginning();p4c1.PlayForward();
                                    p4c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p5c1IsGiven)
                                    {
                                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c1.to = new Vector3(-10f, 78f, 0f);
                                        p5c1.ResetToBeginning();p5c1.PlayForward();
                                        p5c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p6c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p3IsPlay == 1 && !p6c1IsGiven)
                {
                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c1.to = new Vector3(-10f, 78f, 0f);
                    p6c1.ResetToBeginning();p6c1.PlayForward();
                    p6c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p1c1IsGiven)
                    {
                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                        p1c1.ResetToBeginning();p1c1.PlayForward();
                        p1c1s.PlayForward();
                        p1c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p2c1IsGiven)
                        {
                            p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c1.to = new Vector3(-10f, 78f, 0f);
                            p2c1.ResetToBeginning();p2c1.PlayForward();
                            p2c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p3c1IsGiven)
                            {
                                p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c1.to = new Vector3(-10f, 78f, 0f);
                                p3c1.ResetToBeginning();p3c1.PlayForward();
                                p3c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p4c1IsGiven)
                                {
                                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c1.to = new Vector3(-10f, 78f, 0f);
                                    p4c1.ResetToBeginning();p4c1.PlayForward();
                                    p4c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p5c1IsGiven)
                                    {
                                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c1.to = new Vector3(-10f, 78f, 0f);
                                        p5c1.ResetToBeginning();p5c1.PlayForward();
                                        p5c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p6c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p4IsPlay == 1 && !p6c1IsGiven)
                {
                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c1.to = new Vector3(-10f, 78f, 0f);
                    p6c1.ResetToBeginning();p6c1.PlayForward();
                    p6c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p1c1IsGiven)
                    {
                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                        p1c1.ResetToBeginning();p1c1.PlayForward();
                        p1c1s.PlayForward();
                        p1c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p2c1IsGiven)
                        {
                            p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c1.to = new Vector3(-10f, 78f, 0f);
                            p2c1.ResetToBeginning();p2c1.PlayForward();
                            p2c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p3c1IsGiven)
                            {
                                p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c1.to = new Vector3(-10f, 78f, 0f);
                                p3c1.ResetToBeginning();p3c1.PlayForward();
                                p3c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p4c1IsGiven)
                                {
                                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c1.to = new Vector3(-10f, 78f, 0f);
                                    p4c1.ResetToBeginning();p4c1.PlayForward();
                                    p4c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p5c1IsGiven)
                                    {
                                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c1.to = new Vector3(-10f, 78f, 0f);
                                        p5c1.ResetToBeginning();p5c1.PlayForward();
                                        p5c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p6c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p5IsPlay == 1 && !p6c1IsGiven)
                {
                    p6c1.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c1.to = new Vector3(-10f, 78f, 0f);
                    p6c1.ResetToBeginning();p6c1.PlayForward();
                    p6c1IsGiven = true;

                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p1c1IsGiven)
                    {
                        p1c1.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c1.to = new Vector3(-121.9f, -130.3f, 0f);
                        p1c1.ResetToBeginning();p1c1.PlayForward();
                        p1c1s.PlayForward();
                        p1c1IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p2c1IsGiven)
                        {
                            p2c1.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c1.to = new Vector3(-10f, 78f, 0f);
                            p2c1.ResetToBeginning();p2c1.PlayForward();
                            p2c1IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p3c1IsGiven)
                            {
                                p3c1.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c1.to = new Vector3(-10f, 78f, 0f);
                                p3c1.ResetToBeginning();p3c1.PlayForward();
                                p3c1IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p4c1IsGiven)
                                {
                                    p4c1.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c1.to = new Vector3(-10f, 78f, 0f);
                                    p4c1.ResetToBeginning();p4c1.PlayForward();
                                    p4c1IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p5c1IsGiven)
                                    {
                                        p5c1.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c1.to = new Vector3(-10f, 78f, 0f);
                                        p5c1.ResetToBeginning();p5c1.PlayForward();
                                        p5c1IsGiven = true;

                                    }
                                    else
                                    {
                                        p6c2TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p1c2TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {
                if (Players.instance.p1IsPlay == 1 && !p1c2IsGiven)
                {
                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                    p1c2.ResetToBeginning();p1c2.PlayForward();
                    p1c2s.PlayForward();
                    p1c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p2c2IsGiven)
                    {
                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c2.to = new Vector3(35f, 78f, 0f);
                        p2c2.ResetToBeginning();p2c2.PlayForward();
                        p2c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p3c2IsGiven)
                        {
                            p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c2.to = new Vector3(35f, 78f, 0f);
                            p3c2.ResetToBeginning();p3c2.PlayForward();
                            p3c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p4c2IsGiven)
                            {
                                p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                p4c2.to = new Vector3(35f, 78f, 0f);
                                p4c2.ResetToBeginning();p4c2.PlayForward();
                                p4c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p5c2IsGiven)
                                {
                                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c2.to = new Vector3(35f, 78f, 0f);
                                    p5c2.ResetToBeginning();p5c2.PlayForward();
                                    p5c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p6c2IsGiven)
                                    {
                                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c2.to = new Vector3(35f, 78f, 0f);
                                        p6c2.ResetToBeginning();p6c2.PlayForward();
                                        p6c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p1c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {
                if (Players.instance.p2IsPlay == 1 && !p1c2IsGiven)
                {
                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                    p1c2.ResetToBeginning();p1c2.PlayForward();
                    p1c2s.PlayForward();
                    p1c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p2c2IsGiven)
                    {
                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c2.to = new Vector3(35f, 78f, 0f);
                        p2c2.ResetToBeginning();p2c2.PlayForward();
                        p2c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p3c2IsGiven)
                        {
                            p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c2.to = new Vector3(35f, 78f, 0f);
                            p3c2.ResetToBeginning();p3c2.PlayForward();
                            p3c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p4c2IsGiven)
                            {
                                p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                p4c2.to = new Vector3(35f, 78f, 0f);
                                p4c2.ResetToBeginning();p4c2.PlayForward();
                                p4c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p5c2IsGiven)
                                {
                                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c2.to = new Vector3(35f, 78f, 0f);
                                    p5c2.ResetToBeginning();p5c2.PlayForward();
                                    p5c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p6c2IsGiven)
                                    {
                                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c2.to = new Vector3(35f, 78f, 0f);
                                        p6c2.ResetToBeginning();p6c2.PlayForward();
                                        p6c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p1c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {
                if (Players.instance.p3IsPlay == 1 && !p1c2IsGiven)
                {
                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                    p1c2.ResetToBeginning();p1c2.PlayForward();
                    p1c2s.PlayForward();
                    p1c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p2c2IsGiven)
                    {
                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c2.to = new Vector3(35f, 78f, 0f);
                        p2c2.ResetToBeginning();p2c2.PlayForward();
                        p2c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p3c2IsGiven)
                        {
                            p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c2.to = new Vector3(35f, 78f, 0f);
                            p3c2.ResetToBeginning();p3c2.PlayForward();
                            p3c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p4c2IsGiven)
                            {
                                p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                p4c2.to = new Vector3(35f, 78f, 0f);
                                p4c2.ResetToBeginning();p4c2.PlayForward();
                                p4c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p5c2IsGiven)
                                {
                                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c2.to = new Vector3(35f, 78f, 0f);
                                    p5c2.ResetToBeginning();p5c2.PlayForward();
                                    p5c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p6c2IsGiven)
                                    {
                                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c2.to = new Vector3(35f, 78f, 0f);
                                        p6c2.ResetToBeginning();p6c2.PlayForward();
                                        p6c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p1c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {
                if (Players.instance.p4IsPlay == 1 && !p1c2IsGiven)
                {
                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                    p1c2.ResetToBeginning();p1c2.PlayForward();
                    p1c2s.PlayForward();
                    p1c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p2c2IsGiven)
                    {
                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c2.to = new Vector3(35f, 78f, 0f);
                        p2c2.ResetToBeginning();p2c2.PlayForward();
                        p2c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p3c2IsGiven)
                        {
                            p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c2.to = new Vector3(35f, 78f, 0f);
                            p3c2.ResetToBeginning();p3c2.PlayForward();
                            p3c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p4c2IsGiven)
                            {
                                p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                p4c2.to = new Vector3(35f, 78f, 0f);
                                p4c2.ResetToBeginning();p4c2.PlayForward();
                                p4c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p5c2IsGiven)
                                {
                                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c2.to = new Vector3(35f, 78f, 0f);
                                    p5c2.ResetToBeginning();p5c2.PlayForward();
                                    p5c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p6c2IsGiven)
                                    {
                                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c2.to = new Vector3(35f, 78f, 0f);
                                        p6c2.ResetToBeginning();p6c2.PlayForward();
                                        p6c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p1c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {
                if (Players.instance.p5IsPlay == 1 && !p1c2IsGiven)
                {
                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                    p1c2.ResetToBeginning();p1c2.PlayForward();
                    p1c2s.PlayForward();
                    p1c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p2c2IsGiven)
                    {
                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c2.to = new Vector3(35f, 78f, 0f);
                        p2c2.ResetToBeginning();p2c2.PlayForward();
                        p2c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p3c2IsGiven)
                        {
                            p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c2.to = new Vector3(35f, 78f, 0f);
                            p3c2.ResetToBeginning();p3c2.PlayForward();
                            p3c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p4c2IsGiven)
                            {
                                p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                p4c2.to = new Vector3(35f, 78f, 0f);
                                p4c2.ResetToBeginning();p4c2.PlayForward();
                                p4c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p5c2IsGiven)
                                {
                                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c2.to = new Vector3(35f, 78f, 0f);
                                    p5c2.ResetToBeginning();p5c2.PlayForward();
                                    p5c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p6c2IsGiven)
                                    {
                                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c2.to = new Vector3(35f, 78f, 0f);
                                        p6c2.ResetToBeginning();p6c2.PlayForward();
                                        p6c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p1c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {
                if (Players.instance.p6IsPlay == 1 && !p1c2IsGiven)
                {
                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                    p1c2.ResetToBeginning();p1c2.PlayForward();
                    p1c2s.PlayForward();
                    p1c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p2c2IsGiven)
                    {
                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c2.to = new Vector3(35f, 78f, 0f);
                        p2c2.ResetToBeginning();p2c2.PlayForward();
                        p2c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p3c2IsGiven)
                        {
                            p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c2.to = new Vector3(35f, 78f, 0f);
                            p3c2.ResetToBeginning();p3c2.PlayForward();
                            p3c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p4c2IsGiven)
                            {
                                p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                p4c2.to = new Vector3(35f, 78f, 0f);
                                p4c2.ResetToBeginning();p4c2.PlayForward();
                                p4c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p5c2IsGiven)
                                {
                                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c2.to = new Vector3(35f, 78f, 0f);
                                    p5c2.ResetToBeginning();p5c2.PlayForward();
                                    p5c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p6c2IsGiven)
                                    {
                                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c2.to = new Vector3(35f, 78f, 0f);
                                        p6c2.ResetToBeginning();p6c2.PlayForward();
                                        p6c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p1c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p2c2TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p2IsPlay == 1 && !p2c2IsGiven)
                {
                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c2.to = new Vector3(35f, 78f, 0f);
                    p2c2.ResetToBeginning();p2c2.PlayForward();
                    p2c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p3c2IsGiven)
                    {
                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c2.to = new Vector3(35f, 78f, 0f);
                        p3c2.ResetToBeginning();p3c2.PlayForward();
                        p3c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p4c2IsGiven)
                        {
                            p4c2.from = new Vector3(-27.1f, -112f, 0f);
                            p4c2.to = new Vector3(35f, 78f, 0f);
                            p4c2.ResetToBeginning();p4c2.PlayForward();
                            p4c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p5c2IsGiven)
                            {
                                p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c2.to = new Vector3(35f, 78f, 0f);
                                p5c2.ResetToBeginning();p5c2.PlayForward();
                                p5c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p6c2IsGiven)
                                {
                                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c2.to = new Vector3(35f, 78f, 0f);
                                    p6c2.ResetToBeginning();p6c2.PlayForward();
                                    p6c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p1c2IsGiven)
                                    {
                                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                        p1c2.ResetToBeginning();p1c2.PlayForward();
                                        p1c2s.PlayForward();
                                        p1c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p3IsPlay == 1 && !p2c2IsGiven)
                {
                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c2.to = new Vector3(35f, 78f, 0f);
                    p2c2.ResetToBeginning();p2c2.PlayForward();
                    p2c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p3c2IsGiven)
                    {
                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c2.to = new Vector3(35f, 78f, 0f);
                        p3c2.ResetToBeginning();p3c2.PlayForward();
                        p3c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p4c2IsGiven)
                        {
                            p4c2.from = new Vector3(-27.1f, -112f, 0f);
                            p4c2.to = new Vector3(35f, 78f, 0f);
                            p4c2.ResetToBeginning();p4c2.PlayForward();
                            p4c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p5c2IsGiven)
                            {
                                p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c2.to = new Vector3(35f, 78f, 0f);
                                p5c2.ResetToBeginning();p5c2.PlayForward();
                                p5c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p6c2IsGiven)
                                {
                                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c2.to = new Vector3(35f, 78f, 0f);
                                    p6c2.ResetToBeginning();p6c2.PlayForward();
                                    p6c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p1c2IsGiven)
                                    {
                                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                        p1c2.ResetToBeginning();p1c2.PlayForward();
                                        p1c2s.PlayForward();
                                        p1c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p4IsPlay == 1 && !p2c2IsGiven)
                {
                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c2.to = new Vector3(35f, 78f, 0f);
                    p2c2.ResetToBeginning();p2c2.PlayForward();
                    p2c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p3c2IsGiven)
                    {
                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c2.to = new Vector3(35f, 78f, 0f);
                        p3c2.ResetToBeginning();p3c2.PlayForward();
                        p3c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p4c2IsGiven)
                        {
                            p4c2.from = new Vector3(-27.1f, -112f, 0f);
                            p4c2.to = new Vector3(35f, 78f, 0f);
                            p4c2.ResetToBeginning();p4c2.PlayForward();
                            p4c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p5c2IsGiven)
                            {
                                p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c2.to = new Vector3(35f, 78f, 0f);
                                p5c2.ResetToBeginning();p5c2.PlayForward();
                                p5c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p6c2IsGiven)
                                {
                                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c2.to = new Vector3(35f, 78f, 0f);
                                    p6c2.ResetToBeginning();p6c2.PlayForward();
                                    p6c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p1c2IsGiven)
                                    {
                                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                        p1c2.ResetToBeginning();p1c2.PlayForward();
                                        p1c2s.PlayForward();
                                        p1c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p5IsPlay == 1 && !p2c2IsGiven)
                {
                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c2.to = new Vector3(35f, 78f, 0f);
                    p2c2.ResetToBeginning();p2c2.PlayForward();
                    p2c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p3c2IsGiven)
                    {
                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c2.to = new Vector3(35f, 78f, 0f);
                        p3c2.ResetToBeginning();p3c2.PlayForward();
                        p3c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p4c2IsGiven)
                        {
                            p4c2.from = new Vector3(-27.1f, -112f, 0f);
                            p4c2.to = new Vector3(35f, 78f, 0f);
                            p4c2.ResetToBeginning();p4c2.PlayForward();
                            p4c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p5c2IsGiven)
                            {
                                p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c2.to = new Vector3(35f, 78f, 0f);
                                p5c2.ResetToBeginning();p5c2.PlayForward();
                                p5c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p6c2IsGiven)
                                {
                                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c2.to = new Vector3(35f, 78f, 0f);
                                    p6c2.ResetToBeginning();p6c2.PlayForward();
                                    p6c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p1c2IsGiven)
                                    {
                                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                        p1c2.ResetToBeginning();p1c2.PlayForward();
                                        p1c2s.PlayForward();
                                        p1c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p6IsPlay == 1 && !p2c2IsGiven)
                {
                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c2.to = new Vector3(35f, 78f, 0f);
                    p2c2.ResetToBeginning();p2c2.PlayForward();
                    p2c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p3c2IsGiven)
                    {
                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c2.to = new Vector3(35f, 78f, 0f);
                        p3c2.ResetToBeginning();p3c2.PlayForward();
                        p3c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p4c2IsGiven)
                        {
                            p4c2.from = new Vector3(-27.1f, -112f, 0f);
                            p4c2.to = new Vector3(35f, 78f, 0f);
                            p4c2.ResetToBeginning();p4c2.PlayForward();
                            p4c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p5c2IsGiven)
                            {
                                p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c2.to = new Vector3(35f, 78f, 0f);
                                p5c2.ResetToBeginning();p5c2.PlayForward();
                                p5c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p6c2IsGiven)
                                {
                                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c2.to = new Vector3(35f, 78f, 0f);
                                    p6c2.ResetToBeginning();p6c2.PlayForward();
                                    p6c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p1c2IsGiven)
                                    {
                                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                        p1c2.ResetToBeginning();p1c2.PlayForward();
                                        p1c2s.PlayForward();
                                        p1c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p1IsPlay == 1 && !p2c2IsGiven)
                {
                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c2.to = new Vector3(35f, 78f, 0f);
                    p2c2.ResetToBeginning();p2c2.PlayForward();
                    p2c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p3c2IsGiven)
                    {
                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c2.to = new Vector3(35f, 78f, 0f);
                        p3c2.ResetToBeginning();p3c2.PlayForward();
                        p3c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p4c2IsGiven)
                        {
                            p4c2.from = new Vector3(-27.1f, -112f, 0f);
                            p4c2.to = new Vector3(35f, 78f, 0f);
                            p4c2.ResetToBeginning();p4c2.PlayForward();
                            p4c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p5c2IsGiven)
                            {
                                p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c2.to = new Vector3(35f, 78f, 0f);
                                p5c2.ResetToBeginning();p5c2.PlayForward();
                                p5c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p6c2IsGiven)
                                {
                                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c2.to = new Vector3(35f, 78f, 0f);
                                    p6c2.ResetToBeginning();p6c2.PlayForward();
                                    p6c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p1c2IsGiven)
                                    {
                                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                        p1c2.ResetToBeginning();p1c2.PlayForward();
                                        p1c2s.PlayForward();
                                        p1c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p2c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p3c2TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if(Table.instance.place_id == 1)
        {

                if (Players.instance.p3IsPlay == 1 && !p3c2IsGiven)
                {
                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c2.to = new Vector3(35f, 78f, 0f);
                    p3c2.ResetToBeginning();p3c2.PlayForward();
                    p3c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p4c2IsGiven)
                    {
                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                        p4c2.to = new Vector3(35f, 78f, 0f);
                        p4c2.ResetToBeginning();p4c2.PlayForward();
                        p4c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p5c2IsGiven)
                        {
                            p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c2.to = new Vector3(35f, 78f, 0f);
                            p5c2.ResetToBeginning();p5c2.PlayForward();
                            p5c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p6c2IsGiven)
                            {
                                p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c2.to = new Vector3(35f, 78f, 0f);
                                p6c2.ResetToBeginning();p6c2.PlayForward();
                                p6c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p1c2IsGiven)
                                {
                                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                    p1c2.ResetToBeginning();p1c2.PlayForward();
                                    p1c2s.PlayForward();
                                    p1c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p2c2IsGiven)
                                    {
                                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c2.to = new Vector3(35f, 78f, 0f);
                                        p2c2.ResetToBeginning();p2c2.PlayForward();
                                        p2c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p4IsPlay == 1 && !p3c2IsGiven)
                {
                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c2.to = new Vector3(35f, 78f, 0f);
                    p3c2.ResetToBeginning();p3c2.PlayForward();
                    p3c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p4c2IsGiven)
                    {
                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                        p4c2.to = new Vector3(35f, 78f, 0f);
                        p4c2.ResetToBeginning();p4c2.PlayForward();
                        p4c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p5c2IsGiven)
                        {
                            p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c2.to = new Vector3(35f, 78f, 0f);
                            p5c2.ResetToBeginning();p5c2.PlayForward();
                            p5c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p6c2IsGiven)
                            {
                                p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c2.to = new Vector3(35f, 78f, 0f);
                                p6c2.ResetToBeginning();p6c2.PlayForward();
                                p6c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p1c2IsGiven)
                                {
                                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                    p1c2.ResetToBeginning();p1c2.PlayForward();
                                    p1c2s.PlayForward();
                                    p1c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p2c2IsGiven)
                                    {
                                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c2.to = new Vector3(35f, 78f, 0f);
                                        p2c2.ResetToBeginning();p2c2.PlayForward();
                                        p2c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p5IsPlay == 1 && !p3c2IsGiven)
                {
                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c2.to = new Vector3(35f, 78f, 0f);
                    p3c2.ResetToBeginning();p3c2.PlayForward();
                    p3c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p4c2IsGiven)
                    {
                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                        p4c2.to = new Vector3(35f, 78f, 0f);
                        p4c2.ResetToBeginning();p4c2.PlayForward();
                        p4c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p5c2IsGiven)
                        {
                            p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c2.to = new Vector3(35f, 78f, 0f);
                            p5c2.ResetToBeginning();p5c2.PlayForward();
                            p5c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p6c2IsGiven)
                            {
                                p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c2.to = new Vector3(35f, 78f, 0f);
                                p6c2.ResetToBeginning();p6c2.PlayForward();
                                p6c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p1c2IsGiven)
                                {
                                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                    p1c2.ResetToBeginning();p1c2.PlayForward();
                                    p1c2s.PlayForward();
                                    p1c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p2c2IsGiven)
                                    {
                                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c2.to = new Vector3(35f, 78f, 0f);
                                        p2c2.ResetToBeginning();p2c2.PlayForward();
                                        p2c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p6IsPlay == 1 && !p3c2IsGiven)
                {
                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c2.to = new Vector3(35f, 78f, 0f);
                    p3c2.ResetToBeginning();p3c2.PlayForward();
                    p3c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p4c2IsGiven)
                    {
                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                        p4c2.to = new Vector3(35f, 78f, 0f);
                        p4c2.ResetToBeginning();p4c2.PlayForward();
                        p4c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p5c2IsGiven)
                        {
                            p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c2.to = new Vector3(35f, 78f, 0f);
                            p5c2.ResetToBeginning();p5c2.PlayForward();
                            p5c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p6c2IsGiven)
                            {
                                p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c2.to = new Vector3(35f, 78f, 0f);
                                p6c2.ResetToBeginning();p6c2.PlayForward();
                                p6c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p1c2IsGiven)
                                {
                                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                    p1c2.ResetToBeginning();p1c2.PlayForward();
                                    p1c2s.PlayForward();
                                    p1c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p2c2IsGiven)
                                    {
                                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c2.to = new Vector3(35f, 78f, 0f);
                                        p2c2.ResetToBeginning();p2c2.PlayForward();
                                        p2c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p1IsPlay == 1 && !p3c2IsGiven)
                {
                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c2.to = new Vector3(35f, 78f, 0f);
                    p3c2.ResetToBeginning();p3c2.PlayForward();
                    p3c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p4c2IsGiven)
                    {
                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                        p4c2.to = new Vector3(35f, 78f, 0f);
                        p4c2.ResetToBeginning();p4c2.PlayForward();
                        p4c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p5c2IsGiven)
                        {
                            p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c2.to = new Vector3(35f, 78f, 0f);
                            p5c2.ResetToBeginning();p5c2.PlayForward();
                            p5c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p6c2IsGiven)
                            {
                                p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c2.to = new Vector3(35f, 78f, 0f);
                                p6c2.ResetToBeginning();p6c2.PlayForward();
                                p6c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p1c2IsGiven)
                                {
                                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                    p1c2.ResetToBeginning();p1c2.PlayForward();
                                    p1c2s.PlayForward();
                                    p1c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p2c2IsGiven)
                                    {
                                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c2.to = new Vector3(35f, 78f, 0f);
                                        p2c2.ResetToBeginning();p2c2.PlayForward();
                                        p2c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p2IsPlay == 1 && !p3c2IsGiven)
                {
                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c2.to = new Vector3(35f, 78f, 0f);
                    p3c2.ResetToBeginning();p3c2.PlayForward();
                    p3c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p4c2IsGiven)
                    {
                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                        p4c2.to = new Vector3(35f, 78f, 0f);
                        p4c2.ResetToBeginning();p4c2.PlayForward();
                        p4c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p5c2IsGiven)
                        {
                            p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c2.to = new Vector3(35f, 78f, 0f);
                            p5c2.ResetToBeginning();p5c2.PlayForward();
                            p5c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p6c2IsGiven)
                            {
                                p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c2.to = new Vector3(35f, 78f, 0f);
                                p6c2.ResetToBeginning();p6c2.PlayForward();
                                p6c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p1c2IsGiven)
                                {
                                    p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                    p1c2.ResetToBeginning();p1c2.PlayForward();
                                    p1c2s.PlayForward();
                                    p1c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p2c2IsGiven)
                                    {
                                        p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c2.to = new Vector3(35f, 78f, 0f);
                                        p2c2.ResetToBeginning();p2c2.PlayForward();
                                        p2c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p3c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p4c2TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p4IsPlay == 1 && !p4c2IsGiven)
                {
                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                    p4c2.to = new Vector3(35f, 78f, 0f);
                    p4c2.ResetToBeginning();p4c2.PlayForward();
                    p4c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p5c2IsGiven)
                    {
                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c2.to = new Vector3(35f, 78f, 0f);
                        p5c2.ResetToBeginning();p5c2.PlayForward();
                        p5c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p6c2IsGiven)
                        {
                            p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c2.to = new Vector3(35f, 78f, 0f);
                            p6c2.ResetToBeginning();p6c2.PlayForward();
                            p6c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p1c2IsGiven)
                            {
                                p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                p1c2.ResetToBeginning();p1c2.PlayForward();
                                p1c2s.PlayForward();
                                p1c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p2c2IsGiven)
                                {
                                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c2.to = new Vector3(35f, 78f, 0f);
                                    p2c2.ResetToBeginning();p2c2.PlayForward();
                                    p2c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p3c2IsGiven)
                                    {
                                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c2.to = new Vector3(35f, 78f, 0f);
                                        p3c2.ResetToBeginning();p3c2.PlayForward();
                                        p3c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p5IsPlay == 1 && !p4c2IsGiven)
                {
                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                    p4c2.to = new Vector3(35f, 78f, 0f);
                    p4c2.ResetToBeginning();p4c2.PlayForward();
                    p4c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p5c2IsGiven)
                    {
                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c2.to = new Vector3(35f, 78f, 0f);
                        p5c2.ResetToBeginning();p5c2.PlayForward();
                        p5c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p6c2IsGiven)
                        {
                            p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c2.to = new Vector3(35f, 78f, 0f);
                            p6c2.ResetToBeginning();p6c2.PlayForward();
                            p6c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p1c2IsGiven)
                            {
                                p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                p1c2.ResetToBeginning();p1c2.PlayForward();
                                p1c2s.PlayForward();
                                p1c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p2c2IsGiven)
                                {
                                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c2.to = new Vector3(35f, 78f, 0f);
                                    p2c2.ResetToBeginning();p2c2.PlayForward();
                                    p2c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p3c2IsGiven)
                                    {
                                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c2.to = new Vector3(35f, 78f, 0f);
                                        p3c2.ResetToBeginning();p3c2.PlayForward();
                                        p3c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p6IsPlay == 1 && !p4c2IsGiven)
                {
                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                    p4c2.to = new Vector3(35f, 78f, 0f);
                    p4c2.ResetToBeginning();p4c2.PlayForward();
                    p4c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p5c2IsGiven)
                    {
                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c2.to = new Vector3(35f, 78f, 0f);
                        p5c2.ResetToBeginning();p5c2.PlayForward();
                        p5c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p6c2IsGiven)
                        {
                            p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c2.to = new Vector3(35f, 78f, 0f);
                            p6c2.ResetToBeginning();p6c2.PlayForward();
                            p6c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p1c2IsGiven)
                            {
                                p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                p1c2.ResetToBeginning();p1c2.PlayForward();
                                p1c2s.PlayForward();
                                p1c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p2c2IsGiven)
                                {
                                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c2.to = new Vector3(35f, 78f, 0f);
                                    p2c2.ResetToBeginning();p2c2.PlayForward();
                                    p2c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p3c2IsGiven)
                                    {
                                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c2.to = new Vector3(35f, 78f, 0f);
                                        p3c2.ResetToBeginning();p3c2.PlayForward();
                                        p3c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p1IsPlay == 1 && !p4c2IsGiven)
                {
                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                    p4c2.to = new Vector3(35f, 78f, 0f);
                    p4c2.ResetToBeginning();p4c2.PlayForward();
                    p4c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p5c2IsGiven)
                    {
                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c2.to = new Vector3(35f, 78f, 0f);
                        p5c2.ResetToBeginning();p5c2.PlayForward();
                        p5c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p6c2IsGiven)
                        {
                            p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c2.to = new Vector3(35f, 78f, 0f);
                            p6c2.ResetToBeginning();p6c2.PlayForward();
                            p6c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p1c2IsGiven)
                            {
                                p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                p1c2.ResetToBeginning();p1c2.PlayForward();
                                p1c2s.PlayForward();
                                p1c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p2c2IsGiven)
                                {
                                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c2.to = new Vector3(35f, 78f, 0f);
                                    p2c2.ResetToBeginning();p2c2.PlayForward();
                                    p2c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p3c2IsGiven)
                                    {
                                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c2.to = new Vector3(35f, 78f, 0f);
                                        p3c2.ResetToBeginning();p3c2.PlayForward();
                                        p3c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p2IsPlay == 1 && !p4c2IsGiven)
                {
                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                    p4c2.to = new Vector3(35f, 78f, 0f);
                    p4c2.ResetToBeginning();p4c2.PlayForward();
                    p4c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p5c2IsGiven)
                    {
                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c2.to = new Vector3(35f, 78f, 0f);
                        p5c2.ResetToBeginning();p5c2.PlayForward();
                        p5c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p6c2IsGiven)
                        {
                            p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c2.to = new Vector3(35f, 78f, 0f);
                            p6c2.ResetToBeginning();p6c2.PlayForward();
                            p6c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p1c2IsGiven)
                            {
                                p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                p1c2.ResetToBeginning();p1c2.PlayForward();
                                p1c2s.PlayForward();
                                p1c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p2c2IsGiven)
                                {
                                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c2.to = new Vector3(35f, 78f, 0f);
                                    p2c2.ResetToBeginning();p2c2.PlayForward();
                                    p2c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p3c2IsGiven)
                                    {
                                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c2.to = new Vector3(35f, 78f, 0f);
                                        p3c2.ResetToBeginning();p3c2.PlayForward();
                                        p3c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p3IsPlay == 1 && !p4c2IsGiven)
                {
                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                    p4c2.to = new Vector3(35f, 78f, 0f);
                    p4c2.ResetToBeginning();p4c2.PlayForward();
                    p4c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p5c2IsGiven)
                    {
                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c2.to = new Vector3(35f, 78f, 0f);
                        p5c2.ResetToBeginning();p5c2.PlayForward();
                        p5c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p6c2IsGiven)
                        {
                            p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c2.to = new Vector3(35f, 78f, 0f);
                            p6c2.ResetToBeginning();p6c2.PlayForward();
                            p6c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p1c2IsGiven)
                            {
                                p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                                p1c2.ResetToBeginning();p1c2.PlayForward();
                                p1c2s.PlayForward();
                                p1c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p2c2IsGiven)
                                {
                                    p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c2.to = new Vector3(35f, 78f, 0f);
                                    p2c2.ResetToBeginning();p2c2.PlayForward();
                                    p2c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p3c2IsGiven)
                                    {
                                        p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c2.to = new Vector3(35f, 78f, 0f);
                                        p3c2.ResetToBeginning();p3c2.PlayForward();
                                        p3c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p4c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p5c2TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p5IsPlay == 1 && !p5c2IsGiven)
                {
                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c2.to = new Vector3(35f, 78f, 0f);
                    p5c2.ResetToBeginning();p5c2.PlayForward();
                    p5c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p6c2IsGiven)
                    {
                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c2.to = new Vector3(35f, 78f, 0f);
                        p6c2.ResetToBeginning();p6c2.PlayForward();
                        p6c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p1c2IsGiven)
                        {
                            p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                            p1c2.ResetToBeginning();p1c2.PlayForward();
                            p1c2s.PlayForward();
                            p1c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p2c2IsGiven)
                            {
                                p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c2.to = new Vector3(35f, 78f, 0f);
                                p2c2.ResetToBeginning();p2c2.PlayForward();
                                p2c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p3c2IsGiven)
                                {
                                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c2.to = new Vector3(35f, 78f, 0f);
                                    p3c2.ResetToBeginning();p3c2.PlayForward();
                                    p3c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p4c2IsGiven)
                                    {
                                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c2.to = new Vector3(35f, 78f, 0f);
                                        p4c2.ResetToBeginning();p4c2.PlayForward();
                                        p4c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p5c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p6IsPlay == 1 && !p5c2IsGiven)
                {
                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c2.to = new Vector3(35f, 78f, 0f);
                    p5c2.ResetToBeginning();p5c2.PlayForward();
                    p5c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p6c2IsGiven)
                    {
                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c2.to = new Vector3(35f, 78f, 0f);
                        p6c2.ResetToBeginning();p6c2.PlayForward();
                        p6c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p1c2IsGiven)
                        {
                            p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                            p1c2.ResetToBeginning();p1c2.PlayForward();
                            p1c2s.PlayForward();
                            p1c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p2c2IsGiven)
                            {
                                p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c2.to = new Vector3(35f, 78f, 0f);
                                p2c2.ResetToBeginning();p2c2.PlayForward();
                                p2c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p3c2IsGiven)
                                {
                                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c2.to = new Vector3(35f, 78f, 0f);
                                    p3c2.ResetToBeginning();p3c2.PlayForward();
                                    p3c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p4c2IsGiven)
                                    {
                                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c2.to = new Vector3(35f, 78f, 0f);
                                        p4c2.ResetToBeginning();p4c2.PlayForward();
                                        p4c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p5c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p1IsPlay == 1 && !p5c2IsGiven)
                {
                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c2.to = new Vector3(35f, 78f, 0f);
                    p5c2.ResetToBeginning();p5c2.PlayForward();
                    p5c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p6c2IsGiven)
                    {
                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c2.to = new Vector3(35f, 78f, 0f);
                        p6c2.ResetToBeginning();p6c2.PlayForward();
                        p6c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p1c2IsGiven)
                        {
                            p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                            p1c2.ResetToBeginning();p1c2.PlayForward();
                            p1c2s.PlayForward();
                            p1c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p2c2IsGiven)
                            {
                                p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c2.to = new Vector3(35f, 78f, 0f);
                                p2c2.ResetToBeginning();p2c2.PlayForward();
                                p2c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p3c2IsGiven)
                                {
                                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c2.to = new Vector3(35f, 78f, 0f);
                                    p3c2.ResetToBeginning();p3c2.PlayForward();
                                    p3c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p4c2IsGiven)
                                    {
                                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c2.to = new Vector3(35f, 78f, 0f);
                                        p4c2.ResetToBeginning();p4c2.PlayForward();
                                        p4c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p5c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p2IsPlay == 1 && !p5c2IsGiven)
                {
                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c2.to = new Vector3(35f, 78f, 0f);
                    p5c2.ResetToBeginning();p5c2.PlayForward();
                    p5c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p6c2IsGiven)
                    {
                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c2.to = new Vector3(35f, 78f, 0f);
                        p6c2.ResetToBeginning();p6c2.PlayForward();
                        p6c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p1c2IsGiven)
                        {
                            p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                            p1c2.ResetToBeginning();p1c2.PlayForward();
                            p1c2s.PlayForward();
                            p1c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p2c2IsGiven)
                            {
                                p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c2.to = new Vector3(35f, 78f, 0f);
                                p2c2.ResetToBeginning();p2c2.PlayForward();
                                p2c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p3c2IsGiven)
                                {
                                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c2.to = new Vector3(35f, 78f, 0f);
                                    p3c2.ResetToBeginning();p3c2.PlayForward();
                                    p3c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p4c2IsGiven)
                                    {
                                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c2.to = new Vector3(35f, 78f, 0f);
                                        p4c2.ResetToBeginning();p4c2.PlayForward();
                                        p4c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p5c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p3IsPlay == 1 && !p5c2IsGiven)
                {
                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c2.to = new Vector3(35f, 78f, 0f);
                    p5c2.ResetToBeginning();p5c2.PlayForward();
                    p5c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p6c2IsGiven)
                    {
                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c2.to = new Vector3(35f, 78f, 0f);
                        p6c2.ResetToBeginning();p6c2.PlayForward();
                        p6c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p1c2IsGiven)
                        {
                            p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                            p1c2.ResetToBeginning();p1c2.PlayForward();
                            p1c2s.PlayForward();
                            p1c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p2c2IsGiven)
                            {
                                p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c2.to = new Vector3(35f, 78f, 0f);
                                p2c2.ResetToBeginning();p2c2.PlayForward();
                                p2c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p3c2IsGiven)
                                {
                                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c2.to = new Vector3(35f, 78f, 0f);
                                    p3c2.ResetToBeginning();p3c2.PlayForward();
                                    p3c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p4c2IsGiven)
                                    {
                                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c2.to = new Vector3(35f, 78f, 0f);
                                        p4c2.ResetToBeginning();p4c2.PlayForward();
                                        p4c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p5c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p4IsPlay == 1 && !p5c2IsGiven)
                {
                    p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c2.to = new Vector3(35f, 78f, 0f);
                    p5c2.ResetToBeginning();p5c2.PlayForward();
                    p5c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p6c2IsGiven)
                    {
                        p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c2.to = new Vector3(35f, 78f, 0f);
                        p6c2.ResetToBeginning();p6c2.PlayForward();
                        p6c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p1c2IsGiven)
                        {
                            p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                            p1c2.ResetToBeginning();p1c2.PlayForward();
                            p1c2s.PlayForward();
                            p1c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p2c2IsGiven)
                            {
                                p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c2.to = new Vector3(35f, 78f, 0f);
                                p2c2.ResetToBeginning();p2c2.PlayForward();
                                p2c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p3c2IsGiven)
                                {
                                    p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c2.to = new Vector3(35f, 78f, 0f);
                                    p3c2.ResetToBeginning();p3c2.PlayForward();
                                    p3c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p4c2IsGiven)
                                    {
                                        p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c2.to = new Vector3(35f, 78f, 0f);
                                        p4c2.ResetToBeginning();p4c2.PlayForward();
                                        p4c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p5c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p6c2TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p6IsPlay == 1 && !p6c2IsGiven)
                {
                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c2.to = new Vector3(35f, 78f, 0f);
                    p6c2.ResetToBeginning();p6c2.PlayForward();
                    p6c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p1c2IsGiven)
                    {
                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                        p1c2.ResetToBeginning();p1c2.PlayForward();
                        p1c2s.PlayForward();
                        p1c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p2c2IsGiven)
                        {
                            p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c2.to = new Vector3(35f, 78f, 0f);
                            p2c2.ResetToBeginning();p2c2.PlayForward();
                            p2c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p3c2IsGiven)
                            {
                                p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c2.to = new Vector3(35f, 78f, 0f);
                                p3c2.ResetToBeginning();p3c2.PlayForward();
                                p3c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p4c2IsGiven)
                                {
                                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c2.to = new Vector3(35f, 78f, 0f);
                                    p4c2.ResetToBeginning();p4c2.PlayForward();
                                    p4c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p5c2IsGiven)
                                    {
                                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c2.to = new Vector3(35f, 78f, 0f);
                                        p5c2.ResetToBeginning();p5c2.PlayForward();
                                        p5c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p6c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p1IsPlay == 1 && !p6c2IsGiven)
                {
                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c2.to = new Vector3(35f, 78f, 0f);
                    p6c2.ResetToBeginning();p6c2.PlayForward();
                    p6c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p1c2IsGiven)
                    {
                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                        p1c2.ResetToBeginning();p1c2.PlayForward();
                        p1c2s.PlayForward();
                        p1c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p2c2IsGiven)
                        {
                            p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c2.to = new Vector3(35f, 78f, 0f);
                            p2c2.ResetToBeginning();p2c2.PlayForward();
                            p2c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p3c2IsGiven)
                            {
                                p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c2.to = new Vector3(35f, 78f, 0f);
                                p3c2.ResetToBeginning();p3c2.PlayForward();
                                p3c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p4c2IsGiven)
                                {
                                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c2.to = new Vector3(35f, 78f, 0f);
                                    p4c2.ResetToBeginning();p4c2.PlayForward();
                                    p4c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p5c2IsGiven)
                                    {
                                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c2.to = new Vector3(35f, 78f, 0f);
                                        p5c2.ResetToBeginning();p5c2.PlayForward();
                                        p5c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p6c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p2IsPlay == 1 && !p6c2IsGiven)
                {
                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c2.to = new Vector3(35f, 78f, 0f);
                    p6c2.ResetToBeginning();p6c2.PlayForward();
                    p6c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p1c2IsGiven)
                    {
                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                        p1c2.ResetToBeginning();p1c2.PlayForward();
                        p1c2s.PlayForward();
                        p1c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p2c2IsGiven)
                        {
                            p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c2.to = new Vector3(35f, 78f, 0f);
                            p2c2.ResetToBeginning();p2c2.PlayForward();
                            p2c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p3c2IsGiven)
                            {
                                p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c2.to = new Vector3(35f, 78f, 0f);
                                p3c2.ResetToBeginning();p3c2.PlayForward();
                                p3c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p4c2IsGiven)
                                {
                                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c2.to = new Vector3(35f, 78f, 0f);
                                    p4c2.ResetToBeginning();p4c2.PlayForward();
                                    p4c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p5c2IsGiven)
                                    {
                                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c2.to = new Vector3(35f, 78f, 0f);
                                        p5c2.ResetToBeginning();p5c2.PlayForward();
                                        p5c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p6c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p3IsPlay == 1 && !p6c2IsGiven)
                {
                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c2.to = new Vector3(35f, 78f, 0f);
                    p6c2.ResetToBeginning();p6c2.PlayForward();
                    p6c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p1c2IsGiven)
                    {
                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                        p1c2.ResetToBeginning();p1c2.PlayForward();
                        p1c2s.PlayForward();
                        p1c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p2c2IsGiven)
                        {
                            p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c2.to = new Vector3(35f, 78f, 0f);
                            p2c2.ResetToBeginning();p2c2.PlayForward();
                            p2c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p3c2IsGiven)
                            {
                                p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c2.to = new Vector3(35f, 78f, 0f);
                                p3c2.ResetToBeginning();p3c2.PlayForward();
                                p3c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p4c2IsGiven)
                                {
                                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c2.to = new Vector3(35f, 78f, 0f);
                                    p4c2.ResetToBeginning();p4c2.PlayForward();
                                    p4c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p5c2IsGiven)
                                    {
                                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c2.to = new Vector3(35f, 78f, 0f);
                                        p5c2.ResetToBeginning();p5c2.PlayForward();
                                        p5c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p6c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p4IsPlay == 1 && !p6c2IsGiven)
                {
                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c2.to = new Vector3(35f, 78f, 0f);
                    p6c2.ResetToBeginning();p6c2.PlayForward();
                    p6c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p1c2IsGiven)
                    {
                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                        p1c2.ResetToBeginning();p1c2.PlayForward();
                        p1c2s.PlayForward();
                        p1c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p2c2IsGiven)
                        {
                            p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c2.to = new Vector3(35f, 78f, 0f);
                            p2c2.ResetToBeginning();p2c2.PlayForward();
                            p2c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p3c2IsGiven)
                            {
                                p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c2.to = new Vector3(35f, 78f, 0f);
                                p3c2.ResetToBeginning();p3c2.PlayForward();
                                p3c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p4c2IsGiven)
                                {
                                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c2.to = new Vector3(35f, 78f, 0f);
                                    p4c2.ResetToBeginning();p4c2.PlayForward();
                                    p4c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p5c2IsGiven)
                                    {
                                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c2.to = new Vector3(35f, 78f, 0f);
                                        p5c2.ResetToBeginning();p5c2.PlayForward();
                                        p5c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p6c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p5IsPlay == 1 && !p6c2IsGiven)
                {
                    p6c2.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c2.to = new Vector3(35f, 78f, 0f);
                    p6c2.ResetToBeginning();p6c2.PlayForward();
                    p6c2IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p1c2IsGiven)
                    {
                        p1c2.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c2.to = new Vector3(15.4f, -130.3f, 0f);
                        p1c2.ResetToBeginning();p1c2.PlayForward();
                        p1c2s.PlayForward();
                        p1c2IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p2c2IsGiven)
                        {
                            p2c2.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c2.to = new Vector3(35f, 78f, 0f);
                            p2c2.ResetToBeginning();p2c2.PlayForward();
                            p2c2IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p3c2IsGiven)
                            {
                                p3c2.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c2.to = new Vector3(35f, 78f, 0f);
                                p3c2.ResetToBeginning();p3c2.PlayForward();
                                p3c2IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p4c2IsGiven)
                                {
                                    p4c2.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c2.to = new Vector3(35f, 78f, 0f);
                                    p4c2.ResetToBeginning();p4c2.PlayForward();
                                    p4c2IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p5c2IsGiven)
                                    {
                                        p5c2.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c2.to = new Vector3(35f, 78f, 0f);
                                        p5c2.ResetToBeginning();p5c2.PlayForward();
                                        p5c2IsGiven = true;
                                    }
                                    else
                                    {
                                        p6c3TweenPos();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p1c3TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {
                if (Players.instance.p1IsPlay == 1 && !p1c3IsGiven)
                {
                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                    p1c3.ResetToBeginning();p1c3.PlayForward();
                    p1c3s.PlayForward();
                    p1c3IsGiven = true;
                    

                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p2c3IsGiven)
                    {
                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c3.to = new Vector3(80f, 78f, 0f);
                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                        p2c3IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p3c3IsGiven)
                        {
                            p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c3.to = new Vector3(80f, 78f, 0f);
                            p3c3.ResetToBeginning();p3c3.PlayForward();
                            p3c3IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p4c3IsGiven)
                            {
                                p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                p4c3.to = new Vector3(80f, 78f, 0f);
                                p4c3.ResetToBeginning();p4c3.PlayForward();
                                p4c3IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p5c3IsGiven)
                                {
                                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c3.to = new Vector3(80f, 78f, 0f);
                                    p5c3.ResetToBeginning();p5c3.PlayForward();
                                    p5c3IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p6c3IsGiven)
                                    {
                                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c3.to = new Vector3(80f, 78f, 0f);
                                        p6c3.ResetToBeginning();p6c3.PlayForward();
                                        p6c3IsGiven = true;

                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {
                if (Players.instance.p2IsPlay == 1 && !p1c3IsGiven)
                {
                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                    p1c3.ResetToBeginning();p1c3.PlayForward();
                    p1c3s.PlayForward();
                    p1c3IsGiven = true;
                    

                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p2c3IsGiven)
                    {
                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c3.to = new Vector3(80f, 78f, 0f);
                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                        p2c3IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p3c3IsGiven)
                        {
                            p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c3.to = new Vector3(80f, 78f, 0f);
                            p3c3.ResetToBeginning();p3c3.PlayForward();
                            p3c3IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p4c3IsGiven)
                            {
                                p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                p4c3.to = new Vector3(80f, 78f, 0f);
                                p4c3.ResetToBeginning();p4c3.PlayForward();
                                p4c3IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p5c3IsGiven)
                                {
                                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c3.to = new Vector3(80f, 78f, 0f);
                                    p5c3.ResetToBeginning();p5c3.PlayForward();
                                    p5c3IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p6c3IsGiven)
                                    {
                                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c3.to = new Vector3(80f, 78f, 0f);
                                        p6c3.ResetToBeginning();p6c3.PlayForward();
                                        p6c3IsGiven = true;

                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {
                if (Players.instance.p3IsPlay == 1 && !p1c3IsGiven)
                {
                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                    p1c3.ResetToBeginning();p1c3.PlayForward();
                    p1c3s.PlayForward();
                    p1c3IsGiven = true;
                    
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p2c3IsGiven)
                    {
                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c3.to = new Vector3(80f, 78f, 0f);
                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                        p2c3IsGiven = true;

                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p3c3IsGiven)
                        {
                            p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c3.to = new Vector3(80f, 78f, 0f);
                            p3c3.ResetToBeginning();p3c3.PlayForward();
                            p3c3IsGiven = true;

                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p4c3IsGiven)
                            {
                                p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                p4c3.to = new Vector3(80f, 78f, 0f);
                                p4c3.ResetToBeginning();p4c3.PlayForward();
                                p4c3IsGiven = true;

                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p5c3IsGiven)
                                {
                                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c3.to = new Vector3(80f, 78f, 0f);
                                    p5c3.ResetToBeginning();p5c3.PlayForward();
                                    p5c3IsGiven = true;

                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p6c3IsGiven)
                                    {
                                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c3.to = new Vector3(80f, 78f, 0f);
                                        p6c3.ResetToBeginning();p6c3.PlayForward();
                                        p6c3IsGiven = true;
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {
                if (Players.instance.p4IsPlay == 1 && !p1c3IsGiven)
                {
                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                    p1c3.ResetToBeginning();p1c3.PlayForward();
                    p1c3s.PlayForward();
                    p1c3IsGiven = true;
                    
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p2c3IsGiven)
                    {
                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c3.to = new Vector3(80f, 78f, 0f);
                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                        p2c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p3c3IsGiven)
                        {
                            p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c3.to = new Vector3(80f, 78f, 0f);
                            p3c3.ResetToBeginning();p3c3.PlayForward();
                            p3c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p4c3IsGiven)
                            {
                                p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                p4c3.to = new Vector3(80f, 78f, 0f);
                                p4c3.ResetToBeginning();p4c3.PlayForward();
                                p4c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p5c3IsGiven)
                                {
                                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c3.to = new Vector3(80f, 78f, 0f);
                                    p5c3.ResetToBeginning();p5c3.PlayForward();
                                    p5c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p6c3IsGiven)
                                    {
                                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c3.to = new Vector3(80f, 78f, 0f);
                                        p6c3.ResetToBeginning();p6c3.PlayForward();
                                        p6c3IsGiven = true;
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {
                if (Players.instance.p5IsPlay == 1 && !p1c3IsGiven)
                {
                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                    p1c3.ResetToBeginning();p1c3.PlayForward();
                    p1c3s.PlayForward();
                    p1c3IsGiven = true;
                    
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p2c3IsGiven)
                    {
                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c3.to = new Vector3(80f, 78f, 0f);
                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                        p2c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p3c3IsGiven)
                        {
                            p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c3.to = new Vector3(80f, 78f, 0f);
                            p3c3.ResetToBeginning();p3c3.PlayForward();
                            p3c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p4c3IsGiven)
                            {
                                p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                p4c3.to = new Vector3(80f, 78f, 0f);
                                p4c3.ResetToBeginning();p4c3.PlayForward();
                                p4c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p5c3IsGiven)
                                {
                                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c3.to = new Vector3(80f, 78f, 0f);
                                    p5c3.ResetToBeginning();p5c3.PlayForward();
                                    p5c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p6c3IsGiven)
                                    {
                                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c3.to = new Vector3(80f, 78f, 0f);
                                        p6c3.ResetToBeginning();p6c3.PlayForward();
                                        p6c3IsGiven = true;
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {
                if (Players.instance.p6IsPlay == 1 && !p1c3IsGiven)
                {
                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                    p1c3.ResetToBeginning();p1c3.PlayForward();
                    p1c3s.PlayForward();
                    p1c3IsGiven = true;
                    
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p2c3IsGiven)
                    {
                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                        p2c3.to = new Vector3(80f, 78f, 0f);
                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                        p2c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p3c3IsGiven)
                        {
                            p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                            p3c3.to = new Vector3(80f, 78f, 0f);
                            p3c3.ResetToBeginning();p3c3.PlayForward();
                            p3c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p4c3IsGiven)
                            {
                                p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                p4c3.to = new Vector3(80f, 78f, 0f);
                                p4c3.ResetToBeginning();p4c3.PlayForward();
                                p4c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p5c3IsGiven)
                                {
                                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                    p5c3.to = new Vector3(80f, 78f, 0f);
                                    p5c3.ResetToBeginning();p5c3.PlayForward();
                                    p5c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p6c3IsGiven)
                                    {
                                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                        p6c3.to = new Vector3(80f, 78f, 0f);
                                        p6c3.ResetToBeginning();p6c3.PlayForward();
                                        p6c3IsGiven = true;
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p2c3TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p2IsPlay == 1 && !p2c3IsGiven)
                {
                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c3.to = new Vector3(80f, 78f, 0f);
                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                    p2c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p3c3IsGiven)
                    {
                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c3.to = new Vector3(80f, 78f, 0f);
                        p3c3.ResetToBeginning();p3c3.PlayForward();
                        p3c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p4c3IsGiven)
                        {
                            p4c3.from = new Vector3(-27.1f, -112f, 0f);
                            p4c3.to = new Vector3(80f, 78f, 0f);
                            p4c3.ResetToBeginning();p4c3.PlayForward();
                            p4c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p5c3IsGiven)
                            {
                                p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c3.to = new Vector3(80f, 78f, 0f);
                                p5c3.ResetToBeginning();p5c3.PlayForward();
                                p5c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p6c3IsGiven)
                                {
                                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c3.to = new Vector3(80f, 78f, 0f);
                                    p6c3.ResetToBeginning();p6c3.PlayForward();
                                    p6c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p1c3IsGiven)
                                    {
                                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                        p1c3.ResetToBeginning();p1c3.PlayForward();
                                        p1c3s.PlayForward();
                                        p1c3IsGiven = true;
                                        
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p3IsPlay == 1 && !p2c3IsGiven)
                {
                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c3.to = new Vector3(80f, 78f, 0f);
                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                    p2c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p3c3IsGiven)
                    {
                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c3.to = new Vector3(80f, 78f, 0f);
                        p3c3.ResetToBeginning();p3c3.PlayForward();
                        p3c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p4c3IsGiven)
                        {
                            p4c3.from = new Vector3(-27.1f, -112f, 0f);
                            p4c3.to = new Vector3(80f, 78f, 0f);
                            p4c3.ResetToBeginning();p4c3.PlayForward();
                            p4c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p5c3IsGiven)
                            {
                                p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c3.to = new Vector3(80f, 78f, 0f);
                                p5c3.ResetToBeginning();p5c3.PlayForward();
                                p5c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p6c3IsGiven)
                                {
                                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c3.to = new Vector3(80f, 78f, 0f);
                                    p6c3.ResetToBeginning();p6c3.PlayForward();
                                    p6c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p1c3IsGiven)
                                    {
                                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                        p1c3.ResetToBeginning();p1c3.PlayForward();
                                        p1c3s.PlayForward();
                                        p1c3IsGiven = true;
                                        
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p4IsPlay == 1 && !p2c3IsGiven)
                {
                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c3.to = new Vector3(80f, 78f, 0f);
                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                    p2c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p3c3IsGiven)
                    {
                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c3.to = new Vector3(80f, 78f, 0f);
                        p3c3.ResetToBeginning();p3c3.PlayForward();
                        p3c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p4c3IsGiven)
                        {
                            p4c3.from = new Vector3(-27.1f, -112f, 0f);
                            p4c3.to = new Vector3(80f, 78f, 0f);
                            p4c3.ResetToBeginning();p4c3.PlayForward();
                            p4c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p5c3IsGiven)
                            {
                                p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c3.to = new Vector3(80f, 78f, 0f);
                                p5c3.ResetToBeginning();p5c3.PlayForward();
                                p5c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p6c3IsGiven)
                                {
                                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c3.to = new Vector3(80f, 78f, 0f);
                                    p6c3.ResetToBeginning();p6c3.PlayForward();
                                    p6c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p1c3IsGiven)
                                    {
                                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                        p1c3.ResetToBeginning();p1c3.PlayForward();
                                        p1c3s.PlayForward();
                                        p1c3IsGiven = true;
                                       
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p5IsPlay == 1 && !p2c3IsGiven)
                {
                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c3.to = new Vector3(80f, 78f, 0f);
                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                    p2c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p3c3IsGiven)
                    {
                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c3.to = new Vector3(80f, 78f, 0f);
                        p3c3.ResetToBeginning();p3c3.PlayForward();
                        p3c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p4c3IsGiven)
                        {
                            p4c3.from = new Vector3(-27.1f, -112f, 0f);
                            p4c3.to = new Vector3(80f, 78f, 0f);
                            p4c3.ResetToBeginning();p4c3.PlayForward();
                            p4c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p5c3IsGiven)
                            {
                                p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c3.to = new Vector3(80f, 78f, 0f);
                                p5c3.ResetToBeginning();p5c3.PlayForward();
                                p5c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p6c3IsGiven)
                                {
                                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c3.to = new Vector3(80f, 78f, 0f);
                                    p6c3.ResetToBeginning();p6c3.PlayForward();
                                    p6c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p1c3IsGiven)
                                    {
                                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                        p1c3.ResetToBeginning();p1c3.PlayForward();
                                        p1c3s.PlayForward();
                                        p1c3IsGiven = true;
                                        
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p6IsPlay == 1 && !p2c3IsGiven)
                {
                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c3.to = new Vector3(80f, 78f, 0f);
                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                    p2c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p3c3IsGiven)
                    {
                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c3.to = new Vector3(80f, 78f, 0f);
                        p3c3.ResetToBeginning();p3c3.PlayForward();
                        p3c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p4c3IsGiven)
                        {
                            p4c3.from = new Vector3(-27.1f, -112f, 0f);
                            p4c3.to = new Vector3(80f, 78f, 0f);
                            p4c3.ResetToBeginning();p4c3.PlayForward();
                            p4c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p5c3IsGiven)
                            {
                                p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c3.to = new Vector3(80f, 78f, 0f);
                                p5c3.ResetToBeginning();p5c3.PlayForward();
                                p5c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p6c3IsGiven)
                                {
                                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c3.to = new Vector3(80f, 78f, 0f);
                                    p6c3.ResetToBeginning();p6c3.PlayForward();
                                    p6c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p1c3IsGiven)
                                    {
                                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                        p1c3.ResetToBeginning();p1c3.PlayForward();
                                        p1c3s.PlayForward();
                                        p1c3IsGiven = true;
                                        
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p1IsPlay == 1 && !p2c3IsGiven)
                {
                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                    p2c3.to = new Vector3(80f, 78f, 0f);
                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                    p2c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p3c3IsGiven)
                    {
                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                        p3c3.to = new Vector3(80f, 78f, 0f);
                        p3c3.ResetToBeginning();p3c3.PlayForward();
                        p3c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p4c3IsGiven)
                        {
                            p4c3.from = new Vector3(-27.1f, -112f, 0f);
                            p4c3.to = new Vector3(80f, 78f, 0f);
                            p4c3.ResetToBeginning();p4c3.PlayForward();
                            p4c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p5c3IsGiven)
                            {
                                p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                p5c3.to = new Vector3(80f, 78f, 0f);
                                p5c3.ResetToBeginning();p5c3.PlayForward();
                                p5c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p6c3IsGiven)
                                {
                                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                    p6c3.to = new Vector3(80f, 78f, 0f);
                                    p6c3.ResetToBeginning();p6c3.PlayForward();
                                    p6c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p1c3IsGiven)
                                    {
                                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                        p1c3.ResetToBeginning();p1c3.PlayForward();
                                        p1c3s.PlayForward();
                                        p1c3IsGiven = true;
                                        
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }     
    }
    public void p3c3TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p3IsPlay == 1 && !p3c3IsGiven)
                {
                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c3.to = new Vector3(80f, 78f, 0f);
                    p3c3.ResetToBeginning();p3c3.PlayForward();
                    p3c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p4c3IsGiven)
                    {
                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                        p4c3.to = new Vector3(80f, 78f, 0f);
                        p4c3.ResetToBeginning();p4c3.PlayForward();
                        p4c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p5c3IsGiven)
                        {
                            p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c3.to = new Vector3(80f, 78f, 0f);
                            p5c3.ResetToBeginning();p5c3.PlayForward();
                            p5c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p6c3IsGiven)
                            {
                                p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c3.to = new Vector3(80f, 78f, 0f);
                                p6c3.ResetToBeginning();p6c3.PlayForward();
                                p6c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p1c3IsGiven)
                                {
                                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                    p1c3.ResetToBeginning();p1c3.PlayForward();
                                    p1c3s.PlayForward();
                                    p1c3IsGiven = true;
                                    
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p2c3IsGiven)
                                    {
                                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c3.to = new Vector3(80f, 78f, 0f);
                                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                        p2c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p4IsPlay == 1 && !p3c3IsGiven)
                {
                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c3.to = new Vector3(80f, 78f, 0f);
                    p3c3.ResetToBeginning();p3c3.PlayForward();
                    p3c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p4c3IsGiven)
                    {
                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                        p4c3.to = new Vector3(80f, 78f, 0f);
                        p4c3.ResetToBeginning();p4c3.PlayForward();
                        p4c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p5c3IsGiven)
                        {
                            p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c3.to = new Vector3(80f, 78f, 0f);
                            p5c3.ResetToBeginning();p5c3.PlayForward();
                            p5c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p6c3IsGiven)
                            {
                                p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c3.to = new Vector3(80f, 78f, 0f);
                                p6c3.ResetToBeginning();p6c3.PlayForward();
                                p6c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p1c3IsGiven)
                                {
                                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                    p1c3.ResetToBeginning();p1c3.PlayForward();
                                    p1c3s.PlayForward();
                                    p1c3IsGiven = true;
                                    
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p2c3IsGiven)
                                    {
                                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c3.to = new Vector3(80f, 78f, 0f);
                                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                        p2c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p5IsPlay == 1 && !p3c3IsGiven)
                {
                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c3.to = new Vector3(80f, 78f, 0f);
                    p3c3.ResetToBeginning();p3c3.PlayForward();
                    p3c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p4c3IsGiven)
                    {
                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                        p4c3.to = new Vector3(80f, 78f, 0f);
                        p4c3.ResetToBeginning();p4c3.PlayForward();
                        p4c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p5c3IsGiven)
                        {
                            p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c3.to = new Vector3(80f, 78f, 0f);
                            p5c3.ResetToBeginning();p5c3.PlayForward();
                            p5c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p6c3IsGiven)
                            {
                                p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c3.to = new Vector3(80f, 78f, 0f);
                                p6c3.ResetToBeginning();p6c3.PlayForward();
                                p6c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p1c3IsGiven)
                                {
                                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                    p1c3.ResetToBeginning();p1c3.PlayForward();
                                    p1c3s.PlayForward();
                                    p1c3IsGiven = true;
                                    
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p2c3IsGiven)
                                    {
                                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c3.to = new Vector3(80f, 78f, 0f);
                                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                        p2c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p6IsPlay == 1 && !p3c3IsGiven)
                {
                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c3.to = new Vector3(80f, 78f, 0f);
                    p3c3.ResetToBeginning();p3c3.PlayForward();
                    p3c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p4c3IsGiven)
                    {
                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                        p4c3.to = new Vector3(80f, 78f, 0f);
                        p4c3.ResetToBeginning();p4c3.PlayForward();
                        p4c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p5c3IsGiven)
                        {
                            p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c3.to = new Vector3(80f, 78f, 0f);
                            p5c3.ResetToBeginning();p5c3.PlayForward();
                            p5c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p6c3IsGiven)
                            {
                                p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c3.to = new Vector3(80f, 78f, 0f);
                                p6c3.ResetToBeginning();p6c3.PlayForward();
                                p6c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p1c3IsGiven)
                                {
                                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                    p1c3.ResetToBeginning();p1c3.PlayForward();
                                    p1c3s.PlayForward();
                                    p1c3IsGiven = true;
                                    
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p2c3IsGiven)
                                    {
                                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c3.to = new Vector3(80f, 78f, 0f);
                                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                        p2c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p1IsPlay == 1 && !p3c3IsGiven)
                {
                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c3.to = new Vector3(80f, 78f, 0f);
                    p3c3.ResetToBeginning();p3c3.PlayForward();
                    p3c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p4c3IsGiven)
                    {
                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                        p4c3.to = new Vector3(80f, 78f, 0f);
                        p4c3.ResetToBeginning();p4c3.PlayForward();
                        p4c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p5c3IsGiven)
                        {
                            p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c3.to = new Vector3(80f, 78f, 0f);
                            p5c3.ResetToBeginning();p5c3.PlayForward();
                            p5c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p6c3IsGiven)
                            {
                                p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c3.to = new Vector3(80f, 78f, 0f);
                                p6c3.ResetToBeginning();p6c3.PlayForward();
                                p6c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p1c3IsGiven)
                                {
                                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                    p1c3.ResetToBeginning();p1c3.PlayForward();
                                    p1c3s.PlayForward();
                                    p1c3IsGiven = true;
                                    
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p2c3IsGiven)
                                    {
                                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c3.to = new Vector3(80f, 78f, 0f);
                                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                        p2c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p2IsPlay == 1 && !p3c3IsGiven)
                {
                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                    p3c3.to = new Vector3(80f, 78f, 0f);
                    p3c3.ResetToBeginning();p3c3.PlayForward();
                    p3c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p4c3IsGiven)
                    {
                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                        p4c3.to = new Vector3(80f, 78f, 0f);
                        p4c3.ResetToBeginning();p4c3.PlayForward();
                        p4c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p5c3IsGiven)
                        {
                            p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                            p5c3.to = new Vector3(80f, 78f, 0f);
                            p5c3.ResetToBeginning();p5c3.PlayForward();
                            p5c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p6c3IsGiven)
                            {
                                p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                                p6c3.to = new Vector3(80f, 78f, 0f);
                                p6c3.ResetToBeginning();p6c3.PlayForward();
                                p6c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p1c3IsGiven)
                                {
                                    p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                    p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                    p1c3.ResetToBeginning();p1c3.PlayForward();
                                    p1c3s.PlayForward();
                                    p1c3IsGiven = true;
                                    
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p2c3IsGiven)
                                    {
                                        p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                        p2c3.to = new Vector3(80f, 78f, 0f);
                                        p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                        p2c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void p4c3TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p4IsPlay == 1 && !p4c3IsGiven)
                {
                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                    p4c3.to = new Vector3(80f, 78f, 0f);
                    p4c3.ResetToBeginning();p4c3.PlayForward();
                    p4c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p5c3IsGiven)
                    {
                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c3.to = new Vector3(80f, 78f, 0f);
                        p5c3.ResetToBeginning();p5c3.PlayForward();
                        p5c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p6c3IsGiven)
                        {
                            p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c3.to = new Vector3(80f, 78f, 0f);
                            p6c3.ResetToBeginning();p6c3.PlayForward();
                            p6c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p1c3IsGiven)
                            {
                                p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                p1c3.ResetToBeginning();p1c3.PlayForward();
                                p1c3s.PlayForward();
                                p1c3IsGiven = true;
                                
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p2c3IsGiven)
                                {
                                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c3.to = new Vector3(80f, 78f, 0f);
                                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                    p2c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p3c3IsGiven)
                                    {
                                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c3.to = new Vector3(80f, 78f, 0f);
                                        p3c3.ResetToBeginning();p3c3.PlayForward();
                                        p3c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p5IsPlay == 1 && !p4c3IsGiven)
                {
                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                    p4c3.to = new Vector3(80f, 78f, 0f);
                    p4c3.ResetToBeginning();p4c3.PlayForward();
                    p4c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p5c3IsGiven)
                    {
                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c3.to = new Vector3(80f, 78f, 0f);
                        p5c3.ResetToBeginning();p5c3.PlayForward();
                        p5c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p6c3IsGiven)
                        {
                            p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c3.to = new Vector3(80f, 78f, 0f);
                            p6c3.ResetToBeginning();p6c3.PlayForward();
                            p6c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p1c3IsGiven)
                            {
                                p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                p1c3.ResetToBeginning();p1c3.PlayForward();
                                p1c3s.PlayForward();
                                p1c3IsGiven = true;
                                
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p2c3IsGiven)
                                {
                                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c3.to = new Vector3(80f, 78f, 0f);
                                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                    p2c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p3c3IsGiven)
                                    {
                                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c3.to = new Vector3(80f, 78f, 0f);
                                        p3c3.ResetToBeginning();p3c3.PlayForward();
                                        p3c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p6IsPlay == 1 && !p4c3IsGiven)
                {
                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                    p4c3.to = new Vector3(80f, 78f, 0f);
                    p4c3.ResetToBeginning();p4c3.PlayForward();
                    p4c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p5c3IsGiven)
                    {
                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c3.to = new Vector3(80f, 78f, 0f);
                        p5c3.ResetToBeginning();p5c3.PlayForward();
                        p5c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p6c3IsGiven)
                        {
                            p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c3.to = new Vector3(80f, 78f, 0f);
                            p6c3.ResetToBeginning();p6c3.PlayForward();
                            p6c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p1c3IsGiven)
                            {
                                p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                p1c3.ResetToBeginning();p1c3.PlayForward();
                                p1c3s.PlayForward();
                                p1c3IsGiven = true;
                                
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p2c3IsGiven)
                                {
                                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c3.to = new Vector3(80f, 78f, 0f);
                                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                    p2c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p3c3IsGiven)
                                    {
                                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c3.to = new Vector3(80f, 78f, 0f);
                                        p3c3.ResetToBeginning();p3c3.PlayForward();
                                        p3c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p1IsPlay == 1 && !p4c3IsGiven)
                {
                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                    p4c3.to = new Vector3(80f, 78f, 0f);
                    p4c3.ResetToBeginning();p4c3.PlayForward();
                    p4c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p5c3IsGiven)
                    {
                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c3.to = new Vector3(80f, 78f, 0f);
                        p5c3.ResetToBeginning();p5c3.PlayForward();
                        p5c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p6c3IsGiven)
                        {
                            p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c3.to = new Vector3(80f, 78f, 0f);
                            p6c3.ResetToBeginning();p6c3.PlayForward();
                            p6c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p1c3IsGiven)
                            {
                                p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                p1c3.ResetToBeginning();p1c3.PlayForward();
                                p1c3s.PlayForward();
                                p1c3IsGiven = true;
                                
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p2c3IsGiven)
                                {
                                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c3.to = new Vector3(80f, 78f, 0f);
                                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                    p2c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p3c3IsGiven)
                                    {
                                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c3.to = new Vector3(80f, 78f, 0f);
                                        p3c3.ResetToBeginning();p3c3.PlayForward();
                                        p3c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p2IsPlay == 1 && !p4c3IsGiven)
                {
                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                    p4c3.to = new Vector3(80f, 78f, 0f);
                    p4c3.ResetToBeginning();p4c3.PlayForward();
                    p4c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p5c3IsGiven)
                    {
                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c3.to = new Vector3(80f, 78f, 0f);
                        p5c3.ResetToBeginning();p5c3.PlayForward();
                        p5c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p6c3IsGiven)
                        {
                            p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c3.to = new Vector3(80f, 78f, 0f);
                            p6c3.ResetToBeginning();p6c3.PlayForward();
                            p6c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p1c3IsGiven)
                            {
                                p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                p1c3.ResetToBeginning();p1c3.PlayForward();
                                p1c3s.PlayForward();
                                p1c3IsGiven = true;
                                
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p2c3IsGiven)
                                {
                                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c3.to = new Vector3(80f, 78f, 0f);
                                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                    p2c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p3c3IsGiven)
                                    {
                                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c3.to = new Vector3(80f, 78f, 0f);
                                        p3c3.ResetToBeginning();p3c3.PlayForward();
                                        p3c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p3IsPlay == 1 && !p4c3IsGiven)
                {
                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                    p4c3.to = new Vector3(80f, 78f, 0f);
                    p4c3.ResetToBeginning();p4c3.PlayForward();
                    p4c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p5c3IsGiven)
                    {
                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                        p5c3.to = new Vector3(80f, 78f, 0f);
                        p5c3.ResetToBeginning();p5c3.PlayForward();
                        p5c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p6c3IsGiven)
                        {
                            p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                            p6c3.to = new Vector3(80f, 78f, 0f);
                            p6c3.ResetToBeginning();p6c3.PlayForward();
                            p6c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p1c3IsGiven)
                            {
                                p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                                p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                                p1c3.ResetToBeginning();p1c3.PlayForward();
                                p1c3s.PlayForward();
                                p1c3IsGiven = true;
                                
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p2c3IsGiven)
                                {
                                    p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                    p2c3.to = new Vector3(80f, 78f, 0f);
                                    p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                    p2c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p3c3IsGiven)
                                    {
                                        p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                        p3c3.to = new Vector3(80f, 78f, 0f);
                                        p3c3.ResetToBeginning();p3c3.PlayForward();
                                        p3c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }    
    }
    public void p5c3TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p5IsPlay == 1 && !p5c3IsGiven)
                {
                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c3.to = new Vector3(80f, 78f, 0f);
                    p5c3.ResetToBeginning();p5c3.PlayForward();
                    p5c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p6c3IsGiven)
                    {
                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c3.to = new Vector3(80f, 78f, 0f);
                        p6c3.ResetToBeginning();p6c3.PlayForward();
                        p6c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p1c3IsGiven)
                        {
                            p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                            p1c3.ResetToBeginning();p1c3.PlayForward();
                            p1c3s.PlayForward();
                            p1c3IsGiven = true;
                            
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p2c3IsGiven)
                            {
                                p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c3.to = new Vector3(80f, 78f, 0f);
                                p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                p2c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p3c3IsGiven)
                                {
                                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c3.to = new Vector3(80f, 78f, 0f);
                                    p3c3.ResetToBeginning();p3c3.PlayForward();
                                    p3c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p4c3IsGiven)
                                    {
                                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c3.to = new Vector3(80f, 78f, 0f);
                                        p4c3.ResetToBeginning();p4c3.PlayForward();
                                        p4c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p6IsPlay == 1 && !p5c3IsGiven)
                {
                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c3.to = new Vector3(80f, 78f, 0f);
                    p5c3.ResetToBeginning();p5c3.PlayForward();
                    p5c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p6c3IsGiven)
                    {
                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c3.to = new Vector3(80f, 78f, 0f);
                        p6c3.ResetToBeginning();p6c3.PlayForward();
                        p6c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p1c3IsGiven)
                        {
                            p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                            p1c3.ResetToBeginning();p1c3.PlayForward();
                            p1c3s.PlayForward();
                            p1c3IsGiven = true;
                            
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p2c3IsGiven)
                            {
                                p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c3.to = new Vector3(80f, 78f, 0f);
                                p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                p2c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p3c3IsGiven)
                                {
                                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c3.to = new Vector3(80f, 78f, 0f);
                                    p3c3.ResetToBeginning();p3c3.PlayForward();
                                    p3c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p4c3IsGiven)
                                    {
                                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c3.to = new Vector3(80f, 78f, 0f);
                                        p4c3.ResetToBeginning();p4c3.PlayForward();
                                        p4c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p1IsPlay == 1 && !p5c3IsGiven)
                {
                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c3.to = new Vector3(80f, 78f, 0f);
                    p5c3.ResetToBeginning();p5c3.PlayForward();
                    p5c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p6c3IsGiven)
                    {
                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c3.to = new Vector3(80f, 78f, 0f);
                        p6c3.ResetToBeginning();p6c3.PlayForward();
                        p6c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p1c3IsGiven)
                        {
                            p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                            p1c3.ResetToBeginning();p1c3.PlayForward();
                            p1c3s.PlayForward();
                            p1c3IsGiven = true;
                            
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p2c3IsGiven)
                            {
                                p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c3.to = new Vector3(80f, 78f, 0f);
                                p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                p2c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p3c3IsGiven)
                                {
                                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c3.to = new Vector3(80f, 78f, 0f);
                                    p3c3.ResetToBeginning();p3c3.PlayForward();
                                    p3c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p4c3IsGiven)
                                    {
                                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c3.to = new Vector3(80f, 78f, 0f);
                                        p4c3.ResetToBeginning();p4c3.PlayForward();
                                        p4c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p2IsPlay == 1 && !p5c3IsGiven)
                {
                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c3.to = new Vector3(80f, 78f, 0f);
                    p5c3.ResetToBeginning();p5c3.PlayForward();
                    p5c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p6c3IsGiven)
                    {
                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c3.to = new Vector3(80f, 78f, 0f);
                        p6c3.ResetToBeginning();p6c3.PlayForward();
                        p6c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p1c3IsGiven)
                        {
                            p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                            p1c3.ResetToBeginning();p1c3.PlayForward();
                            p1c3s.PlayForward();
                            p1c3IsGiven = true;
                            
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p2c3IsGiven)
                            {
                                p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c3.to = new Vector3(80f, 78f, 0f);
                                p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                p2c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p3c3IsGiven)
                                {
                                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c3.to = new Vector3(80f, 78f, 0f);
                                    p3c3.ResetToBeginning();p3c3.PlayForward();
                                    p3c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p4c3IsGiven)
                                    {
                                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c3.to = new Vector3(80f, 78f, 0f);
                                        p4c3.ResetToBeginning();p4c3.PlayForward();
                                        p4c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p3IsPlay == 1 && !p5c3IsGiven)
                {
                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c3.to = new Vector3(80f, 78f, 0f);
                    p5c3.ResetToBeginning();p5c3.PlayForward();
                    p5c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p6c3IsGiven)
                    {
                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c3.to = new Vector3(80f, 78f, 0f);
                        p6c3.ResetToBeginning();p6c3.PlayForward();
                        p6c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p1c3IsGiven)
                        {
                            p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                            p1c3.ResetToBeginning();p1c3.PlayForward();
                            p1c3s.PlayForward();
                            p1c3IsGiven = true;
                            
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p2c3IsGiven)
                            {
                                p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c3.to = new Vector3(80f, 78f, 0f);
                                p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                p2c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p3c3IsGiven)
                                {
                                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c3.to = new Vector3(80f, 78f, 0f);
                                    p3c3.ResetToBeginning();p3c3.PlayForward();
                                    p3c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p4c3IsGiven)
                                    {
                                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c3.to = new Vector3(80f, 78f, 0f);
                                        p4c3.ResetToBeginning();p4c3.PlayForward();
                                        p4c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p4IsPlay == 1 && !p5c3IsGiven)
                {
                    p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                    p5c3.to = new Vector3(80f, 78f, 0f);
                    p5c3.ResetToBeginning();p5c3.PlayForward();
                    p5c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p6c3IsGiven)
                    {
                        p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                        p6c3.to = new Vector3(80f, 78f, 0f);
                        p6c3.ResetToBeginning();p6c3.PlayForward();
                        p6c3IsGiven = true;
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p1c3IsGiven)
                        {
                            p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                            p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                            p1c3.ResetToBeginning();p1c3.PlayForward();
                            p1c3s.PlayForward();
                            p1c3IsGiven = true;
                            
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p2c3IsGiven)
                            {
                                p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                                p2c3.to = new Vector3(80f, 78f, 0f);
                                p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                                p2c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p3c3IsGiven)
                                {
                                    p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                    p3c3.to = new Vector3(80f, 78f, 0f);
                                    p3c3.ResetToBeginning();p3c3.PlayForward();
                                    p3c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p4c3IsGiven)
                                    {
                                        p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                        p4c3.to = new Vector3(80f, 78f, 0f);
                                        p4c3.ResetToBeginning();p4c3.PlayForward();
                                        p4c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }    
    }
    public void p6c3TweenPos()
    {
        if (Players.instance.start_game == 1)
        {
            if (Table.instance.place_id == 1)
            {

                if (Players.instance.p6IsPlay == 1 && !p6c3IsGiven)
                {
                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c3.to = new Vector3(80f, 78f, 0f);
                    p6c3.ResetToBeginning();p6c3.PlayForward();
                    p6c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p1IsPlay == 1 && !p1c3IsGiven)
                    {
                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                        p1c3.ResetToBeginning();p1c3.PlayForward();
                        p1c3s.PlayForward();
                        p1c3IsGiven = true;
                       
                    }
                    else
                    {
                        if (Players.instance.p2IsPlay == 1 && !p2c3IsGiven)
                        {
                            p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c3.to = new Vector3(80f, 78f, 0f);
                            p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                            p2c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p3IsPlay == 1 && !p3c3IsGiven)
                            {
                                p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c3.to = new Vector3(80f, 78f, 0f);
                                p3c3.ResetToBeginning();p3c3.PlayForward();
                                p3c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p4IsPlay == 1 && !p4c3IsGiven)
                                {
                                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c3.to = new Vector3(80f, 78f, 0f);
                                    p4c3.ResetToBeginning();p4c3.PlayForward();
                                    p4c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p5IsPlay == 1 && !p5c3IsGiven)
                                    {
                                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c3.to = new Vector3(80f, 78f, 0f);
                                        p5c3.ResetToBeginning();p5c3.PlayForward();
                                        p5c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 2)
            {

                if (Players.instance.p1IsPlay == 1 && !p6c3IsGiven)
                {
                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c3.to = new Vector3(80f, 78f, 0f);
                    p6c3.ResetToBeginning();p6c3.PlayForward();
                    p6c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p2IsPlay == 1 && !p1c3IsGiven)
                    {
                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                        p1c3.ResetToBeginning();p1c3.PlayForward();
                        p1c3s.PlayForward();
                        p1c3IsGiven = true;
                        
                    }
                    else
                    {
                        if (Players.instance.p3IsPlay == 1 && !p2c3IsGiven)
                        {
                            p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c3.to = new Vector3(80f, 78f, 0f);
                            p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                            p2c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p4IsPlay == 1 && !p3c3IsGiven)
                            {
                                p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c3.to = new Vector3(80f, 78f, 0f);
                                p3c3.ResetToBeginning();p3c3.PlayForward();
                                p3c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p5IsPlay == 1 && !p4c3IsGiven)
                                {
                                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c3.to = new Vector3(80f, 78f, 0f);
                                    p4c3.ResetToBeginning();p4c3.PlayForward();
                                    p4c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p6IsPlay == 1 && !p5c3IsGiven)
                                    {
                                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c3.to = new Vector3(80f, 78f, 0f);
                                        p5c3.ResetToBeginning();p5c3.PlayForward();
                                        p5c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 3)
            {

                if (Players.instance.p2IsPlay == 1 && !p6c3IsGiven)
                {
                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c3.to = new Vector3(80f, 78f, 0f);
                    p6c3.ResetToBeginning();p6c3.PlayForward();
                    p6c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p3IsPlay == 1 && !p1c3IsGiven)
                    {
                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                        p1c3.ResetToBeginning();p1c3.PlayForward();
                        p1c3s.PlayForward();
                        p1c3IsGiven = true;
                        
                    }
                    else
                    {
                        if (Players.instance.p4IsPlay == 1 && !p2c3IsGiven)
                        {
                            p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c3.to = new Vector3(80f, 78f, 0f);
                            p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                            p2c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p5IsPlay == 1 && !p3c3IsGiven)
                            {
                                p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c3.to = new Vector3(80f, 78f, 0f);
                                p3c3.ResetToBeginning();p3c3.PlayForward();
                                p3c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p6IsPlay == 1 && !p4c3IsGiven)
                                {
                                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c3.to = new Vector3(80f, 78f, 0f);
                                    p4c3.ResetToBeginning();p4c3.PlayForward();
                                    p4c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p1IsPlay == 1 && !p5c3IsGiven)
                                    {
                                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c3.to = new Vector3(80f, 78f, 0f);
                                        p5c3.ResetToBeginning();p5c3.PlayForward();
                                        p5c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 4)
            {

                if (Players.instance.p3IsPlay == 1 && !p6c3IsGiven)
                {
                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c3.to = new Vector3(80f, 78f, 0f);
                    p6c3.ResetToBeginning();p6c3.PlayForward();
                    p6c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p4IsPlay == 1 && !p1c3IsGiven)
                    {
                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                        p1c3.ResetToBeginning();p1c3.PlayForward();
                        p1c3s.PlayForward();
                        p1c3IsGiven = true;
                        
                    }
                    else
                    {
                        if (Players.instance.p5IsPlay == 1 && !p2c3IsGiven)
                        {
                            p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c3.to = new Vector3(80f, 78f, 0f);
                            p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                            p2c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p6IsPlay == 1 && !p3c3IsGiven)
                            {
                                p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c3.to = new Vector3(80f, 78f, 0f);
                                p3c3.ResetToBeginning();p3c3.PlayForward();
                                p3c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p1IsPlay == 1 && !p4c3IsGiven)
                                {
                                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c3.to = new Vector3(80f, 78f, 0f);
                                    p4c3.ResetToBeginning();p4c3.PlayForward();
                                    p4c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p2IsPlay == 1 && !p5c3IsGiven)
                                    {
                                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c3.to = new Vector3(80f, 78f, 0f);
                                        p5c3.ResetToBeginning();p5c3.PlayForward();
                                        p5c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 5)
            {

                if (Players.instance.p4IsPlay == 1 && !p6c3IsGiven)
                {
                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c3.to = new Vector3(80f, 78f, 0f);
                    p6c3.ResetToBeginning();p6c3.PlayForward();
                    p6c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p5IsPlay == 1 && !p1c3IsGiven)
                    {
                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                        p1c3.ResetToBeginning();p1c3.PlayForward();
                        p1c3s.PlayForward();
                        p1c3IsGiven = true;
                        
                    }
                    else
                    {
                        if (Players.instance.p6IsPlay == 1 && !p2c3IsGiven)
                        {
                            p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c3.to = new Vector3(80f, 78f, 0f);
                            p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                            p2c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p1IsPlay == 1 && !p3c3IsGiven)
                            {
                                p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c3.to = new Vector3(80f, 78f, 0f);
                                p3c3.ResetToBeginning();p3c3.PlayForward();
                                p3c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p2IsPlay == 1 && !p4c3IsGiven)
                                {
                                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c3.to = new Vector3(80f, 78f, 0f);
                                    p4c3.ResetToBeginning();p4c3.PlayForward();
                                    p4c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p3IsPlay == 1 && !p5c3IsGiven)
                                    {
                                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c3.to = new Vector3(80f, 78f, 0f);
                                        p5c3.ResetToBeginning();p5c3.PlayForward();
                                        p5c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (Table.instance.place_id == 6)
            {

                if (Players.instance.p5IsPlay == 1 && !p6c3IsGiven)
                {
                    p6c3.from = new Vector3(-500.8f, 181.8f, 0f);
                    p6c3.to = new Vector3(80f, 78f, 0f);
                    p6c3.ResetToBeginning();p6c3.PlayForward();
                    p6c3IsGiven = true;
                }
                else
                {
                    if (Players.instance.p6IsPlay == 1 && !p1c3IsGiven)
                    {
                        p1c3.from = new Vector3(-27.3f, 258.5f, 0f);
                        p1c3.to = new Vector3(151.1f, -130.3f, 0f);
                        p1c3.ResetToBeginning();p1c3.PlayForward();
                        p1c3s.PlayForward();
                        p1c3IsGiven = true;
                    }

                    else
                    {
                        if (Players.instance.p1IsPlay == 1 && !p2c3IsGiven)
                        {
                            p2c3.from = new Vector3(417.5f, 181.8f, 0f);
                            p2c3.to = new Vector3(80f, 78f, 0f);
                            p2c3.ResetToBeginning();p2c3.ResetToBeginning();p2c3.PlayForward();
                            p2c3IsGiven = true;
                        }
                        else
                        {
                            if (Players.instance.p2IsPlay == 1 && !p3c3IsGiven)
                            {
                                p3c3.from = new Vector3(417.5f, -8.9f, 0f);
                                p3c3.to = new Vector3(80f, 78f, 0f);
                                p3c3.ResetToBeginning();p3c3.PlayForward();
                                p3c3IsGiven = true;
                            }
                            else
                            {
                                if (Players.instance.p3IsPlay == 1 && !p4c3IsGiven)
                                {
                                    p4c3.from = new Vector3(-27.1f, -112f, 0f);
                                    p4c3.to = new Vector3(80f, 78f, 0f);
                                    p4c3.ResetToBeginning();p4c3.PlayForward();
                                    p4c3IsGiven = true;
                                }
                                else
                                {
                                    if (Players.instance.p4IsPlay == 1 && !p5c3IsGiven)
                                    {
                                        p5c3.from = new Vector3(-500.7f, -9.2f, 0f);
                                        p5c3.to = new Vector3(80f, 78f, 0f);
                                        p5c3.ResetToBeginning();p5c3.PlayForward();
                                        p5c3IsGiven = true;
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }     
    }
    public void ShowP1C1()
    {
        if(p1c1IsGiven)
        {
            if (Table.instance.place_id == 1)
            {
                GameManagerScript.instance.cards[1].spriteName = Players.instance.p1Card1;
                p1c1BTN.normalSprite = Players.instance.p1Card1;
            }
            if (Table.instance.place_id == 2)
            {
                GameManagerScript.instance.cards[1].spriteName = Players.instance.p2Card1;
                p1c1BTN.normalSprite = Players.instance.p2Card1;
            }
            if (Table.instance.place_id == 3)
            {
                GameManagerScript.instance.cards[1].spriteName = Players.instance.p3Card1;
                p1c1BTN.normalSprite = Players.instance.p3Card1;
            }
            if (Table.instance.place_id == 4)
            {
                GameManagerScript.instance.cards[1].spriteName = Players.instance.p4Card1;
                p1c1BTN.normalSprite = Players.instance.p4Card1;
            }
            if (Table.instance.place_id == 5)
            {
                GameManagerScript.instance.cards[1].spriteName = Players.instance.p4Card1;
                p1c1BTN.normalSprite = Players.instance.p5Card1;
            }
            if (Table.instance.place_id == 6)
            {
                GameManagerScript.instance.cards[1].spriteName = Players.instance.p6Card1;
                p1c1BTN.normalSprite = Players.instance.p6Card1;
            }
        }
    }
    public void ShowP1C2()
    {
        if (p1c2IsGiven)
        {
            if (Table.instance.place_id == 1)
            {
                GameManagerScript.instance.cards[2].spriteName = Players.instance.p1Card2;
                p1c2BTN.normalSprite = Players.instance.p1Card2;
            }
            if (Table.instance.place_id == 2)
            {
                GameManagerScript.instance.cards[2].spriteName = Players.instance.p2Card2;
                p1c2BTN.normalSprite = Players.instance.p2Card2;
            }
            if (Table.instance.place_id == 3)
            {
                GameManagerScript.instance.cards[2].spriteName = Players.instance.p3Card2;
                p1c2BTN.normalSprite = Players.instance.p3Card2;
            }
            if (Table.instance.place_id == 4)
            {
                GameManagerScript.instance.cards[2].spriteName = Players.instance.p4Card2;
                p1c2BTN.normalSprite = Players.instance.p4Card2;
            }
            if (Table.instance.place_id == 5)
            {
                GameManagerScript.instance.cards[2].spriteName = Players.instance.p4Card2;
                p1c2BTN.normalSprite = Players.instance.p5Card2;
            }
            if (Table.instance.place_id == 6)
            {
                GameManagerScript.instance.cards[2].spriteName = Players.instance.p6Card2;
                p1c2BTN.normalSprite = Players.instance.p6Card2;
            }
        }
    }
    public void ShowP1C3()
    {
        if (p1c3IsGiven)
        {
            if (Table.instance.place_id == 1)
            {
                GameManagerScript.instance.cards[3].spriteName = Players.instance.p1Card3;
                p1c3BTN.normalSprite = Players.instance.p1Card3;
            }
            if (Table.instance.place_id == 2)
            {
                GameManagerScript.instance.cards[3].spriteName = Players.instance.p2Card3;
                p1c3BTN.normalSprite = Players.instance.p2Card3;
            }
            if (Table.instance.place_id == 3)
            {
                GameManagerScript.instance.cards[3].spriteName = Players.instance.p3Card3;
                p1c3BTN.normalSprite = Players.instance.p3Card3;
            }
            if (Table.instance.place_id == 4)
            {
                GameManagerScript.instance.cards[3].spriteName = Players.instance.p4Card3;
                p1c3BTN.normalSprite = Players.instance.p4Card3;
            }
            if (Table.instance.place_id == 5)
            {
                GameManagerScript.instance.cards[3].spriteName = Players.instance.p4Card3;
                p1c3BTN.normalSprite = Players.instance.p5Card3;
            }
            if (Table.instance.place_id == 6)
            {
                GameManagerScript.instance.cards[3].spriteName = Players.instance.p6Card3;
                p1c3BTN.normalSprite = Players.instance.p6Card3;
            }
        }
    }
    public void HodAnimationPid1()
    {
        if (Players.instance.p2Card1 != "0" && Players.instance.p2Card1 != null)
        {
            cardTweenPositions[3].duration = 0.2f;
            HodP2C1();
            GameManagerScript.instance.cards[4].spriteName = Players.instance.p2Card1;
        }
        if (Players.instance.p2Card2 != "0" && Players.instance.p2Card2 != null)
        {
            cardTweenPositions[4].duration = 0.2f;
            HodP2C2();
            GameManagerScript.instance.cards[5].spriteName = Players.instance.p2Card2;
        }
        if (Players.instance.p2Card3 != "0" && Players.instance.p2Card3 != null)
        {
            cardTweenPositions[5].duration = 0.2f;
            HodP2C3();
            GameManagerScript.instance.cards[6].spriteName = Players.instance.p2Card3;
        }
        if (Players.instance.p3Card1 != "0" && Players.instance.p3Card1 != null)
        {
            cardTweenPositions[6].duration = 0.2f;
            HodP3C1();
            GameManagerScript.instance.cards[7].spriteName = Players.instance.p3Card1;
        }
        if (Players.instance.p3Card2 != "0" && Players.instance.p3Card2 != null)
        {
            cardTweenPositions[7].duration = 0.2f;
            HodP3C2();
            GameManagerScript.instance.cards[8].spriteName = Players.instance.p3Card2;
        }
        if (Players.instance.p3Card3 != "0" && Players.instance.p3Card3 != null)
        {
            cardTweenPositions[8].duration = 0.2f;
            HodP3C3();
            GameManagerScript.instance.cards[9].spriteName = Players.instance.p3Card3;
        }
        if (Players.instance.p4Card1 != "0" && Players.instance.p4Card1 != null)
        {
            cardTweenPositions[9].duration = 0.2f;
            HodP4C1();
            GameManagerScript.instance.cards[10].spriteName = Players.instance.p4Card1;
        }
        if (Players.instance.p4Card2 != "0" && Players.instance.p4Card2 != null)
        {
            cardTweenPositions[10].duration = 0.2f;
            HodP4C2();
            GameManagerScript.instance.cards[11].spriteName = Players.instance.p4Card2;
        }
        if (Players.instance.p4Card3 != "0" && Players.instance.p4Card3 != null)
        {
            cardTweenPositions[11].duration = 0.2f;
            HodP4C3();
            GameManagerScript.instance.cards[12].spriteName = Players.instance.p4Card3;
        }
        if (Players.instance.p5Card1 != "0" && Players.instance.p5Card1 != null)
        {
            cardTweenPositions[12].duration = 0.2f;
            HodP5C1();
            GameManagerScript.instance.cards[13].spriteName = Players.instance.p5Card1;
        }
        if (Players.instance.p5Card2 != "0" && Players.instance.p5Card2 != null)
        {
            cardTweenPositions[13].duration = 0.2f;
            HodP5C2();
            GameManagerScript.instance.cards[14].spriteName = Players.instance.p5Card2;
        }
        if (Players.instance.p5Card3 != "0" && Players.instance.p5Card3 != null)
        {
            cardTweenPositions[14].duration = 0.2f;
            HodP5C3();
            GameManagerScript.instance.cards[15].spriteName = Players.instance.p5Card3;
        }
        if (Players.instance.p6Card1 != "0" && Players.instance.p6Card1 != null)
        {
            cardTweenPositions[15].duration = 0.2f;
            HodP6C1();
            GameManagerScript.instance.cards[16].spriteName = Players.instance.p6Card1;
        }
        if (Players.instance.p6Card2 != "0" && Players.instance.p6Card2 != null)
        {
            cardTweenPositions[16].duration = 0.2f;
            HodP6C2();
            GameManagerScript.instance.cards[17].spriteName = Players.instance.p6Card2;
        }
        if (Players.instance.p6Card3 != "0" && Players.instance.p6Card3 != null)
        {
            cardTweenPositions[17].duration = 0.2f;
            HodP6C3();
            GameManagerScript.instance.cards[18].spriteName = Players.instance.p6Card3;
        }           
    }
    public void HodAnimationPid2()
    {
        if (Players.instance.p1Card1 != "0" && Players.instance.p2Card1 != null)
        {
            cardTweenPositions[15].duration = 0.2f;
            HodP6C1();
            GameManagerScript.instance.cards[16].spriteName = Players.instance.p1Card1;
        }
        if (Players.instance.p1Card2 != "0" && Players.instance.p2Card2 != null)
        {
            cardTweenPositions[16].duration = 0.2f;
            HodP6C2();
            GameManagerScript.instance.cards[17].spriteName = Players.instance.p1Card2;
        }
        if (Players.instance.p1Card3 != "0" && Players.instance.p2Card3 != null)
        {
            cardTweenPositions[17].duration = 0.2f;
            HodP6C3();
            GameManagerScript.instance.cards[18].spriteName = Players.instance.p1Card3;
        }
        if (Players.instance.p3Card1 != "0" && Players.instance.p3Card1 != null)
        {
            cardTweenPositions[3].duration = 0.2f;
            HodP2C1();
            GameManagerScript.instance.cards[4].spriteName = Players.instance.p3Card1;
        }
        if (Players.instance.p3Card2 != "0" && Players.instance.p3Card2 != null)
        {
            cardTweenPositions[4].duration = 0.2f;
            HodP2C2();
            GameManagerScript.instance.cards[5].spriteName = Players.instance.p3Card2;
        }
        if (Players.instance.p3Card3 != "0" && Players.instance.p3Card3 != null)
        {
            cardTweenPositions[5].duration = 0.2f;
            HodP2C3();
            GameManagerScript.instance.cards[6].spriteName = Players.instance.p3Card3;
        }
        if (Players.instance.p4Card1 != "0" && Players.instance.p4Card1 != null)
        {
            cardTweenPositions[6].duration = 0.2f;
            HodP3C1();
            GameManagerScript.instance.cards[7].spriteName = Players.instance.p4Card1;
        }
        if (Players.instance.p4Card2 != "0" && Players.instance.p4Card2 != null)
        {
            cardTweenPositions[7].duration = 0.2f;
            HodP3C2();
            GameManagerScript.instance.cards[8].spriteName = Players.instance.p4Card2;
        }
        if (Players.instance.p4Card3 != "0" && Players.instance.p4Card3 != null)
        {
            cardTweenPositions[8].duration = 0.2f;
            HodP3C3();
            GameManagerScript.instance.cards[9].spriteName = Players.instance.p4Card3;
        }
        if (Players.instance.p5Card1 != "0" && Players.instance.p5Card1 != null)
        {
            cardTweenPositions[9].duration = 0.2f;
            HodP4C1();
            GameManagerScript.instance.cards[10].spriteName = Players.instance.p5Card1;
        }
        if (Players.instance.p5Card2 != "0" && Players.instance.p5Card2 != null)
        {
            cardTweenPositions[10].duration = 0.2f;
            HodP4C2();
            GameManagerScript.instance.cards[11].spriteName = Players.instance.p5Card2;
        }
        if (Players.instance.p5Card3 != "0" && Players.instance.p5Card3 != null)
        {
            cardTweenPositions[11].duration = 0.2f;
            HodP4C3();
            GameManagerScript.instance.cards[12].spriteName = Players.instance.p5Card3;
        }
        if (Players.instance.p6Card1 != "0" && Players.instance.p6Card1 != null)
        {
            cardTweenPositions[12].duration = 0.2f;
            HodP5C1();
            GameManagerScript.instance.cards[13].spriteName = Players.instance.p6Card1;
        }
        if (Players.instance.p6Card2 != "0" && Players.instance.p6Card2 != null)
        {
            cardTweenPositions[13].duration = 0.2f;
            HodP5C2();
            GameManagerScript.instance.cards[14].spriteName = Players.instance.p6Card2;
        }
        if (Players.instance.p6Card3 != "0" && Players.instance.p6Card3 != null)
        {
            cardTweenPositions[14].duration = 0.2f;
            HodP5C3();
            GameManagerScript.instance.cards[15].spriteName = Players.instance.p6Card3;
        }
    }
    public void HodAnimationPid3()
    {
        if (Players.instance.p1Card1 != "0" && Players.instance.p2Card1 != null)
        {
            cardTweenPositions[12].duration = 0.2f;
            HodP5C1();
            GameManagerScript.instance.cards[13].spriteName = Players.instance.p1Card1;
        }
        if (Players.instance.p1Card2 != "0" && Players.instance.p2Card2 != null)
        {
            cardTweenPositions[13].duration = 0.2f;
            HodP5C2();
            GameManagerScript.instance.cards[14].spriteName = Players.instance.p1Card2;
        }
        if (Players.instance.p1Card3 != "0" && Players.instance.p2Card3 != null)
        {
            cardTweenPositions[14].duration = 0.2f;
            HodP5C3();
            GameManagerScript.instance.cards[15].spriteName = Players.instance.p1Card3;
        }
        if (Players.instance.p2Card1 != "0" && Players.instance.p3Card1 != null)
        {
            cardTweenPositions[15].duration = 0.2f;
            HodP6C1();
            GameManagerScript.instance.cards[16].spriteName = Players.instance.p2Card1;
        }
        if (Players.instance.p2Card2 != "0" && Players.instance.p3Card2 != null)
        {
            cardTweenPositions[16].duration = 0.2f;
            HodP6C2();
            GameManagerScript.instance.cards[17].spriteName = Players.instance.p2Card2;
        }
        if (Players.instance.p2Card3 != "0" && Players.instance.p3Card3 != null)
        {
            cardTweenPositions[17].duration = 0.2f;
            HodP6C3();
            GameManagerScript.instance.cards[18].spriteName = Players.instance.p2Card3;
        }
        if (Players.instance.p4Card1 != "0" && Players.instance.p4Card1 != null)
        {
            cardTweenPositions[3].duration = 0.2f;
            HodP2C1();
            GameManagerScript.instance.cards[4].spriteName = Players.instance.p4Card1;
        }
        if (Players.instance.p4Card2 != "0" && Players.instance.p4Card2 != null)
        {
            cardTweenPositions[4].duration = 0.2f;
            HodP2C2();
            GameManagerScript.instance.cards[5].spriteName = Players.instance.p4Card2;
        }
        if (Players.instance.p4Card3 != "0" && Players.instance.p4Card3 != null)
        {
            cardTweenPositions[5].duration = 0.2f;
            HodP2C3();
            GameManagerScript.instance.cards[6].spriteName = Players.instance.p4Card3;
        }
        if (Players.instance.p5Card1 != "0" && Players.instance.p5Card1 != null)
        {
            cardTweenPositions[6].duration = 0.2f;
            HodP3C1();
            GameManagerScript.instance.cards[7].spriteName = Players.instance.p5Card1;
        }
        if (Players.instance.p5Card2 != "0" && Players.instance.p5Card2 != null)
        {
            cardTweenPositions[7].duration = 0.2f;
            HodP3C2();
            GameManagerScript.instance.cards[8].spriteName = Players.instance.p5Card2;
        }
        if (Players.instance.p5Card3 != "0" && Players.instance.p5Card3 != null)
        {
            cardTweenPositions[8].duration = 0.2f;
            HodP3C3();
            GameManagerScript.instance.cards[9].spriteName = Players.instance.p5Card3;
        }
        if (Players.instance.p6Card1 != "0" && Players.instance.p6Card1 != null)
        {
            cardTweenPositions[9].duration = 0.2f;
            HodP4C1();
            GameManagerScript.instance.cards[10].spriteName = Players.instance.p6Card1;
        }
        if (Players.instance.p6Card2 != "0" && Players.instance.p6Card2 != null)
        {
            cardTweenPositions[10].duration = 0.2f;
            HodP4C2();
            GameManagerScript.instance.cards[11].spriteName = Players.instance.p6Card2;
        }
        if (Players.instance.p6Card3 != "0" && Players.instance.p6Card3 != null)
        {
            cardTweenPositions[11].duration = 0.2f;
            HodP4C3();
            GameManagerScript.instance.cards[12].spriteName = Players.instance.p6Card3;
        }
    }
    public void HodAnimationPid4()
    {
        if (Players.instance.p1Card1 != "0" && Players.instance.p2Card1 != null)
        {
            cardTweenPositions[9].duration = 0.2f;
            HodP4C1();
            GameManagerScript.instance.cards[10].spriteName = Players.instance.p1Card1;
        }
        if (Players.instance.p1Card2 != "0" && Players.instance.p2Card2 != null)
        {
            cardTweenPositions[10].duration = 0.2f;
            HodP4C2();
            GameManagerScript.instance.cards[11].spriteName = Players.instance.p1Card2;
        }
        if (Players.instance.p1Card3 != "0" && Players.instance.p2Card3 != null)
        {
            cardTweenPositions[11].duration = 0.2f;
            HodP4C3();
            GameManagerScript.instance.cards[12].spriteName = Players.instance.p1Card3;
        }
        if (Players.instance.p3Card1 != "0" && Players.instance.p3Card1 != null)
        {
            cardTweenPositions[15].duration = 0.2f;
            HodP6C1();
            GameManagerScript.instance.cards[16].spriteName = Players.instance.p3Card1;
        }
        if (Players.instance.p3Card2 != "0" && Players.instance.p3Card2 != null)
        {
            cardTweenPositions[16].duration = 0.2f;
            HodP6C2();
            GameManagerScript.instance.cards[17].spriteName = Players.instance.p3Card2;
        }
        if (Players.instance.p3Card3 != "0" && Players.instance.p3Card3 != null)
        {
            cardTweenPositions[17].duration = 0.2f;
            HodP6C3();
            GameManagerScript.instance.cards[18].spriteName = Players.instance.p3Card3;
        }
        if (Players.instance.p2Card1 != "0" && Players.instance.p4Card1 != null)
        {
            cardTweenPositions[12].duration = 0.2f;
            HodP5C1();
            GameManagerScript.instance.cards[13].spriteName = Players.instance.p2Card1;
        }
        if (Players.instance.p2Card2 != "0" && Players.instance.p4Card2 != null)
        {
            cardTweenPositions[13].duration = 0.2f;
            HodP5C2();
            GameManagerScript.instance.cards[14].spriteName = Players.instance.p2Card2;
        }
        if (Players.instance.p2Card3 != "0" && Players.instance.p4Card3 != null)
        {
            cardTweenPositions[14].duration = 0.2f;
            HodP5C3();
            GameManagerScript.instance.cards[15].spriteName = Players.instance.p2Card3;
        }
        if (Players.instance.p5Card1 != "0" && Players.instance.p5Card1 != null)
        {
            cardTweenPositions[3].duration = 0.2f;
            HodP2C1();
            GameManagerScript.instance.cards[4].spriteName = Players.instance.p5Card1;
        }
        if (Players.instance.p5Card2 != "0" && Players.instance.p5Card2 != null)
        {
            cardTweenPositions[4].duration = 0.2f;
            HodP2C2();
            GameManagerScript.instance.cards[5].spriteName = Players.instance.p5Card2;
        }
        if (Players.instance.p5Card3 != "0" && Players.instance.p5Card3 != null)
        {
            cardTweenPositions[5].duration = 0.2f;
            HodP2C3();
            GameManagerScript.instance.cards[6].spriteName = Players.instance.p5Card3;
        }
        if (Players.instance.p6Card1 != "0" && Players.instance.p6Card1 != null)
        {
            cardTweenPositions[6].duration = 0.2f;
            HodP3C1();
            GameManagerScript.instance.cards[7].spriteName = Players.instance.p6Card1;
        }
        if (Players.instance.p6Card2 != "0" && Players.instance.p6Card2 != null)
        {
            cardTweenPositions[7].duration = 0.2f;
            HodP3C2();
            GameManagerScript.instance.cards[8].spriteName = Players.instance.p6Card2;
        }
        if (Players.instance.p6Card3 != "0" && Players.instance.p6Card3 != null)
        {
            cardTweenPositions[8].duration = 0.2f;
            HodP3C3();
            GameManagerScript.instance.cards[9].spriteName = Players.instance.p6Card3;
        }
    }
    public void HodAnimationPid5()
    {
        if (Players.instance.p1Card1 != "0" && Players.instance.p2Card1 != null)
        {
            cardTweenPositions[6].duration = 0.2f;
            HodP3C1();
            GameManagerScript.instance.cards[7].spriteName = Players.instance.p1Card1;
        }
        if (Players.instance.p1Card2 != "0" && Players.instance.p2Card2 != null)
        {
            cardTweenPositions[7].duration = 0.2f;
            HodP3C2();
            GameManagerScript.instance.cards[8].spriteName = Players.instance.p1Card2;
        }
        if (Players.instance.p1Card3 != "0" && Players.instance.p2Card3 != null)
        {
            cardTweenPositions[8].duration = 0.2f;
            HodP3C3();
            GameManagerScript.instance.cards[9].spriteName = Players.instance.p1Card3;
        }

        if (Players.instance.p2Card1 != "0" && Players.instance.p3Card1 != null)
        {
            cardTweenPositions[9].duration = 0.2f;
            HodP4C1();
            GameManagerScript.instance.cards[10].spriteName = Players.instance.p2Card1;
        }
        if (Players.instance.p2Card2 != "0" && Players.instance.p3Card2 != null)
        {
            cardTweenPositions[10].duration = 0.2f;
            HodP4C2();
            GameManagerScript.instance.cards[11].spriteName = Players.instance.p2Card2;
        }
        if (Players.instance.p2Card3 != "0" && Players.instance.p3Card3 != null)
        {
            cardTweenPositions[11].duration = 0.2f;
            HodP4C3();
            GameManagerScript.instance.cards[12].spriteName = Players.instance.p2Card3;
        }
        if (Players.instance.p3Card1 != "0" && Players.instance.p4Card1 != null)
        {
            cardTweenPositions[12].duration = 0.2f;
            HodP5C1();
            GameManagerScript.instance.cards[13].spriteName = Players.instance.p3Card1;
        }
        if (Players.instance.p3Card2 != "0" && Players.instance.p4Card2 != null)
        {
            cardTweenPositions[13].duration = 0.2f;
            HodP5C2();
            GameManagerScript.instance.cards[14].spriteName = Players.instance.p3Card2;
        }
        if (Players.instance.p3Card3 != "0" && Players.instance.p4Card3 != null)
        {
            cardTweenPositions[14].duration = 0.2f;
            HodP5C3();
            GameManagerScript.instance.cards[15].spriteName = Players.instance.p3Card3;
        }
        if (Players.instance.p4Card1 != "0" && Players.instance.p5Card1 != null)
        {
            cardTweenPositions[15].duration = 0.2f;
            HodP6C1();
            GameManagerScript.instance.cards[16].spriteName = Players.instance.p4Card1;
        }
        if (Players.instance.p4Card2 != "0" && Players.instance.p5Card2 != null)
        {
            cardTweenPositions[16].duration = 0.2f;
            HodP6C2();
            GameManagerScript.instance.cards[17].spriteName = Players.instance.p4Card2;
        }
        if (Players.instance.p4Card3 != "0" && Players.instance.p5Card3 != null)
        {
            cardTweenPositions[17].duration = 0.2f;
            HodP6C3();
            GameManagerScript.instance.cards[18].spriteName = Players.instance.p4Card3;
        }
        if (Players.instance.p6Card1 != "0" && Players.instance.p6Card1 != null)
        {
            cardTweenPositions[3].duration = 0.2f;
            HodP2C1();
            GameManagerScript.instance.cards[4].spriteName = Players.instance.p6Card1;
        }
        if (Players.instance.p6Card2 != "0" && Players.instance.p6Card2 != null)
        {
            cardTweenPositions[4].duration = 0.2f;
            HodP2C2();
            GameManagerScript.instance.cards[5].spriteName = Players.instance.p6Card2;
        }
        if (Players.instance.p6Card3 != "0" && Players.instance.p6Card3 != null)
        {
            cardTweenPositions[5].duration = 0.2f;
            HodP2C3();
            GameManagerScript.instance.cards[6].spriteName = Players.instance.p6Card3;
        }
    }
    public void HodAnimationPid6()
    {
        if (Players.instance.p1Card1 != "0" && Players.instance.p2Card1 != null)
        {
            cardTweenPositions[3].duration = 0.2f;
            HodP2C1();
            GameManagerScript.instance.cards[4].spriteName = Players.instance.p1Card1;
        }
        if (Players.instance.p1Card2 != "0" && Players.instance.p2Card2 != null)
        {
            cardTweenPositions[4].duration = 0.2f;
            HodP2C2();
            GameManagerScript.instance.cards[5].spriteName = Players.instance.p1Card2;
        }
        if (Players.instance.p1Card3 != "0" && Players.instance.p2Card3 != null)
        {
            cardTweenPositions[5].duration = 0.2f;
            HodP2C3();
            GameManagerScript.instance.cards[6].spriteName = Players.instance.p1Card3;
        }
        if (Players.instance.p2Card1 != "0" && Players.instance.p3Card1 != null)
        {
            cardTweenPositions[6].duration = 0.2f;
            HodP3C1();
            GameManagerScript.instance.cards[7].spriteName = Players.instance.p2Card1;
        }
        if (Players.instance.p2Card2 != "0" && Players.instance.p3Card2 != null)
        {
            cardTweenPositions[7].duration = 0.2f;
            HodP3C2();
            GameManagerScript.instance.cards[8].spriteName = Players.instance.p2Card2;
        }
        if (Players.instance.p2Card3 != "0" && Players.instance.p3Card3 != null)
        {
            cardTweenPositions[8].duration = 0.2f;
            HodP3C3();
            GameManagerScript.instance.cards[9].spriteName = Players.instance.p2Card3;
        }
        if (Players.instance.p3Card1 != "0" && Players.instance.p4Card1 != null)
        {
            cardTweenPositions[9].duration = 0.2f;
            HodP4C1();
            GameManagerScript.instance.cards[10].spriteName = Players.instance.p3Card1;
        }
        if (Players.instance.p3Card2 != "0" && Players.instance.p4Card2 != null)
        {
            cardTweenPositions[10].duration = 0.2f;
            HodP4C2();
            GameManagerScript.instance.cards[11].spriteName = Players.instance.p3Card2;
        }
        if (Players.instance.p3Card3 != "0" && Players.instance.p4Card3 != null)
        {
            cardTweenPositions[11].duration = 0.2f;
            HodP4C3();
            GameManagerScript.instance.cards[12].spriteName = Players.instance.p3Card3;
        }
        if (Players.instance.p4Card1 != "0" && Players.instance.p5Card1 != null)
        {
            cardTweenPositions[12].duration = 0.2f;
            HodP5C1();
            GameManagerScript.instance.cards[13].spriteName = Players.instance.p4Card1;
        }
        if (Players.instance.p4Card2 != "0" && Players.instance.p5Card2 != null)
        {
            cardTweenPositions[13].duration = 0.2f;
            HodP5C2();
            GameManagerScript.instance.cards[14].spriteName = Players.instance.p4Card2;
        }
        if (Players.instance.p4Card3 != "0" && Players.instance.p5Card3 != null)
        {
            cardTweenPositions[14].duration = 0.2f;
            HodP5C3();
            GameManagerScript.instance.cards[15].spriteName = Players.instance.p4Card3;
        }
        if (Players.instance.p5Card1 != "0" && Players.instance.p6Card1 != null)
        {
            cardTweenPositions[15].duration = 0.2f;
            HodP6C1();
            GameManagerScript.instance.cards[16].spriteName = Players.instance.p5Card1;
        }
        if (Players.instance.p5Card2 != "0" && Players.instance.p6Card2 != null)
        {
            cardTweenPositions[16].duration = 0.2f;
            HodP6C2();
            GameManagerScript.instance.cards[17].spriteName = Players.instance.p5Card2;
        }
        if (Players.instance.p5Card3 != "0" && Players.instance.p6Card3 != null)
        {
            cardTweenPositions[17].duration = 0.2f;
            HodP6C3();
            GameManagerScript.instance.cards[18].spriteName = Players.instance.p5Card3;
        }
        
    }
    public void VzyatkaAnimation()
    {
        if (Table.instance.place_id == 1)
        {
            if (Players.instance.round_winner == 1)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP1.transform);
                }
            }
            if (Players.instance.round_winner == 2)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP2.transform);
                }
            }
            if (Players.instance.round_winner == 3)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP3.transform);
                }
            }
            if (Players.instance.round_winner == 4)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP4.transform);
                }
            }
            if (Players.instance.round_winner == 5)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP5.transform);
                }
            }
            if (Players.instance.round_winner == 6)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP6.transform);
                }
            }
        }
        if (Table.instance.place_id == 2)
        {
            if (Players.instance.round_winner == 1)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP6.transform);
                }
            }
            if (Players.instance.round_winner == 2)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP1.transform);
                }
            }
            if (Players.instance.round_winner == 3)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP2.transform);
                }
            }
            if (Players.instance.round_winner == 4)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP3.transform);
                }
            }
            if (Players.instance.round_winner == 5)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP4.transform);
                }
            }
            if (Players.instance.round_winner == 6)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP5.transform);
                }
            }
        }
        if (Table.instance.place_id == 3)
        {
            if (Players.instance.round_winner == 1)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP5.transform);
                }
            }
            if (Players.instance.round_winner == 2)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP6.transform);
                }
            }
            if (Players.instance.round_winner == 3)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP1.transform);
                }
            }
            if (Players.instance.round_winner == 4)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP2.transform);
                }
            }
            if (Players.instance.round_winner == 5)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP3.transform);
                }
            }
            if (Players.instance.round_winner == 6)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP4.transform);
                }
            }
        }
        if (Table.instance.place_id == 4)
        {
            if (Players.instance.round_winner == 1)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP4.transform);
                }
            }
            if (Players.instance.round_winner == 2)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP5.transform);
                }
            }
            if (Players.instance.round_winner == 3)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP6.transform);
                }
            }
            if (Players.instance.round_winner == 4)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP1.transform);
                }
            }
            if (Players.instance.round_winner == 5)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP2.transform);
                }
            }
            if (Players.instance.round_winner == 6)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP3.transform);
                }
            }
        }
        if (Table.instance.place_id == 5)
        {
            if (Players.instance.round_winner == 1)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP3.transform);
                }
            }
            if (Players.instance.round_winner == 2)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP4.transform);
                }
            }
            if (Players.instance.round_winner == 3)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP5.transform);
                }
            }
            if (Players.instance.round_winner == 4)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP6.transform);
                }
            }
            if (Players.instance.round_winner == 5)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP1.transform);
                }
            }
            if (Players.instance.round_winner == 6)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP2.transform);
                }
            }
        }
        if (Table.instance.place_id == 6)
        {
            if (Players.instance.round_winner == 1)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP2.transform);
                }
            }
            if (Players.instance.round_winner == 2)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP3.transform);
                }
            }
            if (Players.instance.round_winner == 3)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP4.transform);
                }
            }
            if (Players.instance.round_winner == 4)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP5.transform);
                }
            }
            if (Players.instance.round_winner == 5)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP6.transform);
                }
            }
            if (Players.instance.round_winner == 6)
            {
                for (int i = 1; i < cardsAreFixedList.Count; i++)
                {
                    if (cardsAreFixedList[i] && !cardsVoVzyatke[i])
                        GameManagerScript.instance.cards[i].transform.SetParent(gridP1.transform);
                }
            }
        }
        gridP1.repositionNow = true;
        gridP2.repositionNow = true;
        gridP3.repositionNow = true;
        gridP4.repositionNow = true;
        gridP5.repositionNow = true;
        gridP6.repositionNow = true;

        if (Table.instance.place_id == Players.instance.next_user)
        {
            GameManagerScript.instance.GetPossibleCards();
            print("vizvano iz vzyatki");
        }
        Players.instance.round_winner = 0;
        vkluchitTimer = true;
        
    }
    public void RazdatFishki()
    {
        chipP1.duration = 0.2f;
        chipP2.duration = 0.2f;
        chipP3.duration = 0.2f;
        chipP4.duration = 0.2f;
        chipP5.duration = 0.2f;
        chipP6.duration = 0.2f;

        chipP1.PlayForward();
        chipP2.PlayForward();
        chipP3.PlayForward();
        chipP4.PlayForward();
        chipP5.PlayForward();
        chipP6.PlayForward();

        chipP1LBL.text = "";
        chipP2LBL.text = "";
        chipP3LBL.text = "";
        chipP4LBL.text = "";
        chipP5LBL.text = "";
        chipP6LBL.text = "";
    }
    public void CheckChips()
    {
        if (Table.instance.place_id == 1)   
        {
            if (Players.instance.p1IsPlay == 1)
            {
                chipP1GO.SetActive(true);
                chipP1LBLGO.SetActive(true);
                p1GO.SetActive(true);
                p1Name.text = Players.instance.p1Name;
                p1Bank.text = Players.instance.p1Balance.ToString();

            }
            else
            {
                chipP1GO.SetActive(false);
                chipP1LBLGO.SetActive(false);p1GO.SetActive(false);
            }
            if (Players.instance.p2IsPlay == 1)
            {
                chipP2GO.SetActive(true);
                chipP2LBLGO.SetActive(true);
                p2GO.SetActive(true);
                p2Name.text = Players.instance.p2Name;
                p2Bank.text = Players.instance.p2Balance.ToString();
            }
            else
            {
                chipP2GO.SetActive(false);
                chipP2LBLGO.SetActive(false);p2GO.SetActive(false);
            }
            if (Players.instance.p3IsPlay == 1)
            {
                chipP3GO.SetActive(true);
                chipP3LBLGO.SetActive(true);
                p3GO.SetActive(true);
                p3Name.text = Players.instance.p3Name;
                p3Bank.text = Players.instance.p3Balance.ToString();
            }
            else
            {
                chipP3GO.SetActive(false);
                chipP3LBLGO.SetActive(false);p3GO.SetActive(false);

            }
            if (Players.instance.p4IsPlay == 1)
            {
                chipP4GO.SetActive(true);
                chipP4LBLGO.SetActive(true);
                p4GO.SetActive(true);
                p4Name.text = Players.instance.p4Name;
                p4Bank.text = Players.instance.p4Balance.ToString();
            }
            else
            {
                chipP4GO.SetActive(false);
                chipP4LBLGO.SetActive(false);p4GO.SetActive(false);

            }
            if (Players.instance.p5IsPlay == 1)
            {
                chipP5GO.SetActive(true);
                chipP5LBLGO.SetActive(true);
                p5GO.SetActive(true);
                p5Name.text = Players.instance.p5Name;
                p5Bank.text = Players.instance.p5Balance.ToString();
            }
            else
            {
                chipP5GO.SetActive(false);
                chipP5LBLGO.SetActive(false);p5GO.SetActive(false);
            }
            if (Players.instance.p6IsPlay == 1)
            {
                chipP6GO.SetActive(true);
                chipP6LBLGO.SetActive(true);
                p6GO.SetActive(true);
                p6Name.text = Players.instance.p6Name;
                p6Bank.text = Players.instance.p6Balance.ToString();
            }
            else
            {
                chipP6GO.SetActive(false);
                chipP6LBLGO.SetActive(false);p6GO.SetActive(false);
            }
        }

        if (Table.instance.place_id == 2)
        {
            if (Players.instance.p1IsPlay == 1)
            {
                chipP6GO.SetActive(true);
                chipP6LBLGO.SetActive(true);
                p6GO.SetActive(true);
                p6Name.text = Players.instance.p1Name;
                p6Bank.text = Players.instance.p1Balance.ToString();
            }
            else
            {
                chipP6GO.SetActive(false);
                chipP6LBLGO.SetActive(false);p6GO.SetActive(false);
            }
            if (Players.instance.p2IsPlay == 1)
            {
                chipP1GO.SetActive(true);
                chipP1LBLGO.SetActive(true);
                p1GO.SetActive(true);
                p1Name.text = Players.instance.p2Name;
                p1Bank.text = Players.instance.p2Balance.ToString();
            }
            else
            {
                chipP1GO.SetActive(false);
                chipP1LBLGO.SetActive(false);p1GO.SetActive(false);
            }
            if (Players.instance.p3IsPlay == 1)
            {
                chipP2GO.SetActive(true);chipP2LBLGO.SetActive(true);
                p2GO.SetActive(true);
                p2Name.text = Players.instance.p3Name;
                p2Bank.text = Players.instance.p3Balance.ToString();
            }
            else
            {
                chipP2GO.SetActive(false);chipP2LBLGO.SetActive(false);p2GO.SetActive(false);
            }
            if (Players.instance.p4IsPlay == 1)
            {
                chipP3GO.SetActive(true);chipP3LBLGO.SetActive(true);
                p3GO.SetActive(true);
                p3Name.text = Players.instance.p4Name;
                p3Bank.text = Players.instance.p4Balance.ToString();
            }
            else
            {
                chipP3GO.SetActive(false);chipP3LBLGO.SetActive(false);p3GO.SetActive(false);
            }
            if (Players.instance.p5IsPlay == 1)
            {
                chipP4GO.SetActive(true);chipP4LBLGO.SetActive(true);
                p4GO.SetActive(true);
                p4Name.text = Players.instance.p5Name;
                p4Bank.text = Players.instance.p5Balance.ToString();
            }
            else
            {
                chipP4GO.SetActive(false);chipP4LBLGO.SetActive(false);p4GO.SetActive(false);
            }
            if (Players.instance.p6IsPlay == 1)
            {
                chipP5GO.SetActive(true);chipP5LBLGO.SetActive(true);
                p5GO.SetActive(true);
                p5Name.text = Players.instance.p6Name;
                p5Bank.text = Players.instance.p6Balance.ToString();
            }
            else
            {
                chipP5GO.SetActive(false);chipP5LBLGO.SetActive(false);p5GO.SetActive(false);
            }
        }

        if (Table.instance.place_id == 3)
        {
            if (Players.instance.p1IsPlay == 1)
            {
                chipP5GO.SetActive(true);
                chipP5LBLGO.SetActive(true);
                p5GO.SetActive(true);
                p5Name.text = Players.instance.p1Name;
                p5Bank.text = Players.instance.p1Balance.ToString();
            }
            else
            {
                chipP5GO.SetActive(false);chipP5LBLGO.SetActive(false);p5GO.SetActive(false);
            }
            if (Players.instance.p2IsPlay == 1)
            {
                chipP6GO.SetActive(true);chipP6LBLGO.SetActive(true);
                p6GO.SetActive(true);
                p6Name.text = Players.instance.p2Name;
                p6Bank.text = Players.instance.p2Balance.ToString();
            }
            else
            {
                chipP6GO.SetActive(false);chipP6LBLGO.SetActive(false);p6GO.SetActive(false);
            }
            if (Players.instance.p3IsPlay == 1)
            {
                chipP1GO.SetActive(true);chipP1LBLGO.SetActive(true);
                p1GO.SetActive(true);
                p1Name.text = Players.instance.p3Name;
                p1Bank.text = Players.instance.p3Balance.ToString();
            }
            else
            {
                chipP1GO.SetActive(false);chipP1LBLGO.SetActive(false);p1GO.SetActive(false);
            }
            if (Players.instance.p4IsPlay == 1)
            {
                chipP2GO.SetActive(true);chipP2LBLGO.SetActive(true);
                p2GO.SetActive(true);
                p2Name.text = Players.instance.p4Name;
                p2Bank.text = Players.instance.p4Balance.ToString();
            }
            else
            {
                chipP2GO.SetActive(false);chipP2LBLGO.SetActive(false);p2GO.SetActive(false);
            }
            if (Players.instance.p5IsPlay == 1)
            {
                chipP3GO.SetActive(true);chipP3LBLGO.SetActive(true);
                p3GO.SetActive(true);
                p3Name.text = Players.instance.p5Name;
                p3Bank.text = Players.instance.p5Balance.ToString();
            }
            else
            {
                chipP3GO.SetActive(false);chipP3LBLGO.SetActive(false);p3GO.SetActive(false);
            }
            if (Players.instance.p6IsPlay == 1)
            {
                chipP4GO.SetActive(true);chipP4LBLGO.SetActive(true);
                p4GO.SetActive(true);
                p4Name.text = Players.instance.p6Name;
                p4Bank.text = Players.instance.p6Balance.ToString();
            }
            else
            {
                chipP4GO.SetActive(false);chipP4LBLGO.SetActive(false);p4GO.SetActive(false);
            }
        }

        if (Table.instance.place_id == 4)
        {
            if (Players.instance.p1IsPlay == 1)
            {
                chipP4GO.SetActive(true);chipP4LBLGO.SetActive(true);
                p4GO.SetActive(true);
                p4Name.text = Players.instance.p1Name;
                p4Bank.text = Players.instance.p1Balance.ToString();
            }
            else
            {
                chipP4GO.SetActive(false);chipP4LBLGO.SetActive(false);p4GO.SetActive(false);
            }
            if (Players.instance.p2IsPlay == 1)
            {
                chipP5GO.SetActive(true);chipP5LBLGO.SetActive(true);
                p5GO.SetActive(true);
                p5Name.text = Players.instance.p2Name;
                p5Bank.text = Players.instance.p2Balance.ToString();
            }
            else
            {
                chipP5GO.SetActive(false);chipP5LBLGO.SetActive(false);p5GO.SetActive(false);
            }
            if (Players.instance.p3IsPlay == 1)
            {
                chipP6GO.SetActive(true);chipP6LBLGO.SetActive(true);
                p6GO.SetActive(true);
                p6Name.text = Players.instance.p3Name;
                p6Bank.text = Players.instance.p3Balance.ToString();
            }
            else
            {
                chipP6GO.SetActive(false);chipP6LBLGO.SetActive(false);p6GO.SetActive(false);
            }
            if (Players.instance.p4IsPlay == 1)
            {
                chipP1GO.SetActive(true);chipP1LBLGO.SetActive(true);
                p1GO.SetActive(true);
                p1Name.text = Players.instance.p4Name;
                p1Bank.text = Players.instance.p4Balance.ToString();
            }
            else
            {
                chipP1GO.SetActive(false);chipP1LBLGO.SetActive(false);p1GO.SetActive(false);
            }
            if (Players.instance.p5IsPlay == 1)
            {
                chipP2GO.SetActive(true);chipP2LBLGO.SetActive(true);
                p2GO.SetActive(true);
                p2Name.text = Players.instance.p5Name;
                p2Bank.text = Players.instance.p5Balance.ToString();
            }
            else
            {
                chipP2GO.SetActive(false);chipP2LBLGO.SetActive(false);p2GO.SetActive(false);
            }
            if (Players.instance.p6IsPlay == 1)
            {
                chipP3GO.SetActive(true);chipP3LBLGO.SetActive(true);
                p3GO.SetActive(true);
                p3Name.text = Players.instance.p6Name;
                p3Bank.text = Players.instance.p6Balance.ToString();
            }
            else
            {
                chipP3GO.SetActive(false);chipP3LBLGO.SetActive(false);p3GO.SetActive(false);
            }
        }

        if (Table.instance.place_id == 5)
        {
            if (Players.instance.p1IsPlay == 1)
            {
                chipP3GO.SetActive(true);chipP3LBLGO.SetActive(true);
                p3GO.SetActive(true);
                p3Name.text = Players.instance.p1Name;
                p3Bank.text = Players.instance.p1Balance.ToString();
            }
            else
            {
                chipP3GO.SetActive(false);chipP3LBLGO.SetActive(false);p3GO.SetActive(false);
            }
            if (Players.instance.p2IsPlay == 1)
            {
                chipP4GO.SetActive(true);chipP4LBLGO.SetActive(true);
                p4GO.SetActive(true);
                p4Name.text = Players.instance.p2Name;
                p4Bank.text = Players.instance.p2Balance.ToString();
            }
            else
            {
                chipP4GO.SetActive(false);chipP4LBLGO.SetActive(false);p4GO.SetActive(false);
            }
            if (Players.instance.p3IsPlay == 1)
            {
                chipP5GO.SetActive(true);chipP5LBLGO.SetActive(true);
                p5GO.SetActive(true);
                p5Name.text = Players.instance.p3Name;
                p5Bank.text = Players.instance.p3Balance.ToString();
            }
            else
            {
                chipP5GO.SetActive(false);chipP5LBLGO.SetActive(false);p5GO.SetActive(false);
            }
            if (Players.instance.p4IsPlay == 1)
            {
                chipP6GO.SetActive(true);chipP6LBLGO.SetActive(true);
                p6GO.SetActive(true);
                p6Name.text = Players.instance.p4Name;
                p6Bank.text = Players.instance.p4Balance.ToString();
            }
            else
            {
                chipP6GO.SetActive(false);chipP6LBLGO.SetActive(false);p6GO.SetActive(false);
            }
            if (Players.instance.p5IsPlay == 1)
            {
                chipP1GO.SetActive(true);chipP1LBLGO.SetActive(true);
                p1GO.SetActive(true);
                p1Name.text = Players.instance.p5Name;
                p1Bank.text = Players.instance.p5Balance.ToString();
            }
            else
            {
                chipP1GO.SetActive(false);chipP1LBLGO.SetActive(false);p1GO.SetActive(false);
            }
            if (Players.instance.p6IsPlay == 1)
            {
                chipP2GO.SetActive(true);chipP2LBLGO.SetActive(true);
                p2GO.SetActive(true);
                p2Name.text = Players.instance.p6Name;
                p2Bank.text = Players.instance.p6Balance.ToString();
            }
            else
            {
                chipP2GO.SetActive(false);chipP2LBLGO.SetActive(false);p2GO.SetActive(false);
            }
        }

        if (Table.instance.place_id == 6)
        {
            if (Players.instance.p1IsPlay == 1)
            {
                chipP2GO.SetActive(true);chipP2LBLGO.SetActive(true);
                p2GO.SetActive(true);
                p2Name.text = Players.instance.p1Name;
                p2Bank.text = Players.instance.p1Balance.ToString();
            }
            else
            {
                chipP2GO.SetActive(false);chipP2LBLGO.SetActive(false);p2GO.SetActive(false);
            }
            if (Players.instance.p2IsPlay == 1)
            {
                chipP3GO.SetActive(true);chipP3LBLGO.SetActive(true);
                p3GO.SetActive(true);
                p3Name.text = Players.instance.p2Name;
                p3Bank.text = Players.instance.p2Balance.ToString();
            }
            else
            {
                chipP3GO.SetActive(false);chipP3LBLGO.SetActive(false);p3GO.SetActive(false);
            }
            if (Players.instance.p3IsPlay == 1)
            {
                chipP4GO.SetActive(true);chipP4LBLGO.SetActive(true);
                p4GO.SetActive(true);
                p4Name.text = Players.instance.p3Name;
                p4Bank.text = Players.instance.p3Balance.ToString();
            }
            else
            {
                chipP4GO.SetActive(false);chipP4LBLGO.SetActive(false);p4GO.SetActive(false);
            }
            if (Players.instance.p4IsPlay == 1)
            {
                chipP5GO.SetActive(true);chipP5LBLGO.SetActive(true);
                p5GO.SetActive(true);
                p5Name.text = Players.instance.p4Name;
                p5Bank.text = Players.instance.p4Balance.ToString();
            }
            else
            {
                chipP5GO.SetActive(false);chipP5LBLGO.SetActive(false);p5GO.SetActive(false);
            }
            if (Players.instance.p5IsPlay == 1)
            {
                chipP6GO.SetActive(true);chipP6LBLGO.SetActive(true);
                p6GO.SetActive(true);
                p6Name.text = Players.instance.p5Name;
                p6Bank.text = Players.instance.p5Balance.ToString();
            }
            else
            {
                chipP6GO.SetActive(false);chipP6LBLGO.SetActive(false);p6GO.SetActive(false);
            }
            if (Players.instance.p6IsPlay == 1)
            {
                chipP1GO.SetActive(true);chipP1LBLGO.SetActive(true);
                p1GO.SetActive(true);
                p1Name.text = Players.instance.p6Name;
                p1Bank.text = Players.instance.p6Balance.ToString();
            }
            else
            {
                chipP1GO.SetActive(false);chipP1LBLGO.SetActive(false);p1GO.SetActive(false);
            }
        }
    }
    public void CheckBets()
    {
        if (Table.instance.place_id == 1)
        {
            if (Players.instance.p1Bet > 0)
            {
                chipP1.PlayReverse();
                chipP1LBL.text = Players.instance.p1Bet.ToString();
            }
            if (Players.instance.p2Bet > 0)
            {
                chipP2.PlayReverse();
                chipP2LBL.text = Players.instance.p2Bet.ToString();
            }
            if (Players.instance.p3Bet > 0)
            {
                chipP3.PlayReverse();
                chipP3LBL.text = Players.instance.p3Bet.ToString();
            }
            if (Players.instance.p4Bet > 0)
            {
                chipP4.PlayReverse();
                chipP4LBL.text = Players.instance.p4Bet.ToString();
            }
            if (Players.instance.p5Bet > 0)
            {
                chipP5.PlayReverse();
                chipP5LBL.text = Players.instance.p5Bet.ToString();
            }
            if (Players.instance.p6Bet > 0)
            {
                chipP6.PlayReverse();
                chipP6LBL.text = Players.instance.p6Bet.ToString();
            }
        }
        if (Table.instance.place_id == 2)
        {
            if (Players.instance.p2Bet > 0)
            {
                chipP1.PlayReverse();
                chipP1LBL.text = Players.instance.p2Bet.ToString();
            }
            if (Players.instance.p3Bet > 0)
            {
                chipP2.PlayReverse();
                chipP2LBL.text = Players.instance.p3Bet.ToString();
            }
            if (Players.instance.p4Bet > 0)
            {
                chipP3.PlayReverse();
                chipP3LBL.text = Players.instance.p4Bet.ToString();
            }
            if (Players.instance.p5Bet > 0)
            {
                chipP4.PlayReverse();
                chipP4LBL.text = Players.instance.p5Bet.ToString();
            }
            if (Players.instance.p6Bet > 0)
            {
                chipP5.PlayReverse();
                chipP5LBL.text = Players.instance.p6Bet.ToString();
            }
            if (Players.instance.p1Bet > 0)
            {
                chipP6.PlayReverse();
                chipP6LBL.text = Players.instance.p1Bet.ToString();
            }
        }
        if (Table.instance.place_id == 3)
        {
            if (Players.instance.p3Bet > 0)
            {
                chipP1.PlayReverse();
                chipP1LBL.text = Players.instance.p3Bet.ToString();
            }
            if (Players.instance.p4Bet > 0)
            {
                chipP2.PlayReverse();
                chipP2LBL.text = Players.instance.p4Bet.ToString();
            }
            if (Players.instance.p5Bet > 0)
            {
                chipP3.PlayReverse();
                chipP3LBL.text = Players.instance.p5Bet.ToString();
            }
            if (Players.instance.p6Bet > 0)
            {
                chipP4.PlayReverse();
                chipP4LBL.text = Players.instance.p6Bet.ToString();
            }
            if (Players.instance.p1Bet > 0)
            {
                chipP5.PlayReverse();
                chipP5LBL.text = Players.instance.p1Bet.ToString();
            }
            if (Players.instance.p2Bet > 0)
            {
                chipP6.PlayReverse();
                chipP6LBL.text = Players.instance.p2Bet.ToString();
            }
        }
        if (Table.instance.place_id == 4)
        {
            if (Players.instance.p4Bet > 0)
            {
                chipP1.PlayReverse();
                chipP1LBL.text = Players.instance.p4Bet.ToString();
            }
            if (Players.instance.p5Bet > 0)
            {
                chipP2.PlayReverse();
                chipP2LBL.text = Players.instance.p5Bet.ToString();
            }
            if (Players.instance.p6Bet > 0)
            {
                chipP3.PlayReverse();
                chipP3LBL.text = Players.instance.p6Bet.ToString();
            }
            if (Players.instance.p1Bet > 0)
            {
                chipP4.PlayReverse();
                chipP4LBL.text = Players.instance.p1Bet.ToString();
            }
            if (Players.instance.p2Bet > 0)
            {
                chipP5.PlayReverse();
                chipP5LBL.text = Players.instance.p2Bet.ToString();
            }
            if (Players.instance.p3Bet > 0)
            {
                chipP6.PlayReverse();
                chipP6LBL.text = Players.instance.p3Bet.ToString();
            }
        }
        if (Table.instance.place_id == 5)
        {
            if (Players.instance.p5Bet > 0)
            {
                chipP1.PlayReverse();
                chipP1LBL.text = Players.instance.p5Bet.ToString();
            }
            if (Players.instance.p6Bet > 0)
            {
                chipP2.PlayReverse();
                chipP2LBL.text = Players.instance.p6Bet.ToString();
            }
            if (Players.instance.p1Bet > 0)
            {
                chipP3.PlayReverse();
                chipP3LBL.text = Players.instance.p1Bet.ToString();
            }
            if (Players.instance.p2Bet > 0)
            {
                chipP4.PlayReverse();
                chipP4LBL.text = Players.instance.p2Bet.ToString();
            }
            if (Players.instance.p3Bet > 0)
            {
                chipP5.PlayReverse();
                chipP5LBL.text = Players.instance.p3Bet.ToString();
            }
            if (Players.instance.p4Bet > 0)
            {
                chipP6.PlayReverse();
                chipP6LBL.text = Players.instance.p4Bet.ToString();
            }
        }
        if (Table.instance.place_id == 6)
        {
            if (Players.instance.p6Bet > 0)
            {
                chipP1.PlayReverse();
                chipP1LBL.text = Players.instance.p6Bet.ToString();
            }
            if (Players.instance.p1Bet > 0)
            {
                chipP2.PlayReverse();
                chipP2LBL.text = Players.instance.p1Bet.ToString();
            }
            if (Players.instance.p2Bet > 0)
            {
                chipP3.PlayReverse();
                chipP3LBL.text = Players.instance.p2Bet.ToString();
            }
            if (Players.instance.p3Bet > 0)
            {
                chipP4.PlayReverse();
                chipP4LBL.text = Players.instance.p3Bet.ToString();
            }
            if (Players.instance.p4Bet > 0)
            {
                chipP5.PlayReverse();
                chipP5LBL.text = Players.instance.p4Bet.ToString();
            }
            if (Players.instance.p5Bet > 0)
            {
                chipP6.PlayReverse();
                chipP6LBL.text = Players.instance.p5Bet.ToString();
            }
        }

    }
    public void SetChipsTPDurationOnReverse()
    {
        if (Players.instance.p1Bet > 0 || Players.instance.p2Bet > 0 || Players.instance.p3Bet > 0 || Players.instance.p4Bet > 0 || Players.instance.p5Bet > 0 || Players.instance.p6Bet > 0)
        {
            chipP1.duration = 0.01f;
            chipP2.duration = 0.01f;
            chipP3.duration = 0.01f;
            chipP4.duration = 0.01f;
            chipP5.duration = 0.01f;
            chipP6.duration = 0.01f;
        }
    }
    public void Obnulit()
    {
        GameManagerScript.instance.cards[1].transform.SetParent(seat1.transform);
        GameManagerScript.instance.cards[2].transform.SetParent(seat1.transform);
        GameManagerScript.instance.cards[3].transform.SetParent(seat1.transform);
        GameManagerScript.instance.cards[4].transform.SetParent(seat2.transform);
        GameManagerScript.instance.cards[5].transform.SetParent(seat2.transform);
        GameManagerScript.instance.cards[6].transform.SetParent(seat2.transform);
        GameManagerScript.instance.cards[7].transform.SetParent(seat3.transform);
        GameManagerScript.instance.cards[8].transform.SetParent(seat3.transform);
        GameManagerScript.instance.cards[9].transform.SetParent(seat3.transform);
        GameManagerScript.instance.cards[10].transform.SetParent(seat4.transform);
        GameManagerScript.instance.cards[11].transform.SetParent(seat4.transform);
        GameManagerScript.instance.cards[12].transform.SetParent(seat4.transform);
        GameManagerScript.instance.cards[13].transform.SetParent(seat5.transform);
        GameManagerScript.instance.cards[14].transform.SetParent(seat5.transform);
        GameManagerScript.instance.cards[15].transform.SetParent(seat5.transform);
        GameManagerScript.instance.cards[16].transform.SetParent(seat6.transform);
        GameManagerScript.instance.cards[17].transform.SetParent(seat6.transform);
        GameManagerScript.instance.cards[18].transform.SetParent(seat6.transform);

        betScrollBar.value = 0;

        p1c1.to = new Vector3(-27.3f, 258.5f, 0f);
        p1c2.to = new Vector3(-27.3f, 258.5f, 0f);
        p1c3.to = new Vector3(-27.3f, 258.5f, 0f);
        p2c1.to = new Vector3(417.5f, 181.8f, 0f);
        p2c2.to = new Vector3(417.5f, 181.8f, 0f);
        p2c3.to = new Vector3(417.5f, 181.8f, 0f);
        p3c1.to = new Vector3(417.5f, -8.9f, 0f);
        p3c2.to = new Vector3(417.5f, -8.9f, 0f);
        p3c3.to = new Vector3(417.5f, -8.9f, 0f);
        p4c1.to = new Vector3(-27.1f, -112f, 0f);
        p4c2.to = new Vector3(-27.1f, -112f, 0f);
        p4c3.to = new Vector3(-27.1f, -112f, 0f);
        p5c1.to = new Vector3(-500.7f, -9.2f, 0f);
        p5c2.to = new Vector3(-500.7f, -9.2f, 0f);
        p5c3.to = new Vector3(-500.7f, -9.2f, 0f);
        p6c1.to = new Vector3(-500.8f, 181.8f, 0f);
        p6c2.to = new Vector3(-500.8f, 181.8f, 0f);
        p6c3.to = new Vector3(-500.8f, 181.8f, 0f);

        for (int i = 0; i < cardTweenPositions.Count; i++)
        {
            cardTweenPositions[i].SetStartToCurrentValue();
            cardTweenPositions[i].PlayForward();
        }
        for (int i = 1; i < cardsTweenScales.Count; i++)
        {
            cardsTweenScales[i].duration = 0.1f;
            cardsTweenScales[i].SetStartToCurrentValue();
            cardsTweenScales[i].to = new Vector3(0.5f, 0.5f, 1f);
            cardsTweenScales[i].PlayForward();
        }


        chipP1.PlayReverse();
        chipP2.PlayReverse();
        chipP3.PlayReverse();
        chipP4.PlayReverse();
        chipP5.PlayReverse();
        chipP6.PlayReverse();

        ktoHodit = 1;

        vkluchitTimer = true;

        pervayaRazdachaFishek = true;

        nelzyaRazdatKarti = false;

        for (int i = 0; i < cardsAreFixedList.Count; i++)
        {
            cardsAreFixedList[i] = false;
            cardsVoVzyatke[i] = false;
        }

        p1c1IsGiven = false;
        p1c2IsGiven = false;
        p1c3IsGiven = false;
        p2c1IsGiven = false;
        p2c2IsGiven = false;
        p2c3IsGiven = false;
        p3c1IsGiven = false;
        p3c2IsGiven = false;
        p3c3IsGiven = false;
        p4c1IsGiven = false;
        p4c2IsGiven = false;
        p4c3IsGiven = false;
        p5c1IsGiven = false;
        p5c2IsGiven = false;
        p5c3IsGiven = false;
        p6c1IsGiven = false;
        p6c2IsGiven = false;
        p6c3IsGiven = false;

        Players.instance.p1Card1 = "0";
        Players.instance.p1Card2 = "0";
        Players.instance.p1Card3 = "0";
        Players.instance.p2Card1 = "0";
        Players.instance.p2Card2 = "0";
        Players.instance.p2Card3 = "0";
        Players.instance.p3Card1 = "0";
        Players.instance.p3Card2 = "0";
        Players.instance.p3Card3 = "0";
        Players.instance.p4Card1 = "0";
        Players.instance.p4Card2 = "0";
        Players.instance.p4Card3 = "0";
        Players.instance.p5Card1 = "0";
        Players.instance.p5Card2 = "0";
        Players.instance.p5Card3 = "0";
        Players.instance.p6Card1 = "0";
        Players.instance.p6Card2 = "0";
        Players.instance.p6Card3 = "0";

        Players.instance.p1Bet = 0;
        Players.instance.p2Bet = 0;
        Players.instance.p3Bet = 0;
        Players.instance.p4Bet = 0;
        Players.instance.p5Bet = 0;
        Players.instance.p6Bet = 0;

        Players.instance.currentBet = 0;
        Players.instance.pot = 0;

        Table.instance.bet_state = 1;

        for (int i = 0; i < GameManagerScript.instance.cards.Count; i++)
        {
            GameManagerScript.instance.cards[i].spriteName = "0";
        }

        sobratFishki = true;
        GameManagerScript.instance.isMakedBet = false;
        GameManagerScript.instance.knopkaB();
        timerPosleObnuleniya = 3f;
        vizvatMetodGGU = true;

    }
    public void BetLabelsValue()
    {
        if(Table.instance.place_id == 1)
        {
            betLBL.text = ((int)(Players.instance.p1Balance * betScrollBar.value)).ToString();
            betBTNLBL.text = ((int)(Players.instance.p1Balance * betScrollBar.value)).ToString();
            if (betScrollBar.value < (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p1Balance))
            {
                betScrollBar.value = (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p1Balance);
            }
        }
        if (Table.instance.place_id == 2)
        {
            betLBL.text = ((int)(Players.instance.p2Balance * betScrollBar.value)).ToString();
            betBTNLBL.text = ((int)(Players.instance.p2Balance * betScrollBar.value)).ToString();
            if (betScrollBar.value < (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p2Balance))
            {
                betScrollBar.value = (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p2Balance);
            }
        }
        if (Table.instance.place_id == 3)
        {
            betLBL.text = ((int)(Players.instance.p3Balance * betScrollBar.value)).ToString();
            betBTNLBL.text = ((int)(Players.instance.p3Balance * betScrollBar.value)).ToString();
            if (betScrollBar.value < (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p3Balance))
            {
                betScrollBar.value = (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p3Balance);
            }
        }
        if (Table.instance.place_id == 4)
        {
            betLBL.text = ((int)(Players.instance.p4Balance * betScrollBar.value)).ToString();
            betBTNLBL.text = ((int)(Players.instance.p4Balance * betScrollBar.value)).ToString();
            if (betScrollBar.value < (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p4Balance))
            {
                betScrollBar.value = (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p4Balance);
            }
        }
        if (Table.instance.place_id == 5)
        {
            betLBL.text = ((int)(Players.instance.p5Balance * betScrollBar.value)).ToString();
            betBTNLBL.text = ((int)(Players.instance.p5Balance * betScrollBar.value)).ToString();
            if (betScrollBar.value < (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p5Balance))
            {
                betScrollBar.value = (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p5Balance);
            }
        }
        if (Table.instance.place_id == 6)
        {
            betLBL.text = ((int)(Players.instance.p6Balance * betScrollBar.value)).ToString();
            betBTNLBL.text = ((int)(Players.instance.p6Balance * betScrollBar.value)).ToString();
            if (betScrollBar.value < (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p6Balance))
            {
                betScrollBar.value = (float)((((float)Players.instance.currentBet * 2) + 1) / (float)Players.instance.p6Balance);
            }
        }
    }
    public void PodsvetkaIgroka()
    {
        if (Table.instance.bet_state == 0)
        {
            if (Table.instance.place_id == 1)
            {
                if (Players.instance.next_user == 1)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 2)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 3)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 4)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 5)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 6)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 2)
            {
                if (Players.instance.next_user == 1)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 2)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 3)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 4)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 5)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 6)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 3)
            {
                if (Players.instance.next_user == 1)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 2)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 3)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 4)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 5)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 6)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 4)
            {
                if (Players.instance.next_user == 1)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 2)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 3)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 4)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 5)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 6)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 5)
            {
                if (Players.instance.next_user == 1)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 2)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 3)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 4)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 5)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 6)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 6)
            {
                if (Players.instance.next_user == 1)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 2)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 3)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 4)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 5)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_user == 6)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
            }
        }
        if(Table.instance.bet_state == 1)
        {
            if (Table.instance.place_id == 1)
            {
                if (Players.instance.next_bet_user == 1)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 2)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 3)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 4)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 5)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 6)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 2)
            {
                if (Players.instance.next_bet_user == 1)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 2)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 3)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 4)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 5)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 6)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 3)
            {
                if (Players.instance.next_bet_user == 1)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 2)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 3)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 4)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 5)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 6)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 4)
            {
                if (Players.instance.next_bet_user == 1)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 2)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 3)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 4)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 5)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 6)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 5)
            {
                if (Players.instance.next_bet_user == 1)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 2)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 3)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 4)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 5)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 6)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
            }
            if (Table.instance.place_id == 6)
            {
                if (Players.instance.next_bet_user == 1)
                {
                    p2Podsvetka.enabled = true;
                }
                else
                {
                    p2Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 2)
                {
                    p3Podsvetka.enabled = true;
                }
                else
                {
                    p3Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 3)
                {
                    p4Podsvetka.enabled = true;
                }
                else
                {
                    p4Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 4)
                {
                    p5Podsvetka.enabled = true;
                }
                else
                {
                    p5Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 5)
                {
                    p6Podsvetka.enabled = true;
                }
                else
                {
                    p6Podsvetka.enabled = false;
                }
                if (Players.instance.next_bet_user == 6)
                {
                    p1Podsvetka.enabled = true;
                }
                else
                {
                    p1Podsvetka.enabled = false;
                }
            }
        }
    }
    public void checkBetButtons()
    {
        if(Table.instance.place_id == Players.instance.next_bet_user && Table.instance.bet_state == 1)
        {
            foldBTN.SetActive(true);
            raiseBTN.SetActive(true);
            betChangerGO.SetActive(true);
            if(mojnoCheckBTNT)
            {
                checkBTN.SetActive(true);
                callBTN.SetActive(false);
            }
            else
            {
                checkBTN.SetActive(false);
                callBTN.SetActive(true);
            }
        }
        else
        {
            foldBTN.SetActive(false);
            raiseBTN.SetActive(false);
            betChangerGO.SetActive(false);
            checkBTN.SetActive(false);
            callBTN.SetActive(false);
        }
    }
    
}
