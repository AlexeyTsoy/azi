﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class preLoadScript : MonoBehaviour
{
    public UIInput input;


    public void Connect()
    {
        ConnectorScript.ipAdress = input.value;
        PlayerPrefs.SetString("ip", input.value);
        SceneManager.LoadScene("Connecting");
    }

    void Start ()
    {
        input.value = PlayerPrefs.GetString("ip");
    }


    void Update ()
    {
		
	}
}
